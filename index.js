/**
 * @format
 */

import { AppRegistry } from 'react-native';
import App from './src/App';
import { name as appName } from './app.json';
import messaging from '@react-native-firebase/messaging';
import NotifService from './src/NotifService';
import { showMessage } from 'react-native-flash-message';

const onRegister = (token) => {
    return token
};

const onNotif = (notif) => {
    showMessage({
        type: 'info',
        message: notif.title,
        description: notif.message
    })
};

const notif = new NotifService(onRegister, onNotif)

messaging().setBackgroundMessageHandler(async remoteMessage => {
    notif.localNotif('sample.mp3', remoteMessage.notification)
    // console.log(remoteMessage.notification, "<<< background fcm")
})

AppRegistry.registerComponent(appName, () => App);
