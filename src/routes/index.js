import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import {
    AddPorto,
    SearchJob,
    SplashScreen,
    SignIn,
    SignUp,
    JobSeeker,
    JobDetail,
    UserProfile,
    AppliedJobs,
    BookmarkJobs,
    WorkExperience,
    Educations,
    Certification,
    NewsScreen,
    DetailNews,
    VibePoint,
    TopUp,
    Payment,
    SignUpOptions,
    AddProfile,
    AddSkill,
    Projects,
    TransferPoint,
    Withdraw,
    BankAccount,
    ProjectDetail,
    ProgressReport,
    DetailReport,
    ConfirmCode,
    InterestCategory,
    Messages,
    Chat,
    HomeInternal,
    Epayslip,
    EpayslipDetails,
    Notifications,
    Bpjs,
    Loan,
    ProfileInternal,
    InterestJobs,
    SearchJobseeker,
    ChangePassword,
    Activity,
    Camera,
    DetailAttendance,
    Permission,
    ApplyPermission,
    FilterJobs,
    ForgetPassword,
    VideoPlay,
    Videos,
    ConfirmApplications,
    FilterPage,
    ListEmployee,
    EmployeeActivity,
    EmployeeAttendance,
    EmployeeOffWork,
    EmployeeOvertime,
    EmployeePermission,
    AttendanceRecap,
    ApplyWorkOvertime,
    WorkOvertime,
    ActivityList,
    InsertActivity,
    Helpdesk,
    DetailAttendanceEmployee
} from '../screens';
import { BottomNavigator } from '../components'
import { useSelector } from 'react-redux';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

const MainApp = () => {
    return (
        <Tab.Navigator initialRouteName='Home' tabBar={props => <BottomNavigator {...props} />} >
            <Tab.Screen name='Profile' component={UserProfile} />
            <Tab.Screen name='Home' component={JobSeeker} />
            <Tab.Screen name='Notifications' component={Notifications} />
            <Tab.Screen name='Helpdesk' component={Helpdesk} />
        </Tab.Navigator>
    )
}

const InternalApp = () => {
    return (
        <Tab.Navigator initialRouteName='Home' tabBar={props => <BottomNavigator {...props} />} >
            <Tab.Screen name='Profile' component={ProfileInternal} />
            <Tab.Screen name='Home' component={HomeInternal} />
            <Tab.Screen name='Notifications' component={Notifications} />
            <Tab.Screen name='Helpdesk' component={Helpdesk} />
        </Tab.Navigator>
    )
}


const Router = () => {
    const token = useSelector(({ user }) => user.Token)

    return (
        <Stack.Navigator
            screenOptions={{ headerShown: false }}
            initialRouteName='SplashScreen'
        >
            <Stack.Screen name='SplashScreen' component={SplashScreen} />
            {!token && (
                <>
                    <Stack.Screen name='SignIn' component={SignIn} />
                    <Stack.Screen name='ConfirmCode' component={ConfirmCode} />
                    <Stack.Screen name='SignUp' component={SignUp} />
                    <Stack.Screen name='SignUpOptions' component={SignUpOptions} />
                    <Stack.Screen name='InterestCategory' component={InterestCategory} />
                </>
            )}

            <Stack.Screen name='Chat' component={Chat} />
            <Stack.Screen name='SearchJob' component={SearchJob} />
            <Stack.Screen name='Notifications' component={Notifications} />
            <Stack.Screen name='InterestJobs' component={InterestJobs} />
            <Stack.Screen name='SearchJobseeker' component={SearchJobseeker} />
            <Stack.Screen name='ChangePassword' component={ChangePassword} />
            <Stack.Screen name='ForgetPassword' component={ForgetPassword} />
            <Stack.Screen name='VideoPlay' component={VideoPlay} />
            <Stack.Screen name='Videos' component={Videos} />

            {/* fulltime */}
            <Stack.Screen name='MainApp' component={MainApp} />
            <Stack.Screen name='JobDetail' component={JobDetail} />
            <Stack.Screen name='BookmarkJobs' component={BookmarkJobs} />
            <Stack.Screen name='AddProfile' component={AddProfile} />
            <Stack.Screen name='JobApplied' component={AppliedJobs} />
            <Stack.Screen name='WorkExperience' component={WorkExperience} />
            <Stack.Screen name='Educations' component={Educations} />
            <Stack.Screen name='Certification' component={Certification} />
            <Stack.Screen name='NewsScreen' component={NewsScreen} />
            <Stack.Screen name='DetailNews' component={DetailNews} />
            <Stack.Screen name='FilterJobs' component={FilterJobs} />
            <Stack.Screen name='ConfirmApplications' component={ConfirmApplications} />
            <Stack.Screen name='FilterPage' component={FilterPage} />

            {/* freelancer */}
            <Stack.Screen name='VibePoint' component={VibePoint} />
            <Stack.Screen name='AddPorto' component={AddPorto} />
            <Stack.Screen name='TopUp' component={TopUp} />
            <Stack.Screen name='Payment' component={Payment} />
            <Stack.Screen name='AddSkill' component={AddSkill} />
            <Stack.Screen name='Projects' component={Projects} />
            <Stack.Screen name='TransferPoint' component={TransferPoint} />
            <Stack.Screen name='Withdraw' component={Withdraw} />
            <Stack.Screen name='BankAccount' component={BankAccount} />
            <Stack.Screen name='ProjectDetail' component={ProjectDetail} />
            <Stack.Screen name='ProgressReport' component={ProgressReport} />
            <Stack.Screen name='DetailReport' component={DetailReport} />

            {/* Internal */}
            <Stack.Screen name='InternalApp' component={InternalApp} />
            <Stack.Screen name='Epayslip' component={Epayslip} />
            <Stack.Screen name='EpayslipDetails' component={EpayslipDetails} />
            <Stack.Screen name='Bpjs' component={Bpjs} />
            <Stack.Screen name='Loan' component={Loan} />
            <Stack.Screen name='Activity' component={Activity} />
            <Stack.Screen name='Camera' component={Camera} />
            <Stack.Screen name='DetailAttendance' component={DetailAttendance} />
            <Stack.Screen name='Permission' component={Permission} />
            <Stack.Screen name='ApplyPermission' component={ApplyPermission} />
            <Stack.Screen name='ListEmployee' component={ListEmployee} />
            <Stack.Screen name='EmployeeActivity' component={EmployeeActivity} />
            <Stack.Screen name='EmployeeAttendance' component={EmployeeAttendance} />
            <Stack.Screen name='EmployeeOffWork' component={EmployeeOffWork} />
            <Stack.Screen name='EmployeeOvertime' component={EmployeeOvertime} />
            <Stack.Screen name='EmployeePermission' component={EmployeePermission} />
            <Stack.Screen name='AttendanceRecap' component={AttendanceRecap} />
            <Stack.Screen name='ApplyWorkOvertime' component={ApplyWorkOvertime} />
            <Stack.Screen name='WorkOvertime' component={WorkOvertime} />
            <Stack.Screen name='ActivityList' component={ActivityList} />
            <Stack.Screen name='InsertActivity' component={InsertActivity} />
            <Stack.Screen name='DetailAttendanceEmployee' component={DetailAttendanceEmployee} />

        </Stack.Navigator>
    )
}

export default Router
