import React, { useEffect } from 'react';
import { View, Text, Image, TouchableOpacity, Alert, StyleSheet } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import { ILChevrontL, ILSetting, ILLogout } from '../../../../assets';
import AvatarImage from '../../../../assets/img/avatar.png';

const Profile = ({ navigation, logout, user, styles }) => {

    const onPress = () => {
        Alert.alert(
            'Are you sure ?',
            'Logout from application',
            [
                {
                    text: 'Yes',
                    onPress: logout
                },
                {
                    text: 'No',
                    onPress: () => {
                        showMessage({
                            type: 'info',
                            message: 'Cancel logout from application'
                        })
                    },
                    style: "cancel"
                }
            ]
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#ffff', paddingBottom: 45 }}>
            <View style={[styles.container, { flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }]}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <ILChevrontL />
                </TouchableOpacity>
                <TouchableOpacity onPress={onPress} >
                    <ILLogout />
                </TouchableOpacity>
            </View>
            <View style={[styles.container, { flexDirection: 'row', marginTop: 20, justifyContent: 'space-between', alignItems: 'center' }]}>
                <View style={{ justifyContent: 'center' }}>

                    <Image source={user?.photo ? { uri: user.photo } : AvatarImage} style={{ width: 50, height: 50, borderRadius: 50 / 2 }} />
                </View>
                <View style={{ maxWidth: 270, flex: 1 }}>
                    <Text style={{ fontSize: 16, fontFamily: 'DMSans-Bold' }} >{user?.user_name}</Text>
                    <Text style={{ fontSize: 12, fontFamily: 'DMSans-Regular' }} >{user?.user_email}</Text>
                </View>
            </View>
        </View>

    )
};

export default Profile;
