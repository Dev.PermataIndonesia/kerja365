import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet, Dimensions } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps'
import { ILEllipse2 } from '../../../../assets';
const JobDescriptions = ({ job }) => {

    if (Object.keys(job).length > 0) {
        let jobResponsibility
        jobResponsibility = job.jobResponsibility
        if (job?.jobResponsibility.includes('-')) {
            let responsibility = job.jobResponsibility.split('-')

            for (let i = 0; i < responsibility.length; i++) {
                responsibility[i] = "- " + responsibility[i] + '\n'
            }
            jobResponsibility = responsibility.join('')
        }

        return (
            <View style={{ padding: 20 }}>
                <View>
                    <Text style={styles.h3}>Job Responsibility</Text>
                    <Text style={{ marginTop: 13, fontFamily: 'DMSans-Regular', fontSize: 13, textAlign: 'justify' }}>{jobResponsibility}</Text>
                </View>
                <View style={styles.space} />
                <View>
                    {job?.jobRequirments && (
                        <>
                            <Text style={styles.h3} >Job Requirements</Text>
                            {job?.jobRequirments?.map((item, i) => (
                                <View key={i} style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10 }}>
                                    <ILEllipse2 />
                                    <View style={{ width: 10 }} />
                                    <Text style={{ fontSize: 13, color: '#6B6969' }} >{item}</Text>
                                </View>
                            ))}
                        </>
                    )}
                </View>
                {job.type === 'Fulltime' && (
                    <>
                        <View style={styles.space} />
                        <View>
                            <Text style={styles.h3} >Area</Text>
                            <Text style={{ fontSize: 13, marginTop: 10, color: '#6B6969' }} >{job.area}</Text>
                        </View>
                        <View style={styles.space} />
                        <View>

                            <Text style={styles.h3} >Address</Text>
                            <Text style={{ fontSize: 13, color: '#6B6969', marginTop: 15 }} >{job.address}</Text>
                            {job.lat && job.lng && (
                                <MapView
                                    provider={PROVIDER_GOOGLE}
                                    style={styles.map}
                                    initialRegion={{
                                        latitude: +job.lat,
                                        longitude: +job.lng,
                                        latitudeDelta: 0.022,
                                        longitudeDelta: 0.001,
                                    }}
                                    showsUserLocation={true}
                                    zoomControlEnabled
                                >
                                    <Marker
                                        coordinate={{
                                            latitude: +job.lat,
                                            longitude: +job.lng,
                                            latitudeDelta: 0.0922,
                                            longitudeDelta: 0.0421,
                                        }}
                                        title={job?.company?.company_name}
                                    />
                                </MapView>
                            )}
                        </View>
                    </>
                )}
            </View>
        )
    }

    return null
}

const styles = StyleSheet.create({
    space: {
        height: 33,
        width: 20
    },
    map: {
        marginTop: 20,
        width: Dimensions.get('window').width - 40,
        height: 202
    },
    h3: {
        fontFamily: 'DMSans-Bold',
        fontSize: 14
    }
})

export default JobDescriptions;
