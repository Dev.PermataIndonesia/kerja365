import React, { useEffect } from 'react';
import { TouchableOpacity } from 'react-native';
import { ILHome, ILSend, ILUser, ILHeadsetSuport, ILHeadsetSuportY, ILHomeY, ILUserY, ILSendY } from '../../../assets';

const TabItem = ({ title, isFocused, options, onPress, onLongPress }) => {

    useEffect(() => {
        console.log(title, isFocused);
    }, [])

    const Icon = () => {
        if (isFocused) {
            if (title === 'Home') {
                return <ILHomeY />
            }
            if (title === 'Profile') {
                return <ILUserY />
            }
            if (title === 'Notifications') {
                return <ILSendY />
            }
            if (title === 'Helpdesk') {
                return <ILHeadsetSuportY />
            }
            return null
        } else {
            if (title === 'Home') {
                return <ILHome />
            }
            if (title === 'Profile') {
                return <ILUser />
            }
            if (title === 'Notifications') {
                return <ILSend />
            }
            if (title === 'Helpdesk') {
                return <ILHeadsetSuport />
            }
            return <ILUser />
        }
    }

    return (
        <TouchableOpacity
            accessibilityRole="button"
            accessibilityState={isFocused ? { selected: true } : {}}
            accessibilityLabel={options.tabBarAccessibilityLabel}
            testID={options.tabBarTestID}
            onPress={onPress}
            onLongPress={onLongPress}
        >
            <Icon />
        </TouchableOpacity>
    )
}

export default TabItem;
