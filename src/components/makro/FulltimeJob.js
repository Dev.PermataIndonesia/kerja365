import React, { useState } from 'react';
import { View, StyleSheet, TouchableOpacity, Text, Dimensions, ActivityIndicator } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import { useDispatch, useSelector } from 'react-redux';
import { ILMoreVErtical } from '../../assets';
import { fetchJobsByCategory } from '../../store/reducer/jobsReducer';
import { JobCard } from '../../components'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';

const JobsList = ({ navigation, loading, jobs, onPress }) => {
    return (
        <View style={styles.container}>
            {jobs && jobs?.map(el => (
                <JobCard
                    key={el._id}
                    ILMoreVErtical={ILMoreVErtical}
                    job={el}
                    navigation={navigation}
                />
            )
            )}
            {loading && (
                <SkeletonPlaceholder>
                    <View style={{ height: 70, borderRadius: 11, marginTop: 20 }} />
                    <View style={{ height: 70, borderRadius: 11, marginTop: 10 }} />
                    <View style={{ height: 70, borderRadius: 11, marginTop: 10 }} />
                    <View style={{ height: 70, borderRadius: 11, marginTop: 10 }} />
                    <View style={{ height: 70, borderRadius: 11, marginTop: 10 }} />
                </SkeletonPlaceholder>
            )}
            {jobs?.length >= 5 && !loading && (
                <TouchableOpacity onPress={onPress} style={styles.btn} >
                    <Text style={styles.btnTitle} >Show More</Text>
                </TouchableOpacity>
            )}
        </View>
    )
};

const JobCategories = ({ navigation, token, jobs, limit, onPress }) => {
    const dispatch = useDispatch()
    const initialLayout = { width: Dimensions.get('window').width };
    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'all', title: 'All' },
        { key: 'marketing', title: 'Marketing' },
        { key: 'it', title: 'IT' },
        { key: 'admin', title: 'Admin' },
        { key: 'sales', title: 'Sales' },
        { key: 'technician', title: 'Technician' },
        { key: 'design', title: 'Design' },
        { key: 'videographer', title: 'Videographer' },
        { key: 'journalist', title: 'Journalist' },
        { key: 'medical', title: 'Medical' },
        { key: 'teaching', title: 'Teaching' },
        { key: 'security', title: 'Security' },
        { key: 'driver', title: 'Driver' },
        { key: 'cleaning service', title: 'Cleaning Service' },
        { key: 'office boy', title: 'Office Boy' },
        { key: 'call center', title: 'Call Center' },
    ]);
    const jobsCategory = useSelector(({ jobs }) => jobs.JobsCategory)
    const loading = useSelector(({ jobs }) => jobs.JobsCategoryLoading)

    const onPressCategory = (route) => {
        dispatch(fetchJobsByCategory(route, token, jobsCategory[`${route.key}`].length))
    }

    const RenderScene = () => {
        return (
            <>
                {routes.map((route, i) => (
                    <View key={i}>
                        {index === i &&
                            <JobsList
                                navigation={navigation}
                                jobs={i == 0 ? jobs : jobsCategory[`${route.key}`] ? jobsCategory[`${route.key}`] : []}
                                limit={limit}
                                onPress={i === 0 ? onPress : () => onPressCategory(route)}
                                loading={loading}
                            />
                        }
                    </View>
                ))}
            </>
        )
    };

    const renderTabBar = props => (
        <TabBar
            {...props}
            indicatorContainerStyle={{ marginHorizontal: 20 }}
            indicatorStyle={{ height: 8, backgroundColor: '#FF9901', borderRadius: 10, marginRight: 120 }}
            style={{ backgroundColor: 'transparent' }}
            getLabelText={({ route }) => route.title}
            renderLabel={({ route }) => (
                <View style={{ maxWidth: 120 }} >
                    <Text style={{ color: 'black', paddingHorizontal: 10, fontFamily: 'DMSans-Bold', textAlign: 'center', textAlignVertical: 'center' }} >{route.title}</Text>
                </View>
            )}
            onTabPress={({ route }) => {
                if (route.key !== 'all' && !jobsCategory[`${route.key}`]) {
                    dispatch(fetchJobsByCategory(route, token, 0))
                }
            }}
            activeColor="#ff9901"
            inactiveColor="red"
            scrollEnabled
            tabStyle={{ width: 'auto', paddingRight: 20 }}
            contentContainerStyle={{ paddingHorizontal: 20 }}
        />
    );

    return (
        <View>
            <TabView
                navigationState={{ index, routes }}
                renderScene={() => null}
                onIndexChange={setIndex}
                initialLayout={initialLayout}
                renderTabBar={renderTabBar}
                swipeEnabled={false}
            />
            <RenderScene />
        </View>
    )
};

const styles = StyleSheet.create({
    underlineStyle: {
        height: 8,
        width: 53,
        backgroundColor: '#FF9901',
        borderRadius: 10,
    },
    tabStyle: {
        borderWidth: 0
    },
    scrollStyle: {
        borderWidth: 0,
        paddingLeft: 20,
    },
    space: {
        height: 15,
        width: 15
    },
    container: {
        paddingHorizontal: 20
    },
    btn: {
        marginTop: 37,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(238, 238, 238, 0.5)',
        paddingVertical: 16,
        borderRadius: 50
    },
    btnTitle: {
        color: '#FF9901',
        fontFamily: 'DMSans-Regular',
        fontSize: 14
    },
    card: {
        borderRadius: 10,
        marginTop: 8,
        backgroundColor: '#ffff',
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 25
    },
    title: {
        fontSize: 14,
        fontFamily: 'DMSans-Bold'
    },
    desc: {
        fontSize: 12,
        color: '#6B6969',
        fontFamily: 'DMSans-Bold',
        paddingRight: 5
    },
    image: {
        justifyContent: 'center'
    },
    logo: {
        width: 40,
        height: 40,
        borderRadius: 40 / 2
    }
})

export default JobCategories;
