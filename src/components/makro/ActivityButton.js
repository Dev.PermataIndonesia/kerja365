import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import { useSelector } from 'react-redux';
import { ILSendW, ILCofee, ILUserW, ILFileText2, ILBookmark3 } from '../../assets';

const ActivityButton = ({ navigation, user }) => {
    const employees = useSelector(({ employees }) => employees.Employees.All)
    const entered = useSelector(({ employees }) => employees.Employees.Entered)
    const permissions = useSelector(({ employees }) => employees.Permissions)
    const absents = useSelector(({ employees }) => employees.Absents)
    const activities = useSelector(({ employees }) => employees.Activities)
    const overtimes = useSelector(({ employees }) => employees.Overtimes)

    return (
        <>
            <View style={{ flexDirection: "row" }} >
                <TouchableOpacity
                    onPress={() => navigation.navigate('Activity')}
                    style={{ padding: 20, justifyContent: "space-between", height: 117, backgroundColor: '#ffff', borderRadius: 11, flex: 1 }}
                >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                        <Text style={{ width: 100 }}>Absensi</Text>
                        <ILSendW />
                    </View>
                    <View style={{ height: 10, backgroundColor: '#088E6B', borderRadius: 11 }} />
                </TouchableOpacity>
                <View style={{ width: 20 }} />
                <TouchableOpacity
                    onPress={() => navigation.navigate('Permission')}
                    style={{ padding: 20, justifyContent: "space-between", height: 117, backgroundColor: '#ffff', borderRadius: 11, flex: 1 }}
                >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                        <Text style={{ width: 100 }}>Cuti</Text>
                        <ILCofee />
                    </View>
                    <View style={{ height: 10, backgroundColor: '#088E6B', borderRadius: 11 }} />
                </TouchableOpacity>
            </View>
            <View style={{ flexDirection: "row", marginTop: 20 }} >
                <TouchableOpacity
                    onPress={() => navigation.navigate('WorkOvertime')}
                    style={{ padding: 20, justifyContent: "space-between", height: 117, backgroundColor: '#ffff', borderRadius: 11, flex: 1 }}
                >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                        <Text style={{ width: 100 }}>Lembur</Text>
                        <ILFileText2 />
                    </View>
                    <View style={{ height: 10, backgroundColor: '#088E6B', borderRadius: 11 }} />
                </TouchableOpacity>
                <View style={{ width: 20 }} />
                <TouchableOpacity
                    onPress={() => navigation.navigate('ActivityList')}
                    style={{ padding: 20, justifyContent: "space-between", height: 117, backgroundColor: '#ffff', borderRadius: 11, flex: 1 }}
                >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                        <Text style={{ width: 100 }}>Aktifitas</Text>
                        <ILBookmark3 />
                    </View>
                    <View style={{ height: 10, backgroundColor: '#088E6B', borderRadius: 11 }} />
                </TouchableOpacity>
            </View>
            {user?.supervisor && (
                <View style={{ marginTop: 20 }}>
                    <View style={{ flexDirection: 'row', paddingVertical: 20, paddingHorizontal: 5, alignItems: 'center' }} >
                        <Text style={{ fontWeight: '700' }} >Monitoring</Text>
                        <View style={{ height: 10, flex: 1, marginLeft: 10, borderRadius: 11, backgroundColor: '#E5E5E5' }} />
                    </View>
                    <View style={{ flexDirection: "row" }} >
                        <TouchableOpacity
                            onPress={() => navigation.navigate('ListEmployee')}
                            style={{ padding: 20, justifyContent: "space-between", height: 130, backgroundColor: '#ffff', borderRadius: 11, flex: 1 }}
                        >
                            <View>
                                <Text >Daftar Karyawan</Text>
                                <View style={{ flexDirection: 'row', paddingVertical: 5, justifyContent: 'space-between' }} >
                                    <View>
                                        <Text>{employees?.length}</Text>
                                        <Text>Persons</Text>
                                    </View>
                                    <ILUserW />
                                </View>
                            </View>
                            <View style={{ height: 10, backgroundColor: '#1795BD', borderRadius: 11 }} />
                        </TouchableOpacity>
                        <View style={{ width: 20 }} />
                        <TouchableOpacity
                            onPress={() => navigation.navigate('EmployeeAttendance')}
                            style={{ padding: 20, justifyContent: "space-between", height: 130, backgroundColor: '#ffff', borderRadius: 11, flex: 1 }}
                        >
                            <View>
                                <Text>Absensi Karyawan</Text>
                                <View style={{ flexDirection: 'row', paddingVertical: 5, justifyContent: 'space-between' }} >
                                    <View>
                                        <Text style={{ width: 100 }} >{entered?.length}/{employees?.length}</Text>
                                        <Text style={{ width: 100 }} >Activity</Text>
                                    </View>
                                    <ILFileText2 />
                                </View>
                            </View>
                            <View style={{ height: 10, backgroundColor: '#1795BD', borderRadius: 11 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: 20 }} >
                        <TouchableOpacity
                            onPress={() => navigation.navigate('EmployeePermission')}
                            style={{ padding: 20, justifyContent: "space-between", height: 130, backgroundColor: '#ffff', borderRadius: 11, flex: 1 }}
                        >
                            <View>
                                <Text>Cuti Karyawan</Text>
                                <View style={{ flexDirection: 'row', paddingVertical: 5, justifyContent: 'space-between' }} >
                                    <View>
                                        <Text style={{ width: 100 }} >{permissions?.allDatas?.length}</Text>
                                        <Text style={{ width: 100 }} >Persons</Text>
                                    </View>
                                    <ILCofee />
                                </View>
                            </View>
                            <View style={{ height: 10, backgroundColor: '#1795BD', borderRadius: 11 }} />
                        </TouchableOpacity>
                        <View style={{ width: 20 }} />
                        <TouchableOpacity
                            onPress={() => navigation.navigate('EmployeeOvertime')}
                            style={{ padding: 20, justifyContent: "space-between", height: 130, backgroundColor: '#ffff', borderRadius: 11, flex: 1 }}
                        >
                            <View>
                                <Text>Lembur Karyawan</Text>
                                <View style={{ flexDirection: 'row', paddingVertical: 5, justifyContent: 'space-between' }} >
                                    <View>
                                        <Text style={{ width: 100 }} >{overtimes?.allDatas?.length}</Text>
                                        <Text style={{ width: 100 }} >Activity</Text>
                                    </View>
                                    <ILFileText2 />
                                </View>
                            </View>
                            <View style={{ height: 10, backgroundColor: '#1795BD', borderRadius: 11 }} />
                        </TouchableOpacity>
                    </View>
                    <View style={{ flexDirection: "row", marginTop: 20 }} >
                        <TouchableOpacity
                            onPress={() => navigation.navigate('EmployeeOffWork')}
                            style={{ padding: 20, justifyContent: "space-between", height: 130, backgroundColor: '#ffff', borderRadius: 11, flex: 1 }}
                        >
                            <View>
                                <Text>Izin Karyawan</Text>
                                <View style={{ flexDirection: 'row', paddingVertical: 5, justifyContent: 'space-between' }} >
                                    <View>
                                        <Text style={{ width: 100 }} >{absents?.allDatas?.length}</Text>
                                        <Text style={{ width: 100 }} >Persons</Text>
                                    </View>
                                    <ILSendW />
                                </View>
                            </View>
                            <View style={{ height: 10, backgroundColor: '#1795BD', borderRadius: 11 }} />
                        </TouchableOpacity>
                        <View style={{ width: 20 }} />
                        <TouchableOpacity
                            onPress={() => navigation.navigate('EmployeeActivity')}
                            style={{ padding: 20, justifyContent: "space-between", height: 130, backgroundColor: '#ffff', borderRadius: 11, flex: 1 }}
                        >
                            <View>
                                <Text>Aktifitas Karyawan</Text>
                                <View style={{ flexDirection: 'row', paddingVertical: 5, justifyContent: 'space-between' }} >
                                    <View>
                                        <Text style={{ width: 100 }} >{activities?.length}</Text>
                                        <Text style={{ width: 100 }} >Activity</Text>
                                    </View>
                                    <ILBookmark3 />
                                </View>
                            </View>
                            <View style={{ height: 10, backgroundColor: '#1795BD', borderRadius: 11 }} />
                        </TouchableOpacity>
                    </View>
                </View>
            )}
        </>
    )
};

export default ActivityButton;
