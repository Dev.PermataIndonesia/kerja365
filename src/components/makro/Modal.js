import React, { useEffect, useState } from 'react';
import { SafeAreaView, Modal, ScrollView, Text, StyleSheet, View, Image, TouchableOpacity, Dimensions, TextInput, RefreshControl } from 'react-native';

const ModalFilter = ({ navigation, visible, type }) => {
    const [typeJobseeker, setTypeJobseeker] = useState(["Fultimer", "Freelancer", "Sales", "Developer", "Admin", "OB"])
    const [selectType, setSelectType] = useState([])

    const select = (type) => {
        let filter
        if (selectType.includes(type)) {
            filter = selectType.filter(el => el !== type)
        }
        else {
            filter = selectType.concat(type)
        }
        setSelectType(filter)
    }

    return (
        <Modal
            animationType="slide"
            transparent={true}
            visible={visible}
        >
            <View style={{ flex: 1, justifyContent: "center", alignItems: "center", marginTop: 22 }} >
                <View style={{
                    margin: 20,
                    backgroundColor: "white",
                    borderRadius: 20,
                    padding: 35,
                    alignItems: "center",
                    shadowColor: "#000",
                    shadowOffset: {
                        width: 0,
                        height: 2
                    },
                    shadowOpacity: 0.25,
                    shadowRadius: 4,
                    elevation: 5
                }} >
                    <View style={{ flexDirection: 'row', flexWrap: 'wrap' }} >
                        {typeJobseeker.map((item, i) => (
                            <View key={i} style={{ padding: 7 }} >
                                <TouchableOpacity
                                    onPress={() => select(item)}
                                    style={{ padding: 5, borderRadius: 7, borderWidth: 1, borderColor: '#ff9901', backgroundColor: selectType.includes(item) ? '#ff9901' : '#ffff' }} >
                                    <Text style={{ color: selectType.includes(item) ? '#ffff' : '#ff9901' }} >{item}</Text>
                                </TouchableOpacity>
                            </View>
                        ))}
                    </View>
                    <TouchableOpacity
                        onPress={() => navigation.navigate('SearchJobseeker', { search: 'filter' })}
                        style={{ marginTop: 50, borderRadius: 7, backgroundColor: '#ff9901', width: 100, height: 50, alignItems: 'center', justifyContent: 'center' }}
                    >
                        <Text style={{ color: '#ffff', fontWeight: '700' }} >Filter</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

export default ModalFilter
