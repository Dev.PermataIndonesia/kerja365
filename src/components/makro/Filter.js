import React, {useEffect, useState} from 'react';
import { Text, View, TouchableOpacity, ScrollView, Dimensions } from 'react-native';
import { useDispatch } from 'react-redux';
import { fetchFilterJobs } from '../../store/reducer/jobsReducer';

const FilterModal = ({filter, setFilter, navigation, token}) => {
    const dispatch = useDispatch()

    const [dropDown, setDropDown] = useState(false)
    const [dropDown1, setDropDown1] = useState(false)
    const [categories] = useState(['Marketing', 'IT', 'Design', 'Sales', "Technician", "Videographer", "Journalist", "Administration", "Medical", "Teaching", "Security", "Driver", "Cleaning Service", "Office Boy", "Call Center"])
    const [states] = useState(['Aceh', 'Medan', 'Pekanbaru', 'Lampung', "Padang", "Bengkulu", "Palembang", "Batam", "DKI Jakarta", "Bandung", "Bogor", "Yogyakarta", "Malang", "Surabaya", "Bali"])
    const [state, setState] = useState('')
    const [category, setCategory] = useState('')

    const selectType = () => {
        if (dropDown1) setDropDown1(false)
        setDropDown(true)
    }

    const selectState = () => {
        if (dropDown) setDropDown(false)
        setDropDown1(true)
    }

    const onPress = () => {
        setFilter(false)
        dispatch(fetchFilterJobs(token, 0, category, state, true))
        navigation.navigate('FilterJobs', {area: state, category: category})
    }

    if (filter) {
        return (
            <View style={{flex:1, width: Dimensions.get('screen').width, height: Dimensions.get('screen').height, position: 'absolute',  backgroundColor: 'rgba(0,0,0, 0.5)', justifyContent: 'center', alignItems: 'center'}} >
            <View style={{width: Dimensions.get('screen').width - 100, height: 400, borderRadius: 11, backgroundColor: '#ffff'}} >
                <View
                    style={{padding: 20}}
                >
                    <View>
                        <Text>Type</Text>
                        <TouchableOpacity
                            onPress={selectType}
                            style={{borderWidth: 1, borderColor: "#eeee", borderRadius: 5, padding: 10, marginTop: 10}}
                        >
                            <Text>{category ? category : 'Select'}</Text>
                        </TouchableOpacity>
                        <View style={{alignItems: 'center'}} >
                            {dropDown && (
                            <ScrollView 
                                showsVerticalScrollIndicator={false}
                                style={{height: 150, width:200, position: 'absolute',zIndex:1, backgroundColor: '#c4c4c4'}} 
                            >
                                {categories.map((el, i) => (
                                    <TouchableOpacity
                                        key={i}
                                        style={{borderWidth: 1, padding: 10, borderColor: '#eeee', borderRadius: 5}}
                                        onPress={() => {
                                            setCategory(el)
                                            setDropDown(false)
                                            }
                                        }
                                    >
                                        <Text style={{color: '#ffff'}} >{el}</Text>
                                    </TouchableOpacity>
                                ))}
                            </ScrollView>
                            )}
                        </View>
                    </View>
                    <View style={{marginTop: 20}} >
                        <Text>Area</Text>
                        <TouchableOpacity
                            onPress={selectState}
                            style={{borderWidth: 1, borderColor: "#eeee", borderRadius: 5, padding: 10, marginTop: 10}}
                        >
                            <Text>{state ? state : 'Select'}</Text>
                        </TouchableOpacity>
                        <View style={{alignItems: 'center'}} >
                            {dropDown1 && (
                            <ScrollView 
                                showsVerticalScrollIndicator={false}
                                style={{height: 150, width:200, position: 'absolute',zIndex:9999, backgroundColor: '#c4c4c4'}} 
                            >
                                {states.map((el, i) => (
                                    <TouchableOpacity
                                        key={i}
                                        style={{borderWidth: 1, padding: 10, borderColor: '#eeee', borderRadius: 5}}
                                        onPress={() => {
                                            setState(el)
                                            setDropDown1(false)
                                            }
                                        }
                                    >
                                        <Text style={{color: '#ffff'}} >{el}</Text>
                                    </TouchableOpacity>
                                ))}
                            </ScrollView>
                            )}
                        </View>
                    </View>
                    <View style={{marginTop: 50}} >
                        {state && category ? (
                        <TouchableOpacity
                            onPress={onPress}
                            style={{backgroundColor: '#ff9901', padding:15,justifyContent: 'center',alignItems: 'center', borderRadius: 11}}
                        >
                            <Text style={{color: '#ffff'}} >Filter</Text>
                        </TouchableOpacity>
                        ) : (
                        <View
                            style={{backgroundColor: '#eeee', padding:15,justifyContent: 'center',alignItems: 'center', borderRadius: 11}}
                        >
                            <Text style={{color: '#ffff'}} >Filter</Text>
                        </View>
                        )}
                        <TouchableOpacity
                            onPress={() => setFilter(false)}
                            style={{backgroundColor: '#e74c3c', padding:15,justifyContent: 'center',alignItems: 'center', borderRadius: 11, marginTop: 10}}
                        >
                            <Text style={{color: '#ffff'}} >Cancel</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </View>
        )
    }

    return null
};

export default FilterModal;
