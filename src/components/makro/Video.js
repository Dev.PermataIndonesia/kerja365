import React from 'react';
import { Text, View, ScrollView, TouchableOpacity, Image, Dimensions } from 'react-native';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { useSelector } from 'react-redux';
import { ILMoreVErtical, ILPLayCircle } from '../../assets';

const Video = ({ navigation }) => {
    const videos = useSelector(({ videos }) => videos.Videos)
    const loading = useSelector(({ videos }) => videos.Loading)

    return (
        <View style={{ paddingVeritical: 20 }} >
            <View style={{ flexDirection: 'row', paddingHorizontal: 20, justifyContent: 'space-between' }} >
                <Text>Videos</Text>
                {!loading && (
                    <TouchableOpacity
                        onPress={() => navigation.navigate('VideoPlay', { video: JSON.stringify(videos[0]) })}
                    >
                        <Text style={{ color: '#ff9901' }} >Show more</Text>
                    </TouchableOpacity>
                )}
            </View>
            <ScrollView
                horizontal
                showsHorizontalScrollIndicator={false}
                style={{ marginTop: 20 }}
            >
                {loading ? (
                    <SkeletonPlaceholder>
                        <View style={{ width: Dimensions.get('screen').width - 80, marginLeft: 20, height: 120, borderRadius: 11 }} />
                    </SkeletonPlaceholder>
                ) : (
                    <>
                        {videos?.length > 0 && videos?.slice(0, 3)?.map((video, i) => (
                            <View
                                key={video._id}
                            >
                                <TouchableOpacity
                                    onPress={() => navigation.navigate('VideoPlay', { video: JSON.stringify(video) })}
                                    style={{ padding: 10, shadowColor: "#000", shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.22, shadowRadius: 2.22, elevation: 3, backgroundColor: '#ffff', borderRadius: 11, marginLeft: 20, marginRight: i === 2 ? 20 : 0, flexDirection: 'row' }}
                                >
                                    <Image source={{ uri: video.thumbnail }} resizeMode="cover" style={{ width: 170, height: 90, justifyContent: 'flex-end', borderRadius: 7 }} />
                                    <View style={{ flex: 1, marginLeft: 10, justifyContent: 'space-between' }} >
                                        <View style={{ zIndex: 1, width: 100 }} >
                                            <Text style={{ fontSize: 12, color: '#2d3436' }} >{((video.title).length > 30) ? (((video.title).substring(0, 30 - 3)) + '...') : video.title}</Text>
                                        </View>
                                        <View style={{
                                            position: 'absolute',
                                        }} >
                                            <ILPLayCircle />
                                        </View>
                                        <View style={{ alignSelf: 'flex-end' }} >
                                            <ILMoreVErtical />
                                        </View>
                                    </View>
                                </TouchableOpacity>
                                <View style={{ width: 70, height: 10 }} />
                            </View>
                        ))}
                    </>
                )}
            </ScrollView>
        </View>

    )
};

export default Video;
