import React, { useRef, useEffect } from 'react';
import { Animated, Dimensions } from 'react-native';

const AnimationSlideIn = (props) => {
    const fadeAnim = useRef(new Animated.Value(Dimensions.get('screen').height)).current

    useEffect(() => {
        if (!props?.opacity) {
            Animated.timing(
                fadeAnim,
                {
                    toValue: Dimensions.get('screen').height,
                    duration: 300,
                    useNativeDriver: true
                }
            ).start()
        } else {
            Animated.timing(
                fadeAnim,
                {
                    toValue: 0,
                    duration: 300,
                    useNativeDriver: true
                }
            ).start()
        }
    }, [fadeAnim, props.opacity])

    return (
        <Animated.View
            style={{
                ...props.style,
                transform: [{ translateY: fadeAnim }]
            }}
        >
            {props.children}
        </Animated.View>
    );
}

export default AnimationSlideIn;
