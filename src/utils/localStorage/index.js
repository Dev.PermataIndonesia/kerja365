import AsyncStorage from '@react-native-async-storage/async-storage';

export const setData = async (key, value) => {
    await AsyncStorage.setItem(key, JSON.stringify(value))
}

export const getData = async (key) => {
    const value = await AsyncStorage.getItem(key);
    if (value) {
        return JSON.parse(value);
    }
}

export const removeData = async () => {
    await AsyncStorage.clear()
}
