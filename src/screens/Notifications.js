import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ILChevrontL, ILEllipse, ILMoreVErtical } from '../assets';
import { fetchNotifications } from '../store/reducer/messagesReducer';
import instance from '../config/axios';
import { showMessage } from 'react-native-flash-message';
import { Loading } from '../components';

const Notifications = ({ navigation }) => {
    const dispatch = useDispatch()

    const user = useSelector(({ user }) => user.User)
    const token = useSelector(({ user }) => user.Token)
    const notifications = useSelector(({ messages }) => messages.Notifications)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        if (token && notifications?.length < 1) {
            let id
            if (user.type === 'internal' || user.type === 'tko') id = user.nrk
            else id = user.id
            dispatch(fetchNotifications(id))
        }

    }, [token])

    const markRead = async (notification) => {
        setLoading(true)
        try {
            await instance.put(`/notification/mark/${notification.id}`, { title: notification.title }, {
                headers: {
                    access_token: token
                }
            })
            let id
            if (user.type === 'internal' || user.type === 'tko') id = user.nrk
            else id = user.id
            dispatch(fetchNotifications(id))
        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops!! something wrong'
            })
        }
        setLoading(false)
    }

    return (
        <>
            <View
                style={{ flex: 1, backgroundColor: '#ffff' }}
            >
                <View style={{ paddingHorizontal: 20 }} >
                    <View
                        style={{
                            flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 40
                        }}
                    >
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                            style={{ width: 50 }}
                        >
                            <ILChevrontL />
                        </TouchableOpacity>
                        <View>
                            <Text style={{ fontSize: 16 }} >Notifications</Text>
                        </View>
                        <View style={{ width: 50 }} />
                    </View>

                </View>
                <ScrollView style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.3)' }} >
                    <View style={{ padding: 20 }}>
                        {notifications?.length > 0 && notifications.map(notification => {
                            if (!notification.read) {
                                return <TouchableOpacity
                                    onPress={() => markRead(notification)}
                                    key={notification.id}
                                    style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between', alignItems: 'center', padding: 20, borderRadius: 11, backgroundColor: '#ffff' }}
                                >
                                    <ILEllipse />
                                    <Text style={{ maxWidth: 230, color: 'black', flex: 1 }} >{notification.title}</Text>
                                    <View>
                                        <ILMoreVErtical />
                                    </View>
                                </TouchableOpacity>
                            } else {
                                return <View
                                    key={notification.id}
                                    style={{ flexDirection: 'row', marginTop: 10, justifyContent: 'space-between', alignItems: 'center', padding: 20, borderRadius: 11, backgroundColor: '#EEEE' }}
                                >
                                    <ILEllipse />
                                    <Text style={{ maxWidth: 200, color: 'black', flex: 1 }} >{notification.title}</Text>
                                    <View
                                        onPress={() => markRead(notification)}
                                        style={{ padding: 7, borderRadius: 5 }}
                                    >
                                        <Text style={{ fontSize: 10, color: 'black' }} >Has read</Text>
                                    </View>
                                </View>
                            }
                        })}
                    </View>
                </ScrollView>
            </View>
            {loading && <Loading />}
        </>
    )
};

export default Notifications;
