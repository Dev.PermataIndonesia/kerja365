import React, { useEffect, useState } from 'react';
import { ActivityIndicator } from 'react-native';
import { SafeAreaView, Text, View, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { ILChevrontL, ILMoreVErtical } from '../assets';
import { JobCard } from '../components';
import { fetchInterestUser } from '../store/reducer/jobsReducer';

const InterestJobs = ({ navigation, route }) => {
    const dispatch = useDispatch()

    const user = useSelector(({ user }) => user.User)
    const token = useSelector(({ user }) => user.Token)
    const jobs = useSelector(({ jobs }) => jobs.InterestJobs)
    const loading = useSelector(({ jobs }) => jobs.JobsLoading)

    // useEffect(() => {
    // }, [])

    const onPress = () => {
        dispatch(fetchInterestUser(token, jobs.length, user.interestCategory))
    }

    return (
        <SafeAreaView
            style={{ backgroundColor: '#ffff', flex: 1 }}
        >
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingHorizontal: 20 }}>
                <TouchableOpacity
                    onPress={() => navigation.goBack()}
                    style={{ width: 50 }}
                >
                    <ILChevrontL />
                </TouchableOpacity>
                <View>
                    <Text style={{ fontSize: 16 }} >Recomended for you</Text>
                </View>
                <View style={{ width: 50 }} />
            </View>
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={{ flex: 1, marginTop: 20, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
            >
                <View
                    style={{ padding: 20, paddingBottom: 50 }}
                >
                    {jobs && jobs.map(job => (
                        <JobCard
                            key={job._id}
                            navigation={navigation}
                            ILMoreVErtical={ILMoreVErtical}
                            job={job}
                        />
                    ))}
                    {loading && <ActivityIndicator color='#ff9901' size='small' style={{ marginTop: 20 }} />}
                    {!loading && jobs.length >= 5 && (
                        <TouchableOpacity
                            onPress={onPress}
                            style={{
                                marginTop: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(238, 238, 238, 0.5)', paddingVertical: 16, borderRadius: 50
                            }}
                        >
                            <Text style={{ color: '#ff9901' }} >Show More</Text>
                        </TouchableOpacity>
                    )}
                </View>
            </ScrollView>
        </SafeAreaView>
    )
};

export default InterestJobs;
