import React, { useState } from 'react';
import { Text, View, ScrollView, TextInput, TouchableOpacity, StyleSheet } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import auth from '@react-native-firebase/auth';
import { ILChevrontL, ILEye } from '../assets';
import { Loading } from '../components';
import instance from '../config/axios';
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell
} from 'react-native-confirmation-code-field';

const CELL_COUNT = 6;

const ForgetPassword = ({ navigation }) => {
    const [email, setEmail] = useState('')
    const [phoneNumber, setPhoneNumber] = useState('')
    const [otp, setOtp] = useState('')
    const [password, setPassword] = useState('')
    const [confirmPassword, setConfirmPassword] = useState('')
    const [matchPassword, setMatchPassword] = useState('')
    const [getConfirm, setConfirm] = useState(null)

    const [loading, setLoading] = useState(false)
    const [visibleFormPassword, setVisibleFormPassword] = useState(false)

    const [user, setUser] = useState({})

    const [secureTextEntry, setSecureTextEntry] = useState(true)
    const [secureTextEntry1, setSecureTextEntry1] = useState(true)

    const ref = useBlurOnFulfill({ value: otp, cellCount: CELL_COUNT });
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({
        value: otp,
        setValue: setOtp,
    });

    const validate = async () => {
        setLoading(true)
        try {
            let getPhoneNumber = '+62'
            if (phoneNumber[0] === '0') {
                getPhoneNumber += String(phoneNumber).substring(1, phoneNumber.length)
            } else getPhoneNumber = phoneNumber
            const form = {
                email: email,
                phoneNumber: getPhoneNumber
            }
            const { data } = await instance.post('/user/forget-password', form)
            if (data?.status === 404) throw data
            setUser(data.data)
            const confirm = await auth().verifyPhoneNumber(getPhoneNumber)
            if (confirm?.code) setOtp(confirm.code)

            setConfirm(confirm)
        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops!!! Somethingwrong',
                description: error.message,
                duration: 5000
            })
        }
        setLoading(false)
    }

    const confirmOtp = async () => {
        setLoading(true)
        try {
            const credential = auth.PhoneAuthProvider.credential(getConfirm.verificationId, otp)
            if (credential) {
                setVisibleFormPassword(true)
                setConfirm(null)
            } else throw { message: 'Invalid Credential' }
        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops!!! Somethingwrong',
                description: error.message,
                duration: 5000
            })
            setConfirm(null)
        }
        setLoading(false)
    }

    const changePassword = async () => {
        try {
            const form = {
                _id: user._id,
                password: password
            }
            const { data } = await instance.post('/user/forget-password', form)
            if (data.status === 200) {
                navigation.navigate('SignIn')
                showMessage({
                    type: 'success',
                    message: 'Success',
                    description: 'Change password'
                })
            } else throw { message: 'Error' }
        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops!!! Somethingwrong'
            })
        }
    }

    const onChangeConfirmPassword = (val) => {
        setConfirmPassword(val)
        if (val === password) setMatchPassword('Matched')
        else setMatchPassword('Not matched')
    }

    return (
        <>
            <View
                style={{ flex: 1, backgroundColor: '#ffff' }}
            >
                <View>
                    <View style={{ marginTop: 20, paddingBottom: 50, paddingHorizontal: 20, justifyContent: 'space-between', flexDirection: 'row' }} >
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                            style={{ width: 50 }}
                        >
                            <ILChevrontL />
                        </TouchableOpacity>
                        <View>
                            <Text style={{ fontSize: 16, color: 'black' }} >Forget Password</Text>
                        </View>
                        <View style={{ width: 50 }} />
                    </View>
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: 'rgba(238,238,238,0.3)' }}
                >
                    <View style={{ padding: 20 }} >
                        {!getConfirm && !visibleFormPassword && (
                            <>
                                <View>
                                    <TextInput
                                        placeholder="Your email / code"
                                        placeholderTextColor="#c4c4c4"
                                        style={{ backgroundColor: '#ffff', padding: 20, borderRadius: 11, borderWidth: 1, borderColor: '#eeee', color: 'black' }}
                                        value={email}
                                        onChangeText={val => setEmail(val)}
                                    />
                                </View>
                                <View style={{ marginTop: 20 }} >
                                    <TextInput
                                        placeholder="Your phone number"
                                        placeholderTextColor="#c4c4c4"
                                        style={{ backgroundColor: '#ffff', padding: 20, borderRadius: 11, borderWidth: 1, borderColor: '#eeee', color: 'black' }}
                                        value={phoneNumber}
                                        onChangeText={val => setPhoneNumber(val)}
                                    />
                                </View>
                            </>
                        )}
                        {visibleFormPassword && (
                            <>
                                <View style={{ flexDirection: 'row' }} >
                                    <TextInput
                                        placeholder="Password"
                                        placeholderTextColor="#c4c4c4"
                                        style={{ flex: 1, backgroundColor: '#ffff', padding: 20, borderTopLeftRadius: 11, borderBottomLeftRadius: 11, color: 'black' }}
                                        value={password}
                                        onChangeText={val => setPassword(val)}
                                        secureTextEntry={secureTextEntry}
                                    />
                                    <TouchableOpacity
                                        onPress={() => secureTextEntry ? setSecureTextEntry(false) : setSecureTextEntry(true)}
                                        style={{ padding: 20, borderTopRightRadius: 11, borderBottomRightRadius: 11, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffff' }}
                                    >
                                        <ILEye />
                                    </TouchableOpacity>
                                </View>
                                <View style={{ height: 30 }} >
                                    {matchPassword.length > 0 && (<Text style={{ color: matchPassword === 'Matched' ? '#2ecc71' : '#e74c3c', fontWeight: '700' }}>{matchPassword}</Text>)}
                                </View>
                                <View style={{ flexDirection: 'row', marginTop: 20 }} >
                                    <TextInput
                                        placeholder="Confirm Password"
                                        placeholderTextColor="#c4c4c4"
                                        style={{ flex: 1, backgroundColor: '#ffff', padding: 20, borderTopLeftRadius: 11, borderBottomLeftRadius: 11, color: 'black' }}
                                        value={confirmPassword}
                                        onChangeText={val => onChangeConfirmPassword(val)}
                                        secureTextEntry={secureTextEntry1}
                                    />
                                    <TouchableOpacity
                                        onPress={() => secureTextEntry1 ? setSecureTextEntry1(false) : setSecureTextEntry1(true)}
                                        style={{ padding: 20, borderTopRightRadius: 11, borderBottomRightRadius: 11, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ffff' }}
                                    >
                                        <ILEye />
                                    </TouchableOpacity>
                                </View>
                            </>
                        )}
                        {getConfirm && (
                            <View style={{ marginTop: 20 }} >
                                <CodeField
                                    ref={ref}
                                    {...props}
                                    value={otp}
                                    autoFocus
                                    onChangeText={setOtp}
                                    cellCount={6}
                                    rootStyle={styles.codeFieldRoot}
                                    keyboardType="number-pad"
                                    textContentType="oneTimeCode"
                                    renderCell={({ index, symbol, isFocused }) => (
                                        <Text
                                            key={index}
                                            style={[styles.cell, isFocused && styles.focusCell]}
                                            onLayout={getCellOnLayoutHandler(index)}>
                                            {symbol || (isFocused ? <Cursor /> : null)}
                                        </Text>
                                    )}
                                />
                            </View>
                        )}
                    </View>
                </ScrollView>
                <View style={{ backgroundColor: '#ffff', padding: 20 }} >
                    {!getConfirm && !visibleFormPassword && (
                        <TouchableOpacity
                            onPress={validate}
                            style={{ backgroundColor: '#ff9901', borderRadius: 11, padding: 20, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <Text style={{ color: '#ffff', fontWeight: '700' }} >Validate</Text>
                        </TouchableOpacity>
                    )}
                    {visibleFormPassword && matchPassword === 'Matched' && (
                        <TouchableOpacity
                            onPress={changePassword}
                            style={{ backgroundColor: '#ff9901', borderRadius: 11, padding: 20, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <Text style={{ color: '#ffff', fontWeight: '700' }} >Submit</Text>
                        </TouchableOpacity>
                    )}
                    {getConfirm && (
                        <TouchableOpacity
                            onPress={confirmOtp}
                            style={{ backgroundColor: '#ff9901', borderRadius: 11, padding: 20, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <Text style={{ color: '#ffff', fontWeight: '700' }} >Confirm OTP</Text>
                        </TouchableOpacity>
                    )}
                </View>
            </View>
            {loading && <Loading />}
        </>
    )
};

export default ForgetPassword;

const styles = StyleSheet.create({
    root: { flex: 1, padding: 20 },
    title: { textAlign: 'center', fontSize: 30 },
    codeFieldRoot: { marginTop: 20, backgroundColor: '#ffff', padding: 10, borderRadius: 11 },
    cell: {
        width: 40,
        height: 40,
        lineHeight: 38,
        fontSize: 24,
        borderWidth: 1,
        borderColor: '#eeee',
        textAlign: 'center',
    },
    focusCell: {
        borderColor: '#ff9901',
    },
});
