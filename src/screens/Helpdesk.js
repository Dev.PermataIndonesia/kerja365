import React, { useRef, useState } from 'react';
import { Text, View, ScrollView, Animated, TouchableOpacity, Dimensions, TextInput, ActivityIndicator } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import { useSelector } from 'react-redux';
import { ILChevrontR, ILGmail, ILInstagram, ILWorldWide, ILXsvg } from '../assets';
import instance from '../config/axios';

const FadeInView = (props) => {
    const fadeAnim = useRef(new Animated.Value(Dimensions.get('screen').height)).current

    React.useEffect(() => {

        if (!props?.opacity) {
            Animated.timing(
                fadeAnim,
                {
                    toValue: Dimensions.get('screen').height,
                    duration: 300,
                    useNativeDriver: true
                }
            ).start()
        } else {
            Animated.timing(
                fadeAnim,
                {
                    toValue: 0,
                    duration: 300,
                    useNativeDriver: true
                }
            ).start()
        }
    }, [fadeAnim, props.opacity])

    return (
        <Animated.View
            style={{
                ...props.style,
                transform: [{ translateY: fadeAnim }]
            }}
        >
            {props.children}
        </Animated.View>
    );
}

const Helpdesk = ({ navigation }) => {
    const token = useSelector(({ user }) => user.Token)

    const [opacity, setOpacity] = useState(false)
    const [question, setQuestion] = useState('')

    const [loading, setLoading] = useState(false)

    const submit = async () => {
        setLoading(true)
        try {
            if (!question) throw { message: 'Field required' }
            await instance.post('/user/question', { question }, {
                headers: {
                    access_token: token
                }
            })
            setOpacity(false)
            setQuestion('')
            showMessage({
                type: 'success',
                message: 'Pertanyaan anda akan segera kami respon',
                duration: 50000
            })
        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops!! something wrong'
            })
        }
        setLoading(false)
    }

    return (
        <>
            <View style={{ padding: 20 }} >
                <Text style={{ fontSize: 20 }} >Pusat Bantuan</Text>
                <View style={{ marginTop: 20 }} >
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, padding: 20, backgroundColor: '#ffff', borderTopLeftRadius: 11, borderTopRightRadius: 11, borderBottomRightRadius: 11 }} >
                        <View style={{ flex: 1 }} >
                            <ILInstagram />
                        </View>
                        <View style={{ flex: 5 }}>
                            <Text>@kerja365</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, padding: 20, backgroundColor: '#ffff', borderTopLeftRadius: 11, borderTopRightRadius: 11, borderBottomRightRadius: 11 }} >
                        <View style={{ flex: 1 }}>
                            <ILGmail />
                        </View>
                        <View style={{ flex: 5 }}>
                            <Text>admin@kerja365.com</Text>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, padding: 20, backgroundColor: '#ffff', borderTopLeftRadius: 11, borderTopRightRadius: 11, borderBottomRightRadius: 11 }} >
                        <View style={{ flex: 1 }}>
                            <ILWorldWide />
                        </View>
                        <View style={{ flex: 5 }}>
                            <Text>kerja365.com</Text>
                        </View>
                    </View>
                    <View style={{ marginTop: 10 }} >
                        <TouchableOpacity
                            onPress={() => opacity ? setOpacity(false) : setOpacity(true)}
                            style={{ padding: 20, backgroundColor: '#ffff', flexDirection: 'row', justifyContent: 'space-between', borderTopLeftRadius: 11, borderTopRightRadius: 11, borderBottomRightRadius: 11 }}
                        >
                            <Text style={{ fontWeight: '700' }} >Tanyakan kami sesuatu</Text>
                            <ILChevrontR />
                        </TouchableOpacity>

                    </View>
                </View>
            </View>
            <FadeInView style={{ position: 'absolute', backgroundColor: '#ffff', width: Dimensions.get('screen').width, height: Dimensions.get('screen').height }} opacity={opacity} >
                <View style={{ padding: 20 }} >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                        <Text>Tanyakan Sesuatu</Text>
                        <TouchableOpacity
                            onPress={() => setOpacity(false)}
                        >
                            <ILXsvg />
                        </TouchableOpacity>
                    </View>
                    <View style={{ marginTop: 20 }} >
                        <TextInput
                            style={{ padding: 10, color: 'black', backgroundColor: '#f5f6fa', height: 300, borderRadius: 5 }}
                            placeholder="Descripsi"
                            placeholderTextColor="#c4c4c4"
                            multiline={true}
                            textAlignVertical="top"
                            value={question}
                            onChangeText={val => setQuestion(val)}
                        />
                    </View>
                </View>
                <View style={{ padding: 20 }} >
                    {loading ? (
                        <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                            <ActivityIndicator size="large" color="#ff9901" />
                        </View>
                    ) : (
                        <TouchableOpacity
                            onPress={submit}
                            style={{ backgroundColor: '#ff9901', padding: 20, borderRadius: 11, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <Text style={{ color: '#ffff', fontWeight: '700' }} >Submit</Text>
                        </TouchableOpacity>
                    )}
                </View>
            </FadeInView>
        </>
    )
};

export default Helpdesk;
