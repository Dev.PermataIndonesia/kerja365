import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, ScrollView, Image } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import { useDispatch, useSelector } from 'react-redux';
import { ILChevrontL, ILMoreVErtical, ILEllipse4 } from '../../assets';
import Avatar from '../../assets/img/avatar.png'
import { Loading } from '../../components';
import instance from '../../config/axios';
import { fetchEmployeePermissions } from '../../store/reducer/employeeReducer';

const EmployeePermission = ({ navigation }) => {
    const dispatch = useDispatch()

    const token = useSelector(({ user }) => user.Token)
    const permissions = useSelector(({ employees }) => employees.Permissions)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        if (token) dispatch(fetchEmployeePermissions(token))
    }, [token])

    const action = async (item, status) => {
        setLoading(true)
        try {
            await instance.put(`/employee/permission/${item._id}`, { status }, {
                headers: {
                    access_token: token
                }
            })
            dispatch(fetchEmployeePermissions(token))
            showMessage({
                type: 'success',
                message: `Success ${status}`
            })
        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops! something error'
            })
        }
        setLoading(false)
    }

    return (
        <>
            <View style={{ flex: 1, backgroundColor: '#ffff' }} >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 50, paddingHorizontal: 20 }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={{ width: 50 }}
                    >
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 16 }} >Cuti Karyawan</Text>
                    </View>
                    <View style={{ width: 50 }} />
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
                >
                    {permissions?.inAction?.length > 0 && permissions?.inAction.map(item => (
                        <View
                            key={item._id}
                            style={{ padding: 20 }}
                        >
                            <View
                                style={{ backgroundColor: '#ffff', marginTop: 20, padding: 20, borderRadius: 10 }}
                            >
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
                                    <View>
                                        <Image source={Avatar} style={{ width: 60, height: 60, borderRadius: 60 / 2 }} />
                                    </View>
                                    <View style={{ flex: 1, paddingHorizontal: 10 }} >
                                        <Text style={{ fontSize: 16, fontWeight: '700' }} >{item?.user?.name}</Text>
                                        <Text style={{ fontSize: 12 }} >{item?.user?.position}</Text>
                                        <Text style={{ fontSize: 12 }}>{item?.user?.region}</Text>
                                    </View>
                                    <View>
                                        <ILMoreVErtical />
                                    </View>
                                </View>
                                <View style={{ marginTop: 30, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
                                    <View style={{ height: 50, width: 7, backgroundColor: '#E5E5E5', borderRadius: 10 }} />
                                    <View style={{ flex: 1, paddingHorizontal: 10 }} >
                                        <Text>{item?.desc}</Text>
                                        <Text>{new Date(item.createdAt).getDate()} - {new Date(item.createdAt).getMonth() + 1} - {new Date(item.createdAt).getFullYear()}</Text>
                                    </View>
                                    <View>
                                        <ILEllipse4 />
                                    </View>
                                </View>
                                <View style={{ marginTop: 20, flexDirection: 'row', justifyContent: 'flex-end' }} >
                                    <TouchableOpacity
                                        onPress={() => action(item, 'Approve')}
                                        style={{ width: 75, height: 30, alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderRadius: 5, borderColor: '#42B442' }} >
                                        <Text style={{ color: '#42B442' }} >Approve</Text>
                                    </TouchableOpacity>
                                    <View style={{ width: 10 }} />
                                    <TouchableOpacity
                                        onPress={() => action(item, 'Reject')}
                                        style={{ width: 75, height: 30, alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderRadius: 5, borderColor: '#C0392B' }} >
                                        <Text style={{ color: '#C0392B' }} >Reject</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                    ))}

                    {permissions?.alreadyAction?.length > 0 && permissions?.alreadyAction?.map(item => (
                        <View
                            key={item._id}
                            style={{ padding: 20 }}
                        >
                            <View
                                style={{ backgroundColor: '#ffff', padding: 20, borderRadius: 10 }}
                            >
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
                                    <View>
                                        <Image source={Avatar} style={{ width: 60, height: 60, borderRadius: 60 / 2 }} />
                                    </View>
                                    <View style={{ flex: 1, paddingHorizontal: 10 }} >
                                        <Text style={{ fontSize: 16, fontWeight: '700' }} >{item?.user?.name}</Text>
                                        <Text style={{ fontSize: 12 }} >{item?.user?.position}</Text>
                                        <Text style={{ fontSize: 12 }}>{item?.user?.region}</Text>
                                    </View>
                                    <View>
                                        <ILMoreVErtical />
                                    </View>
                                </View>
                                <View style={{ marginTop: 30, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
                                    <View style={{ height: 50, width: 7, backgroundColor: '#E5E5E5', borderRadius: 10 }} />
                                    <View style={{ flex: 1, paddingHorizontal: 10 }} >
                                        <Text>{item?.desc}</Text>
                                        <Text>{new Date(item.createdAt).getDate()} - {new Date(item.createdAt).getMonth() + 1} - {new Date(item.createdAt).getFullYear()}</Text>
                                    </View>
                                    <View>
                                        <View
                                            style={{ width: 75, height: 30, alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderRadius: 5, borderColor: item?.status === 'Approve' ? '#42B442' : '#C0392B' }} >
                                            <Text style={{ color: item?.status === 'Approve' ? '#42B442' : '#C0392B' }} >{item?.status}</Text>
                                        </View>
                                    </View>
                                </View>
                            </View>
                        </View>
                    ))}
                </ScrollView>
            </View>
            {loading && <Loading />}
        </>
    )
};

export default EmployeePermission;
