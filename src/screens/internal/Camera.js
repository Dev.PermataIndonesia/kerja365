import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, Image, TextInput, ScrollView, Dimensions } from 'react-native';
import { RNCamera } from 'react-native-camera';
import GeoLocation from 'react-native-get-location';
import instance from '../../config/axios';
import { showMessage } from 'react-native-flash-message';
import { useDispatch, useSelector } from 'react-redux';
import { fetchAttendance, fetchAttendances } from '../../store/reducer/attendanceReducer';

const Camera = ({ navigation, route }) => {
    const dispatch = useDispatch()

    const { status } = route.params

    const token = useSelector(({ user }) => user.Token)

    const [picture, setPicture] = useState(null)
    const [pictureURI, setPictureURI] = useState('')
    const [pictureBase64, setPictureBase64] = useState('')
    const [date, setDate] = useState(new Date())
    const [location, setLocation] = useState({})
    const [desc, setDesc] = useState('')

    useEffect(() => {
        GeoLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000
        })
            .then(res => {
                setLocation({
                    lng: res.longitude,
                    lat: res.latitude
                })
            })
            .catch(err => {
                showMessage({
                    type: 'warning',
                    message: 'Ooops! smoething wrong',
                    description: err.message,
                    duration: 300
                })
            })
    }, [])

    const takePicture = async () => {
        const data = await picture.takePictureAsync({ quality: 0.5, fixOrientation: true, height: 300, width: 200, base64: true })
        setPictureURI(data.uri)
        const base64 = `data:image/jpeg;base64, ${data.base64}`
        setPictureBase64(base64)
        setDate(new Date())
    };

    const goIn = async () => {
        try {
            let data
            if (status === "absent") {
                data = {
                    photo: pictureBase64,
                    dateIn: date,
                    lng: location.lng,
                    lat: location.lat,
                    desc: desc,
                    goIn: false,
                }
            } else {
                data = {
                    photo: pictureBase64,
                    dateIn: date,
                    lng: location.lng,
                    lat: location.lat,
                    goIn: true,
                }
            }

            await instance.post('/attendance', data, {
                headers: {
                    access_token: token
                }
            })
            dispatch(fetchAttendances(token))
            dispatch(fetchAttendance(token))
            showMessage({
                type: 'success',
                message: 'Sucess',
                description: 'Go in'
            })
            navigation.navigate('Activity')
        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops!! something wrong'
            })
        }
    }

    return (
        <View style={{ flex: 1, backgroundColor: "#ffff" }} >
            {pictureURI.length < 1 ? (
                <>
                    <RNCamera
                        ref={ref => {
                            setPicture(ref)
                        }}
                        captureAudio={false}
                        style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center', }}
                        type={RNCamera.Constants.Type.front}
                        flashMode={RNCamera.Constants.FlashMode.on}
                        androidCameraPermissionOptions={{
                            title: 'Permission to use camera',
                            message: 'We need your permission to use your camera',
                            buttonPositive: 'Ok',
                            buttonNegative: 'Cancel',
                        }}
                        androidRecordAudioPermissionOptions={{
                            title: 'Permission to use audio recording',
                            message: 'We need your permission to use your audio',
                            buttonPositive: 'Ok',
                            buttonNegative: 'Cancel',
                        }}
                    >
                        <View style={{ marginTop: 20 }} >
                            <Text style={{ color: "#ff9901", fontWeight: '700' }} >Posisikan wajah anda di area yang ditandai</Text>
                        </View>
                        <View style={{ width: Dimensions.get('screen').width - 100, height: 350, borderWidth: 3, borderColor: "#ff9901" }} />
                        <View style={{ padding: 20, flexDirection: 'row', justifyContent: 'center' }}>
                            <TouchableOpacity
                                onPress={takePicture}
                                style={{
                                    width: 70,
                                    height: 70,
                                    borderRadius: 70 / 2,
                                    borderWidth: 3,
                                    borderColor: '#ff9901',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                }}
                            >
                                <View
                                    style={{
                                        width: 50,
                                        height: 50,
                                        borderRadius: 50 / 2,
                                        backgroundColor: '#ff9901'
                                    }}
                                />
                            </TouchableOpacity>
                        </View>
                    </RNCamera>
                </>
            ) : (
                <View style={{ justifyContent: "space-between", flex: 1, backgroundColor: "rgba(238,238,238,0.3)", padding: 20 }} >
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        style={{ flex: 1, padding: 20 }}
                    >
                        <View>
                            <Image source={{ uri: pictureURI }} style={{ width: 250, height: 400 }} />
                            <View style={{ marginTop: 20 }} >
                                <Text>{date?.getHours()}:{date?.getMinutes()} , {date?.toLocaleDateString()}</Text>
                            </View>
                            {status === 'absent' && (
                                <View
                                    style={{ marginTop: 20 }}
                                >
                                    <TextInput
                                        placeholder="Deskripsi izin"
                                        placeholderTextColor="#eeee"
                                        style={{ padding: 20, height: 170, color: "black", borderRadius: 11, borderWidth: 1, borderColor: "#eeee", backgroundColor: "#ffff" }}
                                        value={desc}
                                        onChangeText={val => setDesc(val)}
                                        textAlignVertical="top"
                                    />
                                </View>
                            )}
                        </View>
                    </ScrollView>
                    <View>
                        <TouchableOpacity
                            onPress={goIn}
                            style={{ padding: 20, justifyContent: 'center', alignItems: 'center', borderRadius: 11, marginTop: 30, backgroundColor: '#ff9901' }}
                        >
                            <Text style={{ color: '#ffff', fontSize: 18 }} >Go In</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            )}
        </View>
    )
};

export default Camera;
