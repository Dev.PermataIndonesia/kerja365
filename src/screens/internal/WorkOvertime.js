import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, ScrollView, Dimensions } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import { useDispatch, useSelector } from 'react-redux';
import { ILChevrontL, ILMoreVErtical, ILPlus } from '../../assets';
import { AnimationSlideIn } from '../../components';
import instance from '../../config/axios';
import dateConvert from '../../helpers/dateConvert';
import { fetchOvertimes } from '../../store/reducer/workOvertimeReducer';


const WorkOvertime = ({ navigation }) => {
    const dispatch = useDispatch()

    const [opacity, setOpacity] = useState(false)
    const [detailData, setDetailData] = useState({})

    const token = useSelector(({ user }) => user.Token)
    const overtimes = useSelector(({ overtimes }) => overtimes.Overtimes)

    useEffect(() => {
        if (token) dispatch(fetchOvertimes(token))
    }, [token])

    const cancel = async () => {
        try {
            await instance.delete(`/work-overtime/${detailData._id}`, {
                headers: {
                    access_token: token
                }
            })
            console.log("here");
            setOpacity(false)
            setDetailData({})
            dispatch(fetchOvertimes(token))
            showMessage({
                type: 'success',
                message: 'Pengajuan dibatalkan'
            })
        } catch (error) {
            console.log(error.message, "<<< error");
            showMessage({
                type: 'warning',
                message: 'Oops!! something error'
            })
        }
    }

    return (
        <>
            <View style={{ flex: 1, backgroundColor: '#ffff' }} >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 50, paddingHorizontal: 20 }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={{ width: 50 }}
                    >
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 16 }} >Lembur</Text>
                    </View>
                    <View style={{ width: 50 }} />
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: 'rgba(238,238,238,0.5)' }}
                >
                    <View style={{ padding: 20 }} >
                        {overtimes?.length > 0 && overtimes?.map(item => (
                            <TouchableOpacity
                                onPress={() => {
                                    setOpacity(true)
                                    setDetailData(item)
                                }}
                                key={item._id}
                                style={{ padding: 20, backgroundColor: '#ffff', flexDirection: 'row', borderRadius: 11, marginTop: 20 }}
                            >
                                <View style={{ flex: 2, flexDirection: 'row' }} >
                                    <View style={{ width: 7, backgroundColor: '#E5E5E5', borderRadius: 10 }} />
                                    <View style={{ marginLeft: 7 }} >
                                        <Text style={{ fontWeight: '700' }} >{item?.title}</Text>
                                        <Text style={{ fontWeight: '400', fontSize: 12 }} >{new Date(item?.createdAt).getDate()} - {new Date(item?.createdAt).getMonth() + 1} - {new Date(item?.createdAt).getFullYear()}</Text>
                                    </View>
                                </View>
                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }} >
                                    <View style={{ borderWidth: 1, borderColor: item?.status === 'Approve' ? '#42B442' : '#C0392B', borderRadius: 7, padding: 5 }} >
                                        <Text style={{ color: item?.status === 'Approve' ? '#42B442' : '#C0392B' }} >{item?.status}</Text>
                                    </View>
                                    <ILMoreVErtical />
                                </View>
                            </TouchableOpacity>
                        ))}
                    </View>
                </ScrollView>
                <View style={{ alignItems: 'flex-end', padding: 20, backgroundColor: 'rgba(238, 238, 238, 0.3)' }} >
                    <TouchableOpacity
                        onPress={() => navigation.navigate('ApplyWorkOvertime')}
                        style={{ width: 60, height: 60, justifyContent: 'center', alignItems: 'center', borderRadius: 60 / 2, backgroundColor: '#ff9901' }}
                    >
                        <ILPlus />
                    </TouchableOpacity>
                </View>
            </View>
            <AnimationSlideIn
                style={{
                    position: 'absolute', backgroundColor: '#ffff', borderTopLeftRadius: 50, borderTopRightRadius: 50, justifyContent: 'space-between', width: Dimensions.get('screen').width, maxHeight: Dimensions.get('screen').height / 2, bottom: 0, shadowColor: "#000", shadowOffset: { width: 0, height: 3, }, shadowOpacity: 0.27, shadowRadius: 4.65, elevation: 6
                }}
                opacity={opacity}
            >
                {Object.keys(detailData).length > 0 && (
                    <View>
                        <TouchableOpacity
                            onPress={() => {
                                setDetailData({})
                                setOpacity(false)
                            }}
                            style={{ backgroundColor: detailData?.status === 'Approve' ? '#42B442' : '#C0392B', borderTopLeftRadius: 50, borderTopRightRadius: 50, jjustifyContent: 'center', alignItems: 'center', padding: 16 }} >
                            <View>
                                <Text style={{ color: '#ffff', fontWeight: '700' }} >Close</Text>
                            </View>
                        </TouchableOpacity>
                        <View style={{ padding: 20, flexDirection: 'row' }} >
                            <View style={{ flex: 1 }} >
                                <Text style={{ fontSize: 16 }} >{detailData?.title}</Text>
                                <Text style={{ fontSize: 14 }} >{detailData?.desc}</Text>
                                <Text style={{ fontSize: 12 }} >{dateConvert(new Date(detailData?.createdAt))}</Text>
                            </View>
                            {detailData?.status === 'Waiting' && (
                                <View>
                                    <TouchableOpacity
                                        onPress={cancel}
                                        style={{ backgroundColor: '#e74c3c', padding: 16, borderRadius: 11 }}
                                    >
                                        <Text style={{ color: '#ffff', textAlign: 'center' }} >Batalkan</Text>
                                    </TouchableOpacity>
                                </View>
                            )}
                        </View>
                    </View>
                )}
            </AnimationSlideIn>
        </>
    )
};

export default WorkOvertime;
