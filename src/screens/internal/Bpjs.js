import React, { useEffect, useState } from 'react';
import { ActivityIndicator } from 'react-native';
import { Text, View, TouchableOpacity, SafeAreaView, StyleSheet, ScrollView, ImageBackground, Platform } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import { useDispatch, useSelector } from 'react-redux';
import { ILChevrontL, ILBPJSY, ILLoan, ILEpaySlip, ILDownload } from '../../assets';
import BpjsCard from '../../assets/bpjs-card.png';
import instance from '../../config/axios';
import { fetchBpjs } from '../../store/reducer/bpjsReducer';
import RNFS from 'react-native-fs';
import SkeletonPlaceHolder from 'react-native-skeleton-placeholder';
import RNFetchBlob from 'rn-fetch-blob';

const Bpjs = ({ navigation }) => {
    const dispatch = useDispatch()

    const user = useSelector(({ user }) => user.User)
    const token = useSelector(({ user }) => user.Token)
    const bpjs = useSelector(({ bpjs }) => bpjs.Bpjs)
    const loadingBpjs = useSelector(({ bpjs }) => bpjs.Loading)

    const [loading, setLoading] = useState(false)

    useEffect(() => {
        if (token) dispatch(fetchBpjs(token))
    }, [token])

    const download = () => {
        setLoading(true)

        const fileName = `${user.nrk} - bpjs.pdf`

        let dirs = RNFetchBlob.fs.dirs
        const path = Platform.OS == 'ios' ? dirs.DocumentDir + '/' + fileName : `${RNFS.ExternalStorageDirectoryPath}/Download/` + fileName

        RNFetchBlob.fs.exists(path)
            .then(async exist => {
                try {
                    if (exist) {
                        showMessage({
                            type: "info",
                            message: 'File already exists',
                        })
                    } else {
                        await instance.post('/bpjs', bpjs, {
                            headers: {
                                access_token: token
                            }
                        })

                        await RNFetchBlob
                            .config({
                                fileCache: true,
                                appendExt: 'pdf',
                                title: fileName,
                                addAndroidDownloads: {
                                    useDownloadManager: true,
                                    description: 'File downloading',
                                    title: fileName,
                                    mime: 'application/pdf',
                                    notification: true,
                                    path: path,
                                }
                            })
                            .fetch('GET', `https://k365.permataindonesia.com/bpjs/file/${user.nrk} - bpjs.pdf`, {})

                        showMessage({
                            type: "success",
                            message: "Success download",
                        })
                    }
                } catch (error) {
                    showMessage({
                        type: 'warning',
                        message: 'Oops something wrong',
                    })
                }
            })

        setLoading(false)
    }

    return (
        <>
            <SafeAreaView
                style={{ flex: 1, backgroundColor: '#ffff' }}
            >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 50, paddingHorizontal: 20 }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={{ width: 50 }}
                    >
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 16 }} >BPJS</Text>
                    </View>
                    <View style={{ width: 50 }} />
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                >
                    <View style={{ paddingHorizontal: 20 }} >
                        <View style={{ flexDirection: 'row', paddingVertical: 10, justifyContent: 'space-between', borderRadius: 100, paddingHorizontal: 45, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Epayslip')}
                            >
                                <View style={{ alignItems: 'center' }}>
                                    <View style={{ height: 15 }}>
                                        <ILEpaySlip />
                                    </View>
                                    <Text style={styles.title} >Epayslip</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                            >
                                <View style={{ alignItems: 'center' }}>
                                    <View style={{ height: 15 }}>
                                        <ILBPJSY />
                                    </View>
                                    <Text style={[styles.title, { color: '#ff9901' }]} >BPJS</Text>
                                </View>
                            </TouchableOpacity>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Loan')}
                            >
                                <View style={{ alignItems: 'center' }}>
                                    <View style={{ height: 15 }}>
                                        <ILLoan />
                                    </View>
                                    <Text style={styles.title} >Loan</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ padding: 40 }} >
                        <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                            <Text>Pribadi</Text>
                            <View style={{ flex: 1, marginLeft: 20, height: 10, borderRadius: 10, backgroundColor: '#eeee' }} />
                        </View>
                        {/* <View style={{ padding: 20, justifyContent: 'center', alignItems: 'center' }} > */}
                        {/* <Image source={BpjsCard} /> */}
                        {loadingBpjs && (
                            <SkeletonPlaceHolder>
                                <View style={{ paddingHorizontal: 20, marginTop: 10, height: 200, borderRadius: 10 }} />
                            </SkeletonPlaceHolder>
                        )}
                        {bpjs?.pribadi && !loadingBpjs && (
                            <View style={{ height: 200 }} >
                                <ImageBackground source={BpjsCard} style={{ resizeMode: 'cover', marginTop: 10 }} >
                                    <View style={{ padding: 20, marginTop: 42 }} >
                                        <View style={{ flexDirection: 'row' }} >
                                            <Text style={{ flex: 2, fontSize: 12, fontWeight: '700', color: '#636e72' }} >{bpjs?.pribadi?.nomor_bpjs ? `${bpjs?.pribadi?.nomor_bpjs}` : '------------------'}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', paddingVertical: 2, marginTop: 10 }} >
                                            <Text style={{ flex: 1, fontSize: 11, fontWeight: '700', color: '#636e72' }} >Nama</Text>
                                            <Text style={{ flex: 2, fontSize: 11, fontWeight: '700', color: '#636e72' }} >: {bpjs?.pribadi?.nama}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', paddingVertical: 2 }} >
                                            <Text style={{ flex: 1, fontSize: 11, fontWeight: '700', color: '#636e72' }} >No KK</Text>
                                            <Text style={{ flex: 2, fontSize: 11, fontWeight: '700', color: '#636e72' }} >: {bpjs?.pribadi?.kartu_keluarga}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', paddingVertical: 2 }} >
                                            <Text style={{ flex: 1, fontSize: 11, fontWeight: '700', color: '#636e72' }} >Kelas</Text>
                                            <Text style={{ flex: 2, fontSize: 11, fontWeight: '700', color: '#636e72' }} >: {bpjs?.pribadi?.kelas}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', paddingVertical: 2 }} >
                                            <Text style={{ flex: 1, fontSize: 11, fontWeight: '700', color: '#636e72' }} >Faskes</Text>
                                            <Text style={{ flex: 2, fontSize: 11, fontWeight: '700', color: '#636e72' }} >: {bpjs?.pribadi?.faskes}</Text>
                                        </View>
                                    </View>
                                </ImageBackground>
                            </View>
                        )}
                        {/* </View> */}
                        <View style={{ flexDirection: 'row', marginTop: 50, alignItems: 'center' }} >
                            <Text>Keluarga</Text>
                            <View style={{ flex: 1, marginLeft: 20, height: 10, borderRadius: 10, backgroundColor: '#eeee' }} />
                        </View>
                        {loadingBpjs && (
                            <>
                                <SkeletonPlaceHolder>
                                    <View style={{ paddingHorizontal: 20, marginTop: 10, height: 200, borderRadius: 10 }} />
                                </SkeletonPlaceHolder>
                                <SkeletonPlaceHolder>
                                    <View style={{ paddingHorizontal: 20, marginTop: 10, height: 200, borderRadius: 10 }} />
                                </SkeletonPlaceHolder>
                            </>
                        )}
                        {!loadingBpjs && bpjs?.keluarga?.map((el, i) => (
                            <View key={i} style={{ height: 200 }} >
                                <ImageBackground source={BpjsCard} style={{ resizeMode: 'cover' }} >
                                    <View style={{ padding: 20, marginTop: 42 }} >
                                        <View style={{ flexDirection: 'row' }} >
                                            <Text style={{ flex: 2, fontSize: 12, fontWeight: '700', color: '#636e72' }} >{el.nomor_bpjs ? `: ${el.nomor_bpjs}` : '------------------'}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', paddingVertical: 2, marginTop: 10 }} >
                                            <Text style={{ flex: 1, fontSize: 11, fontWeight: '700', color: '#636e72' }} >Nama</Text>
                                            <Text style={{ flex: 2, fontSize: 11, fontWeight: '700', color: '#636e72' }} >: {el.nama}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', paddingVertical: 2 }} >
                                            <Text style={{ flex: 1, fontSize: 11, fontWeight: '700', color: '#636e72' }} >No KK</Text>
                                            <Text style={{ flex: 2, fontSize: 11, fontWeight: '700', color: '#636e72' }} >: {el.kartu_keluarga}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', paddingVertical: 2 }} >
                                            <Text style={{ flex: 1, fontSize: 11, fontWeight: '700', color: '#636e72' }} >Kelas</Text>
                                            <Text style={{ flex: 2, fontSize: 11, fontWeight: '700', color: '#636e72' }} >: {el.kelas}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', paddingVertical: 2 }} >
                                            <Text style={{ flex: 1, fontSize: 11, fontWeight: '700', color: '#636e72' }} >Faskes</Text>
                                            <Text style={{ flex: 2, fontSize: 11, fontWeight: '700', color: '#636e72' }} >: {el.faskes}</Text>
                                        </View>
                                    </View>
                                </ImageBackground>
                            </View>
                        ))}
                    </View>
                    {Object?.keys(bpjs)?.length > 1 && (
                        <>
                            {loading || loadingBpjs ? (
                                <ActivityIndicator color='#ff9901' />
                            ) : (
                                <View style={{ padding: 20 }} >
                                    <TouchableOpacity
                                        onPress={download}
                                        style={{ height: 70, backgroundColor: '#eeee', borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}
                                    >
                                        <ILDownload />
                                        <Text style={{ fontWeight: '700' }} >Download file</Text>
                                    </TouchableOpacity>
                                </View>
                            )}
                        </>
                    )}
                </ScrollView>
            </SafeAreaView>
        </>
    )
};

const styles = StyleSheet.create({
    title: {
        fontSize: 12,
        fontFamily: 'DMSans-Bold',
        marginTop: 15,
        color: 'black'
    }
})

export default Bpjs;
