import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, ScrollView, Image, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ILChevrontLW, ILChevrontR } from '../../assets';
import Avatar from '../../assets/img/avatar.png';
import AsyncStorage from '@react-native-async-storage/async-storage';
import auth from '@react-native-firebase/auth';
import { setToken } from '../../store/action/userAction';
import { Loading } from '../../components';

const ProfileInternal = ({ navigation }) => {
    const dispatch = useDispatch()

    const user = useSelector(({ user }) => user.User)
    const [loading, setLoading] = useState(false)

    const logout = () => {
        Alert.alert(
            'Are you sure ?',
            'Logout from application',
            [
                {
                    text: 'Yes',
                    onPress: () => {
                        setLoading(true)
                        dispatch(setToken(''))
                        auth().signOut()
                            .then(async () => {
                                await AsyncStorage.clear()
                                navigation.replace('SignIn')
                            })
                            .finally(() => setLoading(false))
                    }
                },
                {
                    text: 'No',
                    onPress: () => console.log('Cancel'),
                    style: "cancel"
                }
            ]
        )
    }

    return (
        <>
            <View
                style={{ flex: 1, backgroundColor: '#ffff' }}
            >
                <View style={{ backgroundColor: '#ff9901' }}>
                    <View style={{ marginTop: 20, paddingBottom: 50, paddingHorizontal: 20, justifyContent: 'space-between', flexDirection: 'row' }} >
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                            style={{ width: 50 }}
                        >
                            <ILChevrontLW />
                        </TouchableOpacity>
                        <View>
                            <Text style={{ fontSize: 16, color: '#ffff' }} >Profile</Text>
                        </View>
                        <View style={{ width: 50 }} />
                    </View>
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                >
                    <View style={{ backgroundColor: '#ff9901' }}>
                        <View style={{ paddingHorizontal: 20, justifyContent: 'center', alignItems: 'center', paddingBottom: 50 }} >
                            <Image source={user?.photo ? { uri: user?.photo } : Avatar} style={{ width: 60, height: 60, borderRadius: 60 / 2 }} />
                            <View style={{ marginTop: 16, alignItems: 'center' }} >
                                <Text style={{ color: '#ffff' }} >{user?.nama ? user?.nama : user?.name}</Text>
                                <Text style={{ color: '#ffff' }} >{user?.posisi ? user?.posisi : user?.position}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ padding: 20, marginTop: 30 }} >
                        <View>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                <View style={{ height: 42, width: 8, borderRadius: 10, backgroundColor: '#e5e5e5' }} />
                                <Text style={{ marginLeft: 13, fontWeight: '700', fontSize: 14 }} >Info Pekerjaan</Text>
                            </View>
                            <View style={{ paddingHorizontal: 20, marginTop: 31 }} >
                                <View style={{ flexDirection: 'row' }} >
                                    <Text style={{ flex: 1, fontSize: 12 }} >Regional</Text>
                                    <View style={{ flexDirection: 'row', flex: 1 }} >
                                        <Text>: </Text>
                                        <Text style={{ fontSize: 12 }} >{user?.regional ? user?.regional : user?.region}</Text>
                                    </View>
                                </View>
                                <View style={{ flexDirection: 'row' }} >
                                    <Text style={{ flex: 1, fontSize: 12 }} >Penempatan</Text>
                                    <View style={{ flexDirection: 'row', flex: 1 }} >
                                        <Text>: </Text>
                                        <Text style={{ fontSize: 12 }}>{user?.area} {user?.area ? ', ' : ''}{user?.client}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{ padding: 40 }} >
                                <View style={{ height: 1, backgroundColor: '#eeee' }} />
                            </View>
                        </View>
                        <View>
                            <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                <View style={{ height: 42, width: 8, borderRadius: 10, backgroundColor: '#e5e5e5' }} />
                                <Text style={{ marginLeft: 13, fontWeight: '700', fontSize: 14 }} >Biodata</Text>
                            </View>
                            <View style={{ paddingHorizontal: 20, marginTop: 31 }} >
                                <View style={{ flexDirection: 'row' }} >
                                    <Text style={{ flex: 1, fontSize: 12 }} >No Tlp / Hp</Text>
                                    <Text style={{ flex: 1, fontSize: 12 }} >: {user?.tlp ? user?.tlp : user?.phoneNumber}</Text>
                                </View>
                                <View style={{ flexDirection: 'row' }} >
                                    <Text style={{ fontSize: 12, flex: 1 }} >Alamat</Text>
                                    <View style={{ flexDirection: 'row', flex: 1 }} >
                                        <Text>: </Text>
                                        <Text style={{ fontSize: 12 }}>{user?.alamat ? user?.alamat : user?.address}</Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{ padding: 40 }} >
                                <View style={{ height: 1, backgroundColor: '#eeee' }} />
                            </View>
                        </View>
                        <View>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('ChangePassword')}
                                style={{ flexDirection: 'row', alignItems: 'center' }}
                            >
                                <View style={{ height: 42, width: 8, borderRadius: 10, backgroundColor: '#e5e5e5' }} />
                                <Text style={{ marginLeft: 13, flex: 1, fontWeight: '700', fontSize: 14 }} >Change Password</Text>
                                <View style={{ flex: 1, marginLeft: 50 }} >
                                    <ILChevrontR />
                                </View>
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginTop: 50 }} >
                            <TouchableOpacity
                                onPress={logout}
                                style={{ flexDirection: 'row', alignItems: 'center' }}
                            >
                                <View style={{ height: 42, width: 8, borderRadius: 10, backgroundColor: '#e5e5e5' }} />
                                <Text style={{ marginLeft: 13, flex: 1, fontWeight: '700', fontSize: 14 }} >Logout</Text>
                                <View style={{ flex: 1, marginLeft: 50 }} >
                                    <ILChevrontR />
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </View >
            {loading && <Loading />}
        </>
    )
};

export default ProfileInternal;
