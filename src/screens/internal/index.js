import HomeInternal from './Home'
import Epayslip from './Epayslip'
import EpayslipDetails from './EpayslipDetails'
import Bpjs from './Bpjs'
import Loan from './Loan'
import ProfileInternal from './Profile'
import Activity from './Activity'
import Camera from './Camera'
import DetailAttendance from './DetailAttendance'
import Permission from './Permission'
import ApplyPermission from './ApplyPermission'
import ListEmployee from './ListEmployee'
import EmployeeActivity from './EmployeeActivity'
import EmployeeAttendance from './EmployeeAttendance'
import EmployeeOffWork from './EmployeeOffWork'
import EmployeeOvertime from './EmployeeOvertime'
import EmployeePermission from './EmployeePermission'
import AttendanceRecap from './AttendanceRecap'
import ApplyWorkOvertime from './ApplyWorkOvertime'
import WorkOvertime from './WorkOvertime'
import ActivityList from './ActivityList'
import InsertActivity from './InsertActivity'
import DetailAttendanceEmployee from './DetailAttendanceEmployee'

export {
    DetailAttendanceEmployee,
    InsertActivity,
    ActivityList,
    WorkOvertime,
    ApplyWorkOvertime,
    AttendanceRecap,
    EmployeePermission,
    EmployeeOvertime,
    EmployeeOffWork,
    EmployeeAttendance,
    EmployeeActivity,
    ListEmployee,
    ApplyPermission,
    Permission,
    DetailAttendance,
    Camera,
    Activity,
    ProfileInternal,
    Loan,
    Bpjs,
    EpayslipDetails,
    HomeInternal,
    Epayslip
}
