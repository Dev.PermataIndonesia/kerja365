import React, { useEffect, useRef, useState } from 'react';
import { Text, View, TextInput, TouchableOpacity, StyleSheet, ScrollView, Animated, Dimensions } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import { ILChevrontL, ILBPJS, ILLoanY, ILEpaySlip, ILMoreVErtical } from '../../assets';
import { Slider } from 'react-native-elements';
import NumberFormat from 'react-number-format';
import { ActivityIndicator } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import { useDispatch, useSelector } from 'react-redux';
import { fetchLoanHistory, fetchManFeePlafon } from '../../store/reducer/loanReducer';
import instance from '../../config/axios';

const initialLayout = { width: Dimensions.get('window').width };

const Loan = ({ navigation }) => {
    const dispatch = useDispatch()

    const [plafon, setPlafon] = useState(1000000)
    const [select, setSelected] = useState(1)
    const [cost, setCost] = useState(true)
    const [modal, setModal] = useState(false)
    const [loading, setLoading] = useState(false)
    const [password, setPassword] = useState('')
    const [cicilan, setCicilan] = useState(0)
    const [detailHistory, setDetailHistory] = useState(null)

    const user = useSelector(({ user }) => user.User)
    const token = useSelector(({ user }) => user.Token)
    const loan = useSelector(({ loan }) => loan.ManFeePlafon)
    const loanHistory = useSelector(({ loan }) => loan.LoanHistory)
    const loanLoading = useSelector(({ loan }) => loan.Loading)

    const [routes] = useState([
        { key: 'first', title: 'Submission' },
        { key: 'third', title: 'History' }
    ])
    const [index, setIndex] = useState(0)

    const opacity = useRef(new Animated.Value(0)).current
    const [months, setMonths] = useState(['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'])

    useEffect(() => {
        const today = new Date()
        const day = today.getDate()
        const month = today.getMonth()
        const year = today.getFullYear()
        if (token && user.schema) {
            dispatch(fetchManFeePlafon(`${day}${months[month]}${year}`, select, user.schema._id, token))
            dispatch(fetchLoanHistory(token))
        }
    }, [token])

    useEffect(() => {
        setCicilan(plafon?.toFixed() / select)
    }, [loan?.plafonAvailable, plafon])

    const fadeIn = () => {
        Animated.timing(opacity, {
            toValue: 1,
            duration: 300,
            useNativeDriver: true
        }).start()
    }

    const fadeOut = () => {
        Animated.timing(opacity, {
            toValue: 0,
            duration: 300,
            useNativeDriver: true
        }).start()
        setTimeout(() => {
            setModal(false)
        }, 300)
    }

    const onValueChange = () => {
        if (plafon === loan.plafonAvailable) setPlafon(loan.plafonAvailable)
        else {
            let value = String(plafon.toFixed())
            let toFloat = value[0] += value[1] += '.'
            let float = toFloat += value.substring(1, value.length)
            if (float.substring(3, 4) !== '5') {
                setPlafon(Number(float).toFixed() * 100000)
                setCicilan(Number(float).toFixed() * 100000 / select)
            } else {
                let sub = String(value).substring(0, 2)
                sub += '00000'
                setPlafon(Number(sub))
                setCicilan(Number(sub) / select)
            }
        }

        setCost(true)
    }

    const onSlide = (val) => {
        setCost(false)
        setPlafon(val)
    }

    const onPress = () => {
        setTimeout(() => {
            setModal(true)
        }, 250)
        fadeIn()
    }

    const selectTenor = (tenor) => {
        const today = new Date()
        const day = today.getDate()
        const month = today.getMonth()
        const year = today.getFullYear()
        setSelected(tenor)
        setPlafon(1000000)
        dispatch(fetchManFeePlafon(`${day}${months[month]}${year}`, tenor, user.schema._id, token))
    }

    const login = async () => {
        setLoading(true)
        try {
            const userLogin = {
                email: user.nrk,
                password: password,
                phoneNumber: user.tlp
            }
            const { data } = await instance.post('/user/login', userLogin)
            if (data?.type === 'error') {
                throw { message: 'Wrong password' }
            }
            if (data?.token) {
                const loanSubmission = {
                    besarPinjaman: plafon,
                    tenor: select,
                    cicilanPerbulan: +cicilan.toFixed(),
                    keterangan: "Dengan ini saya setuju. Pengembalian pinjaman akan mulai dipotong dari gaji di bulan berjalan pengajuan.",
                    adminFee: loan?.manfee * 100 / 100 * plafon,
                    jumlahPinjamanDiterima: plafon - loan?.manfee * 100 / 100 * plafon
                }
                await instance.post('/loan/submission', loanSubmission, {
                    headers: {
                        access_token: token
                    }
                })
                setLoading(false)
                fadeOut()
                showMessage({
                    type: 'success',
                    message: 'Success',
                    description: 'Pengajuan berhasil, pengajuan anda akan diproses segera'
                })
            }
            setPassword('')
        } catch (error) {
            setLoading(false)
            setPassword('')
            fadeOut()
            showMessage({
                type: 'warning',
                message: 'Oops! something wrong',
                description: error.message
            })
        }
    }

    const renderTabBar = props => {
        return (
            <View style={{ paddingHorizontal: 20, flex: 1, backgroundColor: '#ffff' }} >
                <TabBar {...props}
                    style={{
                        elevation: 0, paddingBottom: 31, backgroundColor: '#ffff'
                    }}
                    inactiveColor='black'
                    activeColor='black'
                    renderLabel={({ route }) => (
                        <Text style={{ color: 'black', fontFamily: 'DMSans-Bold' }} >{route.title}</Text>
                    )}
                    labelStyle={{
                        fontSize: 14, fontWeight: '700'
                    }}
                    indicatorStyle={{
                        backgroundColor: '#FF9901', height: 8, borderRadius: 10, marginBottom: 20
                    }}
                />
            </View>
        )
    }

    return (
        <>
            <View
                style={{ flex: 1, backgroundColor: '#ffff' }}
            >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 50, paddingHorizontal: 20 }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={{ width: 50 }}
                    >
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 16 }} >Loan</Text>
                    </View>
                    <View style={{ width: 50 }} />
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ paddingVertical: 0, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
                >
                    <View style={{ backgroundColor: '#ffff' }} >
                        <View style={{ padding: 20 }} >
                            <View style={{ flexDirection: 'row', paddingVertical: 10, justifyContent: 'space-between', borderRadius: 100, paddingHorizontal: 45, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}>
                                <TouchableOpacity
                                    onPress={() => navigation.navigate('Epayslip')}
                                >
                                    <View style={{ alignItems: 'center' }}>
                                        <View style={{ height: 15 }}>
                                            <ILEpaySlip />
                                        </View>
                                        <Text style={styles.title} >Epayslip</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => navigation.push('Bpjs')}
                                >
                                    <View style={{ alignItems: 'center' }}>
                                        <View style={{ height: 15 }}>
                                            <ILBPJS />
                                        </View>
                                        <Text style={styles.title} >BPJS</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                >
                                    <View style={{ alignItems: 'center' }}>
                                        <View style={{ height: 15 }}>
                                            <ILLoanY />
                                        </View>
                                        <Text style={[styles.title, { color: '#ff9901' }]} >Loan</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <View>
                        <TabView
                            navigationState={{ index, routes }}
                            renderScene={() => null}
                            onIndexChange={setIndex}
                            initialLayout={initialLayout}
                            renderTabBar={renderTabBar}
                        />
                        {index === 0 && (
                            <View style={{ padding: 40 }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                    <Text style={{ fontWeight: '700' }} >Info Pinjaman</Text>
                                    <View style={{ flex: 1, marginLeft: 20, height: 10, borderRadius: 10, backgroundColor: '#eeee' }} />
                                </View>
                                <View style={{ marginTop: 20, alignItems: 'center', justifyContent: 'center' }} >
                                    <Text>{loan?.message ? loan?.message : 'Tidak ada pinjaman yang tersedia'}</Text>
                                </View>
                                {loan.status && (
                                    <>
                                        <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center' }} >
                                            <Text style={{ fontWeight: '700' }} >Plafon Tersedia</Text>
                                            <View style={{ flex: 1, marginLeft: 20, height: 10, borderRadius: 10, backgroundColor: '#eeee' }} />
                                        </View>
                                        <View style={{ alignItems: 'center', justifyContent: 'center', marginTop: 30 }} >
                                            {loanLoading ? (
                                                <ActivityIndicator size='large' color='#ff9901' />
                                            ) : (
                                                <NumberFormat
                                                    value={loan?.plafonAvailable}
                                                    thousandSeparator={true}
                                                    displayType={'text'}
                                                    renderText={val => (
                                                        <Text style={{ fontSize: 30, color: '#088E6B' }} >Rp. {val}</Text>
                                                    )}
                                                />
                                            )}
                                            <View style={{ marginTop: 40 }} >
                                                <Slider
                                                    value={plafon}
                                                    onValueChange={onSlide}
                                                    minimumValue={1000000}
                                                    maximumValue={loan?.plafonAvailable}
                                                    trackStyle={{ width: Dimensions.get('screen').width - 80, height: 10, borderRadius: 10 }}
                                                    thumbStyle={{ height: 20, width: 20, backgroundColor: '#ff9901' }}
                                                    maximumTrackTintColor='#eeee'
                                                    minimumTrackTintColor='#ff9901'
                                                    onSlidingComplete={onValueChange}
                                                />
                                            </View>
                                        </View>
                                        <View style={{ marginTop: 20 }} >
                                            <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center' }} >
                                                <Text style={{ flex: 2 }} >Jumlah pengajuan</Text>
                                                {/* <Text style={{ flex: 1 }} >Rp. 4500.000</Text> */}
                                                <NumberFormat
                                                    value={plafon?.toFixed()}
                                                    thousandSeparator={true}
                                                    displayType={'text'}
                                                    renderText={val => (
                                                        <Text style={{ flex: 1, color: 'black' }} >Rp. {val}</Text>
                                                    )}
                                                />
                                            </View>
                                            <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center' }} >
                                                <Text style={{ flex: 2 }} >Biaya {loan?.manfee ? loan.manfee * 100 : ''}%</Text>
                                                {cost ? (
                                                    <>
                                                        {loan?.manfee ? (
                                                            <NumberFormat
                                                                value={loan?.manfee * 100 / 100 * plafon}
                                                                thousandSeparator={true}
                                                                displayType={'text'}
                                                                renderText={val => (
                                                                    <Text style={{ flex: 1, color: 'black' }} >Rp. {val}</Text>
                                                                )}
                                                            />
                                                        ) : (
                                                            <Text style={{ flex: 1 }}>Rp -</Text>
                                                        )}
                                                    </>
                                                ) : (
                                                    <ActivityIndicator color='#ff9901' style={{ flex: 1 }} />
                                                )}
                                            </View>
                                            <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center' }} >
                                                <Text style={{ flex: 2 }} >Nominal diterima</Text>
                                                {cost ? (
                                                    <>
                                                        {loan?.manfee ? (
                                                            <NumberFormat
                                                                value={plafon - loan?.manfee * 100 / 100 * plafon}
                                                                thousandSeparator={true}
                                                                displayType={'text'}
                                                                renderText={val => (
                                                                    <Text style={{ flex: 1, color: '#ff9901' }} >Rp. {val}</Text>
                                                                )}
                                                            />
                                                        ) : (
                                                            <Text style={{ flex: 1 }}>Rp -</Text>
                                                        )}
                                                    </>
                                                ) : (
                                                    <ActivityIndicator color='#ff9901' style={{ flex: 1 }} />
                                                )}
                                            </View>
                                        </View>

                                        <View style={{ marginTop: 30 }} >
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                                <Text style={{ fontWeight: '700' }} >Jangka Waktu</Text>
                                                <View style={{ flex: 1, marginLeft: 20, height: 10, borderRadius: 10, backgroundColor: '#eeee' }} />
                                            </View>
                                            <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center', justifyContent: 'space-between', flexWrap: 'wrap' }} >
                                                {loan?.tenors?.map(el => (
                                                    <View
                                                        key={el._id}
                                                        style={{ flexDirection: 'row', alignItems: 'center' }}
                                                    >
                                                        <TouchableOpacity
                                                            onPress={() => selectTenor(1)}
                                                            style={{ width: 15, height: 15, borderRadius: 15 / 2, backgroundColor: select === 1 ? '#ff9901' : '#eeee' }}
                                                        >
                                                        </TouchableOpacity>
                                                        <Text style={{ marginLeft: 10 }} >{el.tenor} Bulan</Text>
                                                    </View>
                                                ))}
                                            </View>
                                        </View>

                                        <View style={{ marginTop: 30 }} >
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                                <Text style={{ fontWeight: '700' }} >Jumlah Cicilan</Text>
                                                <View style={{ flex: 1, marginLeft: 20, height: 10, borderRadius: 10, backgroundColor: '#eeee' }} />
                                            </View>
                                            <View style={{ marginTop: 20, alignItems: 'flex-end' }} >
                                                {!cost ? (
                                                    <ActivityIndicator color='#ff9901' />
                                                ) : (
                                                    <NumberFormat
                                                        value={cicilan.toFixed()}
                                                        thousandSeparator={true}
                                                        displayType={'text'}
                                                        renderText={val => (
                                                            <Text style={{ fontSize: 16, color: '#088E6B' }} >Rp. {val}</Text>
                                                        )}
                                                    />
                                                )}
                                            </View>
                                        </View>

                                        <View style={{ marginTop: 30 }} >
                                            <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                                <Text style={{ fontWeight: '700' }} >Ketentuan</Text>
                                                <View style={{ flex: 1, marginLeft: 20, height: 10, borderRadius: 10, backgroundColor: '#eeee' }} />
                                            </View>
                                            <View style={{ marginTop: 30 }} >
                                                <Text style={{ textAlign: 'center' }} >Dengan ini anda setuju.</Text>
                                                <Text style={{ textAlign: 'center' }} >Pengembalian pinjaman akan mulai dipotong dari gaji di bulan berjalan pengajuan.</Text>
                                            </View>
                                        </View>
                                        <View style={{ padding: 20, marginTop: 10 }} >
                                            <TouchableOpacity
                                                onPress={onPress}
                                                style={{ height: 60, backgroundColor: '#eeee', borderRadius: 50, justifyContent: 'center', alignItems: 'center' }}
                                            >
                                                <Text style={{ fontWeight: '700' }} >Ajukan pinjaman</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </>
                                )}
                                {!loan?.status && loan?.data && (
                                    <View style={{ marginTop: 30 }} >
                                        <View style={{ flexDirection: 'row', marginTop: 10 }} >
                                            <Text style={{ flex: 1 }} >Tanggal pengajuan</Text>
                                            <Text style={{ flex: 1 }} >{loan?.data?.tanggal_pengajuan?.substring(0, 2)} {months[+loan?.data?.tanggal_pengajuan?.substring(3, 5) - 1]} {loan?.data?.tanggal_pengajuan?.substring(6, 10)}</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', marginTop: 10 }} >
                                            <Text style={{ flex: 1 }} >Jumlah pengajuan</Text>
                                            <NumberFormat
                                                value={loan?.data?.besar_pinjaman}
                                                thousandSeparator={true}
                                                displayType={'text'}
                                                renderText={val => (
                                                    <Text style={{ flex: 1, color: 'black' }} >Rp. {val}</Text>
                                                )}
                                            />
                                        </View>
                                        <View style={{ flexDirection: 'row', marginTop: 10 }} >
                                            <Text style={{ flex: 1 }} >Biaya</Text>
                                            <NumberFormat
                                                value={loan?.data?.admin_fee}
                                                thousandSeparator={true}
                                                displayType={'text'}
                                                renderText={val => (
                                                    <Text style={{ flex: 1, color: 'black' }} >Rp. {val}</Text>
                                                )}
                                            />
                                        </View>
                                        <View style={{ flexDirection: 'row', marginTop: 10 }} >
                                            <Text style={{ flex: 1 }} >Nominal diterima</Text>
                                            <NumberFormat
                                                value={loan?.data?.jumlah_pinjaman_diterima}
                                                thousandSeparator={true}
                                                displayType={'text'}
                                                renderText={val => (
                                                    <Text style={{ flex: 1, color: '#ff9901', fontWeight: '700' }} >Rp. {val}</Text>
                                                )}
                                            />
                                        </View>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 30 }} >
                                            <Text style={{ fontWeight: '700' }} >Jangka waktu</Text>
                                            <View style={{ flex: 1, marginLeft: 20, height: 10, borderRadius: 10, backgroundColor: '#eeee' }} />
                                        </View>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 30 }} >
                                            <View style={{ width: 15, height: 15, borderRadius: 15 / 2, backgroundColor: '#ff9901' }} />
                                            <Text style={{ marginLeft: 17 }} >{loan?.data?.tenor} Bulan</Text>
                                        </View>
                                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center', marginTop: 30 }} >
                                            {months.map((month, i) => (
                                                <View key={i}>
                                                    {i < +loan?.data?.tenor && (
                                                        <View style={{ borderColor: '#ff9901', marginLeft: 10, marginTop: 5, borderWidth: 1, borderRadius: 5, padding: 7 }} >
                                                            <Text style={{ color: '#ff9901' }} >{months[+String(loan?.data?.tanggal_pengajuan).substring(3, 5) + i]}</Text>
                                                        </View>
                                                    )}
                                                </View>
                                            ))}
                                        </View>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginTop: 30 }} >
                                            <Text style={{ fontWeight: '700' }} >Ketentuan</Text>
                                            <View style={{ flex: 1, marginLeft: 20, height: 10, borderRadius: 10, backgroundColor: '#eeee' }} />
                                        </View>
                                        <View style={{ marginTop: 39 }} >
                                            <Text style={{ textAlign: 'center' }} >Dengan ini anda setuju. Pengembalian pinjaman akan mulai dipotong dari gaji di bulan berjalan pengajuan.</Text>
                                        </View>
                                    </View>
                                )}
                            </View>
                        )}
                        {index === 1 && (
                            <View style={{ padding: 20 }} >
                                {!detailHistory ? (
                                    <>
                                        {loanHistory?.length < 1 && (
                                            <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                                                <Text>Belum ada history peminjaman</Text>
                                            </View>
                                        )}
                                        {loanHistory && loanHistory?.map((el, i) => (
                                            <TouchableOpacity
                                                key={i}
                                                onPress={() => setDetailHistory(el)}
                                                style={{ flexDirection: 'row', padding: 20, backgroundColor: '#ffff', marginTop: 20, borderRadius: 11, justifyContent: 'space-between' }}
                                            >
                                                <Text style={{ flex: 2, fontWeight: '700' }} >{el?.status_pinjaman}</Text>
                                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }} >
                                                    <Text style={{ fontSize: 12 }} >{el?.tanggal_pengajuan}</Text>
                                                    <ILMoreVErtical />
                                                </View>
                                            </TouchableOpacity>
                                        ))}
                                    </>
                                ) : (
                                    <View>
                                        {detailHistory && (
                                            <>
                                                <View style={{ flexDirection: 'row' }} >
                                                    <Text style={{ flex: 1 }} >Besar Pinjaman</Text>
                                                    <NumberFormat
                                                        value={detailHistory?.besar_pinjaman}
                                                        thousandSeparator={true}
                                                        displayType={'text'}
                                                        renderText={val => (
                                                            <Text style={{ flex: 1 }} >: Rp. {val}</Text>
                                                        )}
                                                    />
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 20 }} >
                                                    <Text style={{ flex: 1 }} >Tenor</Text>
                                                    <Text style={{ flex: 1 }} >: {detailHistory?.tenor}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 20 }} >
                                                    <Text style={{ flex: 1 }} >Cicilan Perbulan</Text>
                                                    <NumberFormat
                                                        value={detailHistory?.cicilan_perbulan}
                                                        thousandSeparator={true}
                                                        displayType={'text'}
                                                        renderText={val => (
                                                            <Text style={{ flex: 1 }} >: Rp. {val}</Text>
                                                        )}
                                                    />
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 20 }} >
                                                    <Text style={{ flex: 1 }} >Status</Text>
                                                    <Text style={{ flex: 1 }} >: {detailHistory?.status_pinjaman}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 20 }} >
                                                    <Text style={{ flex: 1 }} >Keterangan</Text>
                                                    <Text style={{ flex: 1 }} >: {detailHistory?.keterangan}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 20 }} >
                                                    <Text style={{ flex: 1 }} >Tanggan Pengajuan</Text>
                                                    <Text style={{ flex: 1 }} >: {detailHistory?.tanggal_pengajuan}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 20 }} >
                                                    <Text style={{ flex: 1 }} >Tanggal Approve</Text>
                                                    <Text style={{ flex: 1 }} >: {detailHistory?.tanggal_approved}</Text>
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 20 }} >
                                                    <Text style={{ flex: 1 }} >Pinjaman Diterima</Text>
                                                    <NumberFormat
                                                        value={detailHistory?.jumlah_pinjaman_diterima}
                                                        thousandSeparator={true}
                                                        displayType={'text'}
                                                        renderText={val => (
                                                            <Text style={{ flex: 1 }} >: Rp. {val}</Text>
                                                        )}
                                                    />
                                                </View>
                                                <View style={{ flexDirection: 'row', marginTop: 20 }} >
                                                    <Text style={{ flex: 1 }} >Biaya Admin</Text>
                                                    <NumberFormat
                                                        value={detailHistory?.admin_fee}
                                                        thousandSeparator={true}
                                                        displayType={'text'}
                                                        renderText={val => (
                                                            <Text style={{ flex: 1 }} >: Rp. {val}</Text>
                                                        )}
                                                    />
                                                </View>
                                                <TouchableOpacity
                                                    onPress={() => setDetailHistory(null)}
                                                    style={{ height: 50, marginTop: 50, padding: 10, alignItems: 'center', justifyContent: 'center', borderRadius: 11, backgroundColor: '#e74c3c' }}
                                                >
                                                    <Text style={{ color: '#ffff', fontWeight: '700' }} >Close</Text>
                                                </TouchableOpacity>
                                            </>
                                        )}
                                    </View>
                                )}
                            </View>
                        )}
                    </View>
                </ScrollView>
            </View>
            {modal && (
                <Animated.View
                    style={{ position: 'absolute', width: Dimensions.get('screen').width, height: Dimensions.get('screen').height, opacity: opacity, backgroundColor: 'rgba(0, 0, 0, 0.5)', justifyContent: 'center', alignItems: 'center' }}
                >
                    <View style={{ width: Dimensions.get('screen').width - 80, height: 270, backgroundColor: '#ffff', borderRadius: 30 }} >
                        <View style={{ padding: 20 }} >
                            <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                <Text>Login Password</Text>
                                <View style={{ marginLeft: 10, borderRadius: 10, backgroundColor: '#ff9901', height: 10, flex: 1 }} />
                            </View>
                            <View style={{ marginTop: 50 }} >
                                <TextInput
                                    value={password}
                                    onChangeText={val => setPassword(val)}
                                    style={{ borderColor: '#eeee', borderBottomWidth: 2, color: 'black' }}
                                    secureTextEntry={true}
                                />
                            </View>
                            <View style={{ marginTop: 20 }} >
                                {loading ? (
                                    <ActivityIndicator
                                        color='#ff9901' size='small'
                                    />
                                ) : (
                                    <TouchableOpacity
                                        onPress={login}
                                        style={{ backgroundColor: '#eeee', padding: 30, borderRadius: 50, alignItems: 'center', justifyContent: 'center' }}
                                    >
                                        <Text>Process</Text>
                                    </TouchableOpacity>
                                )}
                            </View>
                        </View>
                    </View>
                </Animated.View>
            )}
        </>
    )
};

const styles = StyleSheet.create({
    title: {
        fontSize: 12,
        fontFamily: 'DMSans-Bold',
        marginTop: 15,
        color: 'black'
    }
})

export default Loan;
