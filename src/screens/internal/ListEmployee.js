import React from 'react';
import { Text, View, TouchableOpacity, ScrollView, Image } from 'react-native';
import { useSelector } from 'react-redux';
import { ILChevrontL, ILMoreVErtical } from '../../assets';
import Avatar from '../../assets/img/avatar.png'

const ListEmployee = ({ navigation }) => {
    const employees = useSelector(({ employees }) => employees.Employees.All)

    return (
        <View style={{ flex: 1, backgroundColor: '#ffff' }} >
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 50, paddingHorizontal: 20 }}>
                <TouchableOpacity
                    onPress={() => navigation.goBack()}
                    style={{ width: 50 }}
                >
                    <ILChevrontL />
                </TouchableOpacity>
                <View>
                    <Text style={{ fontSize: 16 }} >Daftar Karyawan</Text>
                </View>
                <View style={{ width: 50 }} />
            </View>
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
            >
                <View style={{ paddingHorizontal: 20 }} >
                    {employees && employees?.map(item => (
                        <View
                            key={item._id}
                            style={{ backgroundColor: '#ffff', padding: 20, marginTop: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', borderRadius: 10 }}
                        >
                            <View>
                                <Image source={item?.photo ? { uri: item?.photo } : Avatar} style={{ width: 60, height: 60, borderRadius: 60 / 2 }} />
                            </View>
                            <View style={{ flex: 1, paddingHorizontal: 10 }} >
                                <Text style={{ fontSize: 14, fontWeight: '700' }} >{item?.name}</Text>
                                <Text style={{ fontSize: 11 }} >{item?.position}</Text>
                                <Text style={{ fontSize: 11 }}>{item?.region}</Text>
                            </View>
                            <View>
                                <ILMoreVErtical />
                            </View>
                        </View>
                    ))}
                </View>
            </ScrollView>
        </View>
    )
};

export default ListEmployee;
