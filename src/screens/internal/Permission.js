import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, ScrollView, Dimensions, ActivityIndicator } from 'react-native';
import { ILChevrontL, ILMoreVErtical, ILPlus } from '../../assets';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPermissions, fetchPermission } from '../../store/reducer/permissionReducer';
import dateConvert from '../../helpers/dateConvert';
import instance from '../../config/axios';
import { showMessage } from 'react-native-flash-message';
import { AnimationSlideIn } from '../../components';


const Permission = ({ navigation }) => {
    const dispatch = useDispatch()

    const token = useSelector(({ user }) => user.Token)
    const permissions = useSelector(({ permission }) => permission.Permissions)
    const permission = useSelector(({ permission }) => permission.Permission)
    const loading = useSelector(({ permission }) => permission.Loading)
    const [opacity, setOpacity] = useState(false)

    const [detailData, setDetailData] = useState({})

    useEffect(() => {
        if (token) {
            dispatch(fetchPermissions(token))
            dispatch(fetchPermission(token))
        }
    }, [token])

    const cancel = async () => {
        try {
            await instance.delete(`/permission/${detailData._id}`, {
                headers: {
                    access_token: token
                }
            })
            setOpacity(false)
            setDetailData({})
            dispatch(fetchPermissions(token))
        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops!! something wrong'
            })
        }
    }

    return (
        <>
            <View style={{ flex: 1, backgroundColor: '#ffff' }} >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 50, paddingHorizontal: 20 }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={{ width: 50 }}
                    >
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 16 }} >Cuti</Text>
                    </View>
                    <View style={{ width: 50 }} />
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
                >
                    <View style={{ padding: 20 }} >
                        {loading ? (
                            <ActivityIndicator size="large" color="#ff9901" />
                        ) : (
                            <>
                                {permissions?.map(permission => (
                                    <TouchableOpacity
                                        onPress={() => {
                                            setDetailData(permission)
                                            setOpacity(true)
                                        }}
                                        key={permission._id}
                                        style={{ padding: 20, backgroundColor: '#ffff', flexDirection: 'row', borderRadius: 11, marginTop: 20 }}
                                    >
                                        <View style={{ flex: 2 }} >
                                            <Text style={{ fontWeight: '700' }} >{permission?.desc}</Text>
                                            <Text>{dateConvert(new Date(permission.startDate))} - {dateConvert(new Date(permission.endDate))}</Text>
                                        </View>
                                        <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-end' }} >
                                            <View style={{ borderWidth: 1, borderColor: permission?.status === 'Approve' ? '#42B442' : '#C0392B', borderRadius: 7, padding: 5 }} >
                                                <Text style={{ color: permission?.status === 'Approve' ? '#42B442' : '#C0392B' }} >{permission?.status}</Text>
                                            </View>
                                            <ILMoreVErtical />
                                        </View>
                                    </TouchableOpacity>
                                ))}
                            </>
                        )}
                    </View>
                </ScrollView>
                {loading ? (
                    <>
                    </>
                ) : (
                    <View style={{ alignItems: 'flex-end', padding: 20, backgroundColor: 'rgba(238, 238, 238, 0.3)' }} >
                        {permission ? (
                            <TouchableOpacity
                                onPress={() => showMessage({
                                    type: 'warning',
                                    message: 'Oops!!',
                                    description: 'Anda telah mengajukan cuti, tunggu konfirmasi'
                                })}
                                style={{ width: 60, height: 60, justifyContent: 'center', alignItems: 'center', borderRadius: 60 / 2, backgroundColor: '#c4c4c4' }}
                            >
                                <ILPlus />
                            </TouchableOpacity>
                        ) : (
                            <TouchableOpacity
                                onPress={() => navigation.navigate('ApplyPermission')}
                                style={{ width: 60, height: 60, justifyContent: 'center', alignItems: 'center', borderRadius: 60 / 2, backgroundColor: '#ff9901' }}
                            >
                                <ILPlus />
                            </TouchableOpacity>
                        )}
                    </View>
                )}
            </View>
            <AnimationSlideIn
                style={{
                    position: 'absolute', backgroundColor: '#ffff', borderTopLeftRadius: 50, borderTopRightRadius: 50, justifyContent: 'space-between', width: Dimensions.get('screen').width, maxHeight: Dimensions.get('screen').height / 2, bottom: 0, shadowColor: "#000", shadowOffset: { width: 0, height: 3, }, shadowOpacity: 0.27, shadowRadius: 4.65, elevation: 6
                }}
                opacity={opacity}
            >
                {Object.keys(detailData).length > 0 && (
                    <View style={{ flex: 1 }} >
                        <TouchableOpacity
                            onPress={() => {
                                setDetailData({})
                                setOpacity(false)
                            }}
                            style={{ backgroundColor: detailData?.status === 'Approve' ? '#42B442' : '#C0392B', borderTopLeftRadius: 50, borderTopRightRadius: 50, jjustifyContent: 'center', alignItems: 'center', padding: 16 }} >
                            <View>
                                <Text style={{ color: '#ffff', fontWeight: '700' }} >Close</Text>
                            </View>
                        </TouchableOpacity>
                        <View style={{ padding: 20, flexDirection: 'row' }} >
                            <View style={{ flex: 1 }} >
                                <Text style={{ fontSize: 18 }} >{detailData?.desc}</Text>
                                <View style={{ flexDirection: 'row' }} >
                                    <Text>{dateConvert(new Date(detailData?.endDate))}</Text>
                                    <Text style={{ paddingHorizontal: 10 }} >s/d</Text>
                                    <Text>{dateConvert(new Date(detailData?.startDate))}</Text>
                                </View>
                            </View>
                            {detailData?.status === 'Waiting' && (
                                <View>
                                    <TouchableOpacity
                                        onPress={cancel}
                                        style={{ backgroundColor: '#e74c3c', padding: 16, borderRadius: 11 }}
                                    >
                                        <Text style={{ color: '#ffff', textAlign: 'center' }} >Batalkan</Text>
                                    </TouchableOpacity>
                                </View>
                            )}
                        </View>
                    </View>
                )}
            </AnimationSlideIn>
        </>
    )
};

export default Permission;
