import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, ScrollView, Dimensions, Image, ActivityIndicator } from 'react-native';
import { TabView, TabBar } from 'react-native-tab-view';
import { useDispatch, useSelector } from 'react-redux';
import { ILCalendarY, ILChevrontL, ILChevrontR, ILMoreVErtical } from '../../assets';
import Avatar from '../../assets/img/avatar.png'
import { fetchEmployees } from '../../store/reducer/employeeReducer';

const EmployeeAttendance = ({ navigation }) => {
    const dispatch = useDispatch()
    const initialLayout = { width: Dimensions.get('window').width };

    const [days] = useState(["Minggu", 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", "Sabtu", "Minggu"])
    const [today] = useState(new Date())

    const [routes] = useState([
        { key: 'first', title: 'Sudah Absen' },
        { key: 'third', title: 'Belum Absen' }
    ])
    const [index, setIndex] = useState(0)

    const token = useSelector(({ user }) => user.Token)
    const entered = useSelector(({ employees }) => employees.Employees.Entered)
    const unentered = useSelector(({ employees }) => employees.Employees.Unentered)
    const loading = useSelector(({ employees }) => employees.Employees.Loading)

    useEffect(() => {
        if (token) dispatch(fetchEmployees(token))
    }, [token])

    const renderTabBar = props => {
        return (
            <View style={{ paddingHorizontal: 20, flex: 1, backgroundColor: '#ffff' }} >
                <TabBar {...props}
                    style={{
                        elevation: 0, paddingBottom: 31, backgroundColor: '#ffff'
                    }}
                    inactiveColor='black'
                    activeColor='black'
                    renderLabel={({ route }) => (
                        <Text style={{ color: 'black', fontFamily: 'DMSans-Bold' }} >{route.title}</Text>
                    )}
                    labelStyle={{
                        fontSize: 14, fontWeight: '700'
                    }}
                    indicatorStyle={{
                        backgroundColor: '#FF9901', height: 8, borderRadius: 10, marginBottom: 20
                    }}
                />
            </View>
        )
    }

    return (
        <>
            {loading ? (
                <View style={{ flex: 1 }} >
                    <ActivityIndicator size="large" color="#ff9901" />
                </View>
            ) : (
                <View style={{ flex: 1, backgroundColor: '#ffff' }} >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 50, paddingHorizontal: 20 }}>
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                            style={{ width: 50 }}
                        >
                            <ILChevrontL />
                        </TouchableOpacity>
                        <View>
                            <Text style={{ fontSize: 16 }} >Absensi Karyawan</Text>
                        </View>
                        <View style={{ width: 50 }} />
                    </View>
                    <View style={{ marginTop: 30, padding: 20 }} >
                        <View style={{ flexDirection: 'row' }} >
                            <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                                <View style={{ height: 42, width: 7, borderRadius: 10, backgroundColor: '#E5E5E5' }} />
                                <View style={{ flex: 1, marginLeft: 10, }} >
                                    <Text style={{ fontWeight: '700' }}>{days[+today.getDay()]}</Text>
                                    <Text style={{ fontSize: 12 }}>{`${today.getDate()}-${String(today.getMonth()).length === 1 ? `0${today.getMonth()}` : today.getMonth()}-${today.getFullYear()}`}</Text>
                                </View>
                            </View>
                            <View style={{ justifyContent: 'center' }} >
                                <View style={{ height: 42, width: 7, borderRadius: 10, backgroundColor: '#E5E5E5' }} />
                            </View>
                            <View style={{ flex: 1, justifyContent: 'center' }} >
                                <TouchableOpacity
                                    onPress={() => navigation.navigate('AttendanceRecap')}
                                    style={{ marginLeft: 10, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}
                                >
                                    <ILCalendarY />
                                    <Text style={{ flex: 1, marginLeft: 5, fontWeight: '700' }} >Rekap Absen</Text>
                                    <ILChevrontR />
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
                    >
                        <TabView
                            navigationState={{ index, routes }}
                            renderScene={() => null}
                            onIndexChange={setIndex}
                            initialLayout={initialLayout}
                            renderTabBar={renderTabBar}
                        />
                        {index === 0 && (
                            <View style={{ padding: 20 }} >
                                {entered.length > 0 && entered?.map(item => (
                                    <TouchableOpacity
                                        onPress={() => navigation.navigate('DetailAttendanceEmployee', { id: item.attendance._id })}
                                        key={item._id}
                                        style={{ backgroundColor: '#ffff', marginTop: 20, padding: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', borderRadius: 10 }}
                                    >
                                        <View>
                                            <Image source={Avatar} style={{ width: 60, height: 60, borderRadius: 60 / 2 }} />
                                        </View>
                                        <View style={{ flex: 1, paddingHorizontal: 10 }} >
                                            <Text style={{ fontSize: 16, fontWeight: '700' }} >{item?.name}</Text>
                                            <Text style={{ fontSize: 12 }} >{item?.position}</Text>
                                            <Text style={{ fontSize: 12 }}>{item?.region}</Text>
                                        </View>
                                        <View>
                                            <ILMoreVErtical />
                                        </View>
                                    </TouchableOpacity>
                                ))}
                            </View>
                        )}
                        {index === 1 && (
                            <View style={{ padding: 20 }} >
                                {unentered.length > 0 && unentered.map(item => (
                                    <View
                                        key={item._id}
                                        style={{ backgroundColor: '#ffff', marginTop: 20, padding: 20, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', borderRadius: 10 }}
                                    >
                                        <View>
                                            <Image source={Avatar} style={{ width: 60, height: 60, borderRadius: 60 / 2 }} />
                                        </View>
                                        <View style={{ flex: 1, paddingHorizontal: 10 }} >
                                            <Text style={{ fontSize: 16, fontWeight: '700' }} >{item?.name}</Text>
                                            <Text style={{ fontSize: 12 }} >{item?.position}</Text>
                                            <Text style={{ fontSize: 12 }}>{item?.region}</Text>
                                        </View>
                                        <View>
                                            <ILMoreVErtical />
                                        </View>
                                    </View>
                                ))}
                            </View>
                        )}

                    </ScrollView>
                </View>
            )}
        </>
    )
};

export default EmployeeAttendance;
