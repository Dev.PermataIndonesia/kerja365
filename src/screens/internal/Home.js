import React, { useEffect, useState, useCallback } from 'react';
import { SafeAreaView, ScrollView, Text, StyleSheet, View, Image, TouchableOpacity, RefreshControl } from 'react-native';
import { ILBell, ILBPJS, ILEpaySlip, ILLoan } from '../../assets'
import Avatar from '../../assets/img/avatar.png'
import { VibePointsButton, JobNews, Video } from '../../components'
import { useDispatch, useSelector } from 'react-redux';
import { getData } from '../../utils/localStorage';
import { fetchVibePoints } from '../../store/reducer/vibePointReducer';
import { fetchNotifications } from '../../store/reducer/messagesReducer';
import { fetchNews } from '../../store/reducer/newsReducer';
import { ActivityButton } from '../../components'
import { fetchVideos } from '../../store/reducer/videosReducer';
import { fetchEmployeeAbsents, fetchEmployeeActivities, fetchEmployeePermissions, fetchEmployees, fetchEmployeeOvertimes } from '../../store/reducer/employeeReducer';

const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
}

const HomeInternal = ({ navigation }) => {
    const dispatch = useDispatch()

    const [refreshing, setRefreshing] = useState(false)

    const user = useSelector(({ user }) => user.User)
    const token = useSelector(({ user }) => user.Token)
    const points = useSelector(({ points }) => points.Points)
    const notifications = useSelector(({ messages }) => messages.Notifications)

    useEffect(() => {

        (async () => {
            const getDataUser = await getData('user')
            dispatch({ type: 'SET_USER', payload: getDataUser })
            const getToken = await getData('token')
            dispatch({ type: 'SET_TOKEN', payload: getToken })
        })()
        if (token) {
            // dispatch(fetchVibePoints(token))
            dispatch(fetchNews())
            dispatch(fetchVideos(0, 3))
            let id
            if (user.type === 'internal' || user.type === 'tko') id = user.nrk
            else id = user.id
            dispatch(fetchNotifications(id))
        }
        if (token && user?.supervisor) {
            dispatch(fetchEmployees(token))
            dispatch(fetchEmployeePermissions(token))
            dispatch(fetchEmployeeAbsents(token))
            dispatch(fetchEmployeeActivities(token))
            dispatch(fetchEmployeeOvertimes(token))
        }
    }, [token, refreshing])


    const onRefreshing = useCallback(() => {
        setRefreshing(true)
        wait(2000).then(() => setRefreshing(false))
    }, [])

    return (
        <>
            <SafeAreaView
                style={{ flex: 1, paddingVertical: 10, backgroundColor: '#ffff' }}
            >
                <ScrollView
                    style={{ backgroundColor: 'rgba(238,238,238,0.3)' }}
                    showsVerticalScrollIndicator={false}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefreshing}
                        />
                    }
                >
                    <View style={{ backgroundColor: '#ffff' }} >
                        <View style={{ padding: 20 }} >
                            <TouchableOpacity
                                onPress={() => navigation.navigate('Notifications')}
                                style={{ alignSelf: 'flex-end' }}
                            >
                                <ILBell />
                                {notifications?.length > 0 && (
                                    <View
                                        style={{ position: 'absolute', bottom: -5, left: -10, width: 20, height: 20, borderRadius: 20 / 2, backgroundColor: '#EA2027', justifyContent: 'center', alignItems: 'center' }} >
                                        <Text style={{ color: '#ffff', fontSize: 14 }} >{notifications?.filter(el => !el.read).length}</Text>
                                    </View>
                                )}
                            </TouchableOpacity>
                            <View style={{ marginTop: 28, alignSelf: 'flex-end', flexDirection: 'row', alignItems: 'center' }} >
                                <View style={{ marginRight: 17 }} >
                                    <Text style={{ textAlign: 'right', fontSize: 11, fontWeight: '700' }} >Welcome</Text>
                                    <Text style={{ textAlign: 'right', fontSize: 14, fontWeight: '700' }} >{user?.name ? user?.name : user?.nama}</Text>
                                    <Text style={{ textAlign: 'right', fontSize: 12 }} >{user?.nrk}</Text>
                                    <Text style={{ textAlign: 'right', fontSize: 12 }} >{user?.client}</Text>
                                    <Text style={{ textAlign: 'right', fontSize: 12 }} >{user?.position}</Text>
                                </View>
                                <Image source={user?.photo ? { uri: user?.photo } : Avatar} style={{ width: 60, height: 60, borderRadius: 60 / 2 }} />
                            </View>
                        </View>
                        <View style={{ padding: 20 }} >
                            <View style={{ flexDirection: 'row', paddingVertical: 10, justifyContent: 'space-between', borderRadius: 100, paddingHorizontal: 45, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}>
                                <TouchableOpacity
                                    onPress={() => navigation.navigate('Epayslip')}
                                >
                                    <View style={{ alignItems: 'center' }}>
                                        <View style={{ height: 15 }}>
                                            <ILEpaySlip />
                                        </View>
                                        <Text style={styles.title} >Epayslip</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => navigation.navigate('Bpjs')}
                                >
                                    <View style={{ alignItems: 'center' }}>
                                        <View style={{ height: 15 }}>
                                            <ILBPJS />
                                        </View>
                                        <Text style={styles.title} >BPJS</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity
                                    onPress={() => navigation.navigate('Loan')}
                                >
                                    <View style={{ alignItems: 'center' }}>
                                        <View style={{ height: 15 }}>
                                            <ILLoan />
                                        </View>
                                        <Text style={styles.title} >Loan</Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                        {/* <View style={{ padding: 20 }} >
                            <VibePointsButton
                                navigation={navigation}
                                points={points}
                            />
                        </View> */}
                    </View>

                    <View style={{ padding: 20 }}>
                        <ActivityButton
                            navigation={navigation}
                            user={user}
                        />
                    </View>

                    <Video
                        navigation={navigation}
                    />

                    <JobNews navigation={navigation} />
                </ScrollView>
            </SafeAreaView>
        </>
    )
};

const styles = StyleSheet.create({
    title: {
        fontSize: 12,
        fontFamily: 'DMSans-Bold',
        marginTop: 15
    }
})

export default HomeInternal;
