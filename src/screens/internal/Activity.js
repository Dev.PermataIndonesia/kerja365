import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, ScrollView, Dimensions, Alert } from 'react-native';
import { ILChevrontL, ILCalendarY, ILMoreVErtical } from '../../assets';
import { useDispatch, useSelector } from 'react-redux';
import { fetchAttendances, fetchAttendance, fetchAttendancesByRange } from '../../store/reducer/attendanceReducer';
import DateTimePicker from '@react-native-community/datetimepicker';
import dateConvert from '../../helpers/dateConvert';
import { useInput } from '../../customHook';
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'; import instance from '../../config/axios';
import GeoLocation from 'react-native-get-location';
import { showMessage } from 'react-native-flash-message';

const Activity = ({ navigation }) => {
    const dispatch = useDispatch()

    const [days] = useState(['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', "Jum'at", 'Sabtu'])

    const token = useSelector(({ user }) => user.Token)
    const attendances = useSelector(({ attendance }) => attendance.Attendances)
    const attendance = useSelector(({ attendance }) => attendance.Attendance)
    const loading = useSelector(({ attendance }) => attendance.Loading)

    const startDate = useInput(new Date())
    const endDate = useInput(new Date())

    const [location, setLocation] = useState({})

    useEffect(() => {
        if (token) {
            dispatch(fetchAttendances(token))
            dispatch(fetchAttendance(token))
        }
    }, [token])

    useEffect(() => {
        GeoLocation.getCurrentPosition({
            enableHighAccuracy: true,
            timeout: 15000
        })
            .then(res => {
                setLocation({
                    lng: res.longitude,
                    lat: res.latitude
                })
            })
            .catch(err => {
                showMessage({
                    type: 'warning',
                    message: 'Ooops! smoething wrong',
                    description: err.message,
                    duration: 300
                })
            })
    }, [])

    const submit = async () => {
        dispatch(fetchAttendancesByRange(token, startDate.date, endDate.date))
    }

    const goOut = () => {
        Alert.alert(
            "Are you sure?",
            "Go out now",
            [
                {
                    text: "No",
                    style: "cancel"
                },
                {
                    text: "Yes",
                    onPress: async () => {
                        try {
                            if (!location.lng && !location.lat) throw { message: "Cannot find your current location" }

                            if (!attendance.dateIn) {
                                throw { message: "Anda belum absen masuk" }
                            } else {
                                attendance.dateOut = new Date()
                                attendance.locationOut = {
                                    lng: location.lng,
                                    lat: location.lat
                                }
                                await instance.put(`/attendance/${attendance._id}`, attendance, {
                                    headers: {
                                        access_token: token
                                    }
                                })
                                dispatch(fetchAttendance(token))
                                showMessage({
                                    type: "success",
                                    message: "Success Go out"
                                })
                            }
                        } catch (error) {
                            showMessage({
                                type: "warning",
                                message: "Oops!! something error",
                                description: error.message
                            })
                        }
                    }
                }
            ]
        )
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#ffff' }} >
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 20, paddingHorizontal: 20 }}>
                <TouchableOpacity
                    onPress={() => navigation.goBack()}
                    style={{ width: 50 }}
                >
                    <ILChevrontL />
                </TouchableOpacity>
                <View>
                    <Text style={{ fontSize: 16 }} >Absensi</Text>
                </View>
                <View style={{ width: 50 }} />
            </View>
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
            >
                <View style={{ padding: 20, backgroundColor: '#ffff' }} >
                    <View style={{ flexDirection: 'row' }} >
                        <View style={{ flex: 1 }} >
                            <TouchableOpacity
                                onPress={startDate.showDatePicker}
                                style={{ flexDirection: 'row', alignItems: 'center', borderWidth: 1, borderColor: '#c4c4c4', backgroundColor: '#ffff', padding: 20, borderRadius: 11 }}
                            >
                                <ILCalendarY />
                                <Text style={{ marginLeft: 10, color: '#c4c4c4' }} >{dateConvert(startDate.date)}</Text>
                            </TouchableOpacity>
                            {startDate.show && (
                                <DateTimePicker
                                    testID="dateTimePicker1"
                                    value={startDate.date}
                                    mode={startDate.mode}
                                    is24Hour={true}
                                    display='spinner'
                                    onChange={startDate.onChange}
                                />
                            )}
                        </View>
                        <View style={{ width: 20 }} />
                        <View style={{ flex: 1 }} >
                            <TouchableOpacity
                                onPress={endDate.showDatePicker}
                                style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#ffff', borderWidth: 1, borderColor: '#c4c4c4', padding: 20, borderRadius: 11 }}
                            >
                                <ILCalendarY />
                                <Text style={{ marginLeft: 10, color: '#c4c4c4' }} >{dateConvert(endDate.date)}</Text>
                            </TouchableOpacity>
                            {endDate.show && (
                                <DateTimePicker
                                    testID="dateTimePicker1"
                                    value={endDate.date}
                                    mode={endDate.mode}
                                    is24Hour={true}
                                    display='spinner'
                                    onChange={endDate.onChange}
                                />
                            )}
                        </View>
                    </View>
                    <View style={{ alignItems: 'flex-end', marginTop: 10 }} >
                        <TouchableOpacity
                            onPress={submit}
                            style={{ width: 100, height: 50, backgroundColor: '#ff9901', borderRadius: 11, justifyContent: "center", alignItems: "center" }}
                        >
                            <Text style={{ color: '#ffff', fontWeight: '700' }} >Filter</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <View style={{ padding: 20 }} >
                    {loading ? (
                        <SkeletonPlaceholder>
                            <View style={{ width: Dimensions.get('screen').width - 40, height: 60, marginTop: 10, borderRadius: 11 }} />
                            <View style={{ width: Dimensions.get('screen').width - 40, height: 60, marginTop: 10, borderRadius: 11 }} />
                            <View style={{ width: Dimensions.get('screen').width - 40, height: 60, marginTop: 10, borderRadius: 11 }} />
                            <View style={{ width: Dimensions.get('screen').width - 40, height: 60, marginTop: 10, borderRadius: 11 }} />
                            <View style={{ width: Dimensions.get('screen').width - 40, height: 60, marginTop: 10, borderRadius: 11 }} />
                        </SkeletonPlaceholder>
                    ) : (
                        <>
                            {attendances?.map(attendance => (
                                <TouchableOpacity
                                    onPress={() => navigation.navigate("DetailAttendance", { id: attendance._id })}
                                    key={attendance._id}
                                    style={{ marginTop: 10, padding: 20, backgroundColor: '#ffff', borderRadius: 11, flexDirection: 'row', justifyContent: 'space-between' }}
                                >
                                    <Text style={{ fontWeight: "400", color: "#34495e" }} >{days[new Date(attendance.dateIn).getDay()]} , {new Date(attendance.dateIn).getDate()} - {new Date(attendance.dateIn).getMonth()} - {new Date(attendance.dateIn).getFullYear()}</Text>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                        {attendance.goIn ? (
                                            <Text style={{ color: '#2ecc71', fontWeight: '400' }} >{new Date(attendance.dateIn).getHours()}:{String(new Date(attendance.dateIn).getMinutes()).length < 2 ? `0${new Date(attendance.dateIn).getMinutes()}` : new Date(attendance.dateIn).getMinutes()}</Text>
                                        ) : (
                                            <Text style={{ color: "#e74c3c", fontWeight: '400' }} >Izin</Text>
                                        )}
                                        <ILMoreVErtical />
                                    </View>

                                </TouchableOpacity>
                            ))}
                        </>
                    )}
                </View>
            </ScrollView>
            <View style={{ padding: 20 }} >
                {Object.keys(attendance).length < 1 ? (
                    <View style={{ flexDirection: 'row', }} >
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Camera', { status: "go in" })}
                            style={{ flex: 1, padding: 20, backgroundColor: '#ff9901', borderRadius: 11, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <Text style={{ fontWeight: '700', color: '#ffff' }} >Absen Masuk</Text>
                        </TouchableOpacity>
                        <View style={{ width: 20 }} />
                        <TouchableOpacity
                            onPress={() => navigation.navigate('Camera', { status: "absent" })}
                            style={{ flex: 1, padding: 20, backgroundColor: '#e74c3c', borderRadius: 11, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <Text style={{ fontWeight: '700', color: '#ffff' }} >Izin</Text>
                        </TouchableOpacity>
                    </View>
                ) : (
                    <View style={{ flexDirection: 'row', }} >
                        {attendance?.dateOut ? (
                            <View
                                style={{ flex: 1, padding: 20, backgroundColor: '#eeee', borderRadius: 11, justifyContent: 'center', alignItems: 'center' }}
                            >
                                <Text style={{ fontWeight: '700', color: '#2ecc71' }} >Absen Pulang</Text>
                            </View>
                        ) : (
                            <TouchableOpacity
                                onPress={goOut}
                                style={{ flex: 1, padding: 20, backgroundColor: '#2ecc71', borderRadius: 11, justifyContent: 'center', alignItems: 'center' }}
                            >
                                <Text style={{ fontWeight: '700', color: '#ffff' }} >Absen Pulang</Text>
                            </TouchableOpacity>
                        )}
                        <View style={{ width: 20 }} />
                        <View
                            style={{ flex: 1, padding: 20, backgroundColor: '#eeee', borderRadius: 11, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <Text style={{ fontWeight: '700', color: '#e74c3c' }} >Izin</Text>
                        </View>
                    </View>
                )}
            </View>
        </View>
    )
};

export default Activity;
