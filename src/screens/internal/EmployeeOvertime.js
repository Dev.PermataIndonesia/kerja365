import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, ScrollView, TextInput, Image, ActivityIndicator } from 'react-native';
import { ILChevrontL, ILCalendarY, ILSeacrhW, ILMoreVErtical, ILEllipse4, ILChevrontDY, } from '../../assets';
import DateTimePicker from '@react-native-community/datetimepicker';
import dateConvert from '../../helpers/dateConvert';
import { useInput } from '../../customHook';
import Avatar from '../../assets/img/avatar.png'
import { useDispatch, useSelector } from 'react-redux';
import { fetchEmployeeOvertimes, fetchEmployeeOvertimeSearch } from '../../store/reducer/employeeReducer';
import { showMessage } from 'react-native-flash-message';
import instance from '../../config/axios';
import { Loading } from '../../components';

const EmployeeOvertime = ({ navigation }) => {
    const dispatch = useDispatch()

    const overtimes = useSelector(({ employees }) => employees.Overtimes)
    const loading = useSelector(({ employees }) => employees.Loading)
    const token = useSelector(({ user }) => user.Token)

    const [name, setName] = useState('')

    const startDate = useInput(new Date())
    const endDate = useInput(new Date())

    useEffect(() => {
        if (token) dispatch(fetchEmployeeOvertimes(token))
    }, [token])

    const action = async (item, status) => {
        try {
            await instance.put(`/employee/work-overtime/${item._id}`, { status }, {
                headers: {
                    access_token: token
                }
            })
            dispatch(fetchEmployeeOvertimes(token))
            showMessage({
                type: 'success',
                message: `Success ${status}`
            })
        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops!! something error'
            })
        }
    }

    const onPress = () => {
        if (token) dispatch(fetchEmployeeOvertimeSearch(token, startDate.date, endDate.date, name))
    }

    return (
        <>
            <View style={{ flex: 1, backgroundColor: '#ffff' }} >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 50, paddingHorizontal: 20 }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={{ width: 50 }}
                    >
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 16 }} >Lembur Karyawan</Text>
                    </View>
                    <View style={{ width: 50 }} />
                </View>
                <View style={{ padding: 20 }} >
                    <View style={{ flexDirection: 'row' }} >
                        <View style={{ flex: 1 }} >
                            <TouchableOpacity
                                onPress={startDate.showDatePicker}
                                style={{ flexDirection: 'row', alignItems: 'center', borderWidth: 1, borderColor: '#eeee', backgroundColor: '#ffff', padding: 20, borderRadius: 11 }}
                            >
                                <ILCalendarY />
                                <Text style={{ marginLeft: 10, color: '#ff9901' }} >{dateConvert(startDate.date)}</Text>
                            </TouchableOpacity>
                            {startDate.show && (
                                <DateTimePicker
                                    testID="dateTimePicker1"
                                    value={startDate.date}
                                    mode={startDate.mode}
                                    is24Hour={true}
                                    display='spinner'
                                    onChange={startDate.onChange}
                                />
                            )}
                        </View>
                        <View style={{ width: 20 }} />
                        <View style={{ flex: 1 }} >
                            <TouchableOpacity
                                onPress={endDate.showDatePicker}
                                style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#ffff', borderWidth: 1, borderColor: '#eeee', padding: 20, borderRadius: 11 }}
                            >
                                <ILCalendarY />
                                <Text style={{ marginLeft: 10, color: '#ff9901' }} >{dateConvert(endDate.date)}</Text>
                            </TouchableOpacity>
                            {endDate.show && (
                                <DateTimePicker
                                    testID="dateTimePicker1"
                                    value={endDate.date}
                                    mode={endDate.mode}
                                    is24Hour={true}
                                    display='spinner'
                                    onChange={endDate.onChange}
                                />
                            )}
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center' }} >
                        <View style={{ flex: 1 }} >
                            <TextInput
                                style={{ borderColor: '#eeee', borderWidth: 1, borderRadius: 10, height: 61, paddingHorizontal: 10, color: 'black' }}
                                placeholder="Cari nama"
                                placeholderTextColor="#c4c4c4"
                                value={name}
                                onChangeText={val => setName(val)}
                            />
                        </View>
                        <View style={{ width: 20 }} />
                        <TouchableOpacity
                            onPress={onPress}
                            style={{ width: 71, height: 61, borderRadius: 10, backgroundColor: '#ff9901', justifyContent: 'center', alignItems: 'center' }}
                        >
                            <ILSeacrhW />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
                >
                    {loading ? (
                        <View style={{ padding: 20, justifyContent: 'center', alignItems: 'center' }} >
                            <ActivityIndicator size="large" color="#ff9901" />
                            <View style={{ marginTop: 20 }} >
                                <Text>Loading ....</Text>
                            </View>
                        </View>
                    ) : (
                        <>
                            <View style={{ padding: 20 }} >
                                {overtimes?.inAction?.length > 0 && overtimes?.inAction.map(item => (
                                    <View
                                        key={item._id}
                                        style={{ backgroundColor: '#ffff', marginTop: 20, padding: 20, borderRadius: 10 }}
                                    >
                                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
                                            <View>
                                                <Image source={Avatar} style={{ width: 60, height: 60, borderRadius: 60 / 2 }} />
                                            </View>
                                            <View style={{ flex: 1, paddingHorizontal: 10 }} >
                                                <Text style={{ fontSize: 16, fontWeight: '700' }} >{item?.user?.name}</Text>
                                                <Text style={{ fontSize: 12 }} >{item?.user?.position}</Text>
                                                <Text style={{ fontSize: 12 }}>{item?.user?.region}</Text>
                                            </View>
                                            <View>
                                                <ILMoreVErtical />
                                            </View>
                                        </View>
                                        <View style={{ marginTop: 30, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
                                            <View style={{ height: 50, width: 7, backgroundColor: '#E5E5E5', borderRadius: 10 }} />
                                            <View style={{ flex: 1, paddingHorizontal: 10 }} >
                                                <Text>{item?.title}</Text>
                                                <Text>{new Date(item?.createdAt).getDate()} - {new Date(item?.createdAt).getMonth() + 1} - {new Date(item?.createdAt).getFullYear()}</Text>
                                            </View>
                                            <View>
                                                <ILEllipse4 />
                                            </View>
                                        </View>

                                        <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center' }} >
                                            <View style={{ flex: 1, height: 1, backgroundColor: '#eeee' }} />
                                            <View style={{ width: 10 }} />
                                            <TouchableOpacity style={{ flexDirection: 'row' }} >
                                                <Text style={{ color: '#ff9901' }} >Show details</Text>
                                                <ILChevrontDY />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ marginTop: 20, flexDirection: 'row', justifyContent: 'flex-end' }} >
                                            <TouchableOpacity
                                                onPress={() => action(item, 'Approve')}
                                                style={{ width: 75, height: 30, alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderRadius: 5, borderColor: '#42B442' }} >
                                                <Text style={{ color: '#42B442' }} >Approve</Text>
                                            </TouchableOpacity>
                                            <View style={{ width: 10 }} />
                                            <TouchableOpacity
                                                onPress={() => action(item, 'Reject')}
                                                style={{ width: 75, height: 30, alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderRadius: 5, borderColor: '#C0392B' }} >
                                                <Text style={{ color: '#C0392B' }} >Reject</Text>
                                            </TouchableOpacity>
                                        </View>
                                    </View>
                                ))}
                            </View>

                            <View style={{ padding: 20 }} >
                                {overtimes?.alreadyAction?.length > 0 && overtimes?.alreadyAction.map(item => (
                                    <View
                                        key={item._id}
                                        style={{ backgroundColor: '#ffff', marginTop: 20, padding: 20, borderRadius: 10 }}
                                    >
                                        <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
                                            <View>
                                                <Image source={Avatar} style={{ width: 60, height: 60, borderRadius: 60 / 2 }} />
                                            </View>
                                            <View style={{ flex: 1, paddingHorizontal: 10 }} >
                                                <Text style={{ fontSize: 16, fontWeight: '700' }} >{item?.user?.name}</Text>
                                                <Text style={{ fontSize: 12 }} >{item?.user?.position}</Text>
                                                <Text style={{ fontSize: 12 }}>{item?.user?.region}</Text>
                                            </View>
                                            <View>
                                                <ILMoreVErtical />
                                            </View>
                                        </View>
                                        <View style={{ marginTop: 30, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
                                            <View style={{ height: 50, width: 7, backgroundColor: '#E5E5E5', borderRadius: 10 }} />
                                            <View style={{ flex: 1, paddingHorizontal: 10 }} >
                                                <Text>{item?.title}</Text>
                                                <Text>{new Date(item?.createdAt).getDate()} - {new Date(item?.createdAt).getMonth() + 1} - {new Date(item?.createdAt).getFullYear()}</Text>
                                            </View>
                                        </View>

                                        <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center' }} >
                                            <View style={{ flex: 1, height: 1, backgroundColor: '#eeee' }} />
                                            <View style={{ width: 10 }} />
                                            <TouchableOpacity style={{ flexDirection: 'row' }} >
                                                <Text style={{ color: '#ff9901' }} >Show details</Text>
                                                <ILChevrontDY />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ marginTop: 20, flexDirection: 'row', justifyContent: 'flex-end' }} >
                                            <View
                                                style={{ width: 75, height: 30, alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderRadius: 5, borderColor: item.status === 'Approve' ? '#42B442' : '#C0392B' }} >
                                                <Text style={{ color: item.status === 'Approve' ? '#42B442' : '#C0392B' }} >{item.status}</Text>
                                            </View>
                                            <View style={{ width: 10 }} />
                                        </View>
                                    </View>

                                ))}
                            </View>
                        </>
                    )}

                </ScrollView>
            </View>
        </>
    )
};

export default EmployeeOvertime;
