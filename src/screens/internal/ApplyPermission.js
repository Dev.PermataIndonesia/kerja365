import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, ScrollView, Dimensions, TextInput } from 'react-native';
import { ILChevrontL, ILCalendarY } from '../../assets';
import { useDispatch, useSelector } from 'react-redux';
import DateTimePicker from '@react-native-community/datetimepicker';
import dateConvert from '../../helpers/dateConvert';
import { useInput } from '../../customHook';
import { showMessage } from 'react-native-flash-message';
import instance from '../../config/axios';
import { fetchPermissions } from '../../store/reducer/permissionReducer';
import { Loading } from '../../components';

const ApplyPermission = ({ navigation }) => {
    const dispatch = useDispatch()

    const token = useSelector(({ user }) => user.Token)

    const [desc, setDesc] = useState('')
    const [loading, setLoading] = useState(false)

    const startDate = useInput(new Date())
    const endDate = useInput(new Date())

    const submit = async () => {
        setLoading(true)
        try {
            if (!desc) {
                throw { description: "Deskripsi tidak boleh kosong" }
            }

            if (startDate.date > endDate.date) {
                throw { description: 'Tanggal mulai tidak boleh lebih besar dari tangal akhir pengajuan' }
            }

            const data = {
                desc: desc,
                startDate: startDate.date,
                endDate: endDate.date
            }
            await instance.post('/permission', data, {
                headers: {
                    access_token: token
                }
            })
            dispatch(fetchPermissions(token))
            navigation.goBack()
            showMessage({
                type: 'success',
                message: 'Success',
                description: 'Berhasil mengirim pengajuan'
            })
        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops! something wrong',
                description: error.description ? error.description : ''
            })
        }
        setLoading(false)
    }

    return (
        <>
            {loading && <Loading />}
            <View style={{ flex: 1, backgroundColor: '#ffff' }} >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 50, paddingHorizontal: 20 }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={{ width: 50 }}
                    >
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 16 }} >Pengajuan Cuti</Text>
                    </View>
                    <View style={{ width: 50 }} />
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
                >
                    <View style={{ padding: 20 }} >
                        <View style={{ padding: 20, backgroundColor: '#ffff', borderRadius: 11 }} >
                            <TextInput
                                placeholder='Description'
                                placeholderTextColor='#C4C4C4'
                                value={desc}
                                onChangeText={val => setDesc(val)}
                                style={{ color: 'black' }}
                            />
                        </View>
                        <View style={{ marginTop: 20 }} >
                            <TouchableOpacity
                                onPress={startDate.showDatePicker}
                                style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#ffff', padding: 20, borderRadius: 11 }}
                            >
                                <ILCalendarY />
                                <Text style={{ marginLeft: 10, color: '#ff9901' }} >From</Text>
                                <Text style={{ marginLeft: 10, color: '#c4c4c4' }} >{dateConvert(startDate.date)}</Text>
                            </TouchableOpacity>
                            {startDate.show && (
                                <DateTimePicker
                                    testID="dateTimePicker1"
                                    value={startDate.date}
                                    mode={startDate.mode}
                                    is24Hour={true}
                                    display='spinner'
                                    onChange={startDate.onChange}
                                />
                            )}
                        </View>
                        <View style={{ marginTop: 20 }} >
                            <TouchableOpacity
                                onPress={endDate.showDatePicker}
                                style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#ffff', padding: 20, borderRadius: 11 }}
                            >
                                <ILCalendarY />
                                <Text style={{ marginLeft: 10, color: '#ff9901' }} >To</Text>
                                <Text style={{ marginLeft: 10, color: '#c4c4c4' }} >{dateConvert(endDate.date)}</Text>
                            </TouchableOpacity>
                            {endDate.show && (
                                <DateTimePicker
                                    testID="dateTimePicker1"
                                    value={endDate.date}
                                    mode={endDate.mode}
                                    is24Hour={true}
                                    display='spinner'
                                    onChange={endDate.onChange}
                                />
                            )}
                        </View>
                    </View>
                </ScrollView>
                <View style={{ alignItems: 'center', padding: 20, backgroundColor: 'rgba(238, 238, 238, 0.3)' }} >
                    <TouchableOpacity
                        onPress={submit}
                        style={{ height: 70, width: Dimensions.get('screen').width - 40, justifyContent: 'center', alignItems: 'center', borderRadius: 50, backgroundColor: '#EEEE' }}
                    >
                        <Text style={{ fontWeight: '700', color: 'black' }} >Apply Permission</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </>
    )
};

export default ApplyPermission;
