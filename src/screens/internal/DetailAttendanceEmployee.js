import React, { useEffect } from 'react';
import { Text, View, TouchableOpacity, ScrollView, Dimensions, Image, ActivityIndicator } from 'react-native';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps';
import { useDispatch, useSelector } from 'react-redux';
import { ILCalendarY, ILChevrontL, ILChevrontR, ILMoreVErtical } from '../../assets';
import { fetchAttendanceEmployee } from '../../store/reducer/attendanceReducer';

const DetailAttendanceEmployee = ({ navigation, route }) => {
    const dispatch = useDispatch()

    const { id } = route.params

    const token = useSelector(({ user }) => user.Token)
    const attendance = useSelector(({ attendance }) => attendance.AttendanceEmployee)
    const loading = useSelector(({ attendance }) => attendance.Loading)

    useEffect(() => {
        if (token && id) dispatch(fetchAttendanceEmployee(token, id))
    }, [token])

    return (
        <View style={{ flex: 1, backgroundColor: '#ffff' }} >
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 50, paddingHorizontal: 20 }}>
                <TouchableOpacity
                    onPress={() => navigation.goBack()}
                    style={{ width: 50 }}
                >
                    <ILChevrontL />
                </TouchableOpacity>
                <View>
                    <Text style={{ fontSize: 16 }} >Detail Absensi Karyawan</Text>
                </View>
                <View style={{ width: 50 }} />
            </View>
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={{ flex: 1, backgroundColor: 'rgba(238,238,238,0.5)' }}
            >
                {loading ? (
                    <View style={{ padding: 20 }} >
                        <ActivityIndicator size="large" color="#ff9901" />
                    </View>
                ) : (
                    <View style={{ padding: 20 }} >
                        <View style={{ padding: 10, backgroundColor: '#ffff', borderRadius: 11 }} >
                            <View style={{ flexDirection: 'row', paddingVertical: 10, alignItems: 'center', justifyContent: 'center' }} >
                                {attendance?.photo && (
                                    <Image source={{ uri: attendance?.photo }} style={{ width: 50, height: 50, borderRadius: 50 / 2 }} />
                                )}
                                <View style={{ marginLeft: 10, flex: 1 }} >
                                    <Text>{attendance?.user?.name}</Text>
                                    <Text style={{ fontSize: 11 }} >{attendance?.user?.position}</Text>
                                    <Text style={{ fontSize: 11 }} >{attendance?.user?.region}</Text>
                                </View>
                                <View>
                                    <ILMoreVErtical />
                                </View>
                            </View>
                            <View style={{ marginTop: 10 }} >
                                {attendance?.dateIn && (
                                    <>
                                        <Text style={{ fontSize: 18 }} >{new Date(attendance?.dateIn).getDate()} - {new Date(attendance?.dateIn).getMonth()} - {new Date(attendance?.dateIn).getFullYear()}</Text>
                                        <View style={{ flexDirection: 'row' }} >
                                            <Text style={{ fontSize: 12 }} >Absen Masuk</Text>
                                            <Text style={{ fontSize: 12, marginLeft: 20 }} >{new Date(attendance?.dateIn).getHours()} - {new Date(attendance?.dateIn).getMinutes()}</Text>
                                        </View>
                                    </>
                                )}
                                {attendance?.dateOut && (
                                    <View style={{ flexDirection: 'row' }} >
                                        <Text style={{ fontSize: 12 }} >Absen Pulang</Text>
                                        <Text style={{ fontSize: 12, marginLeft: 20 }} >{new Date(attendance?.dateOut).getHours()} - {new Date(attendance?.dateOut).getMinutes()}</Text>
                                    </View>
                                )}
                            </View>
                            <View style={{ marginTop: 20 }} >
                                {attendance?.locationIn && (
                                    <>
                                        <Text>Lokasi absen masuk</Text>
                                        <MapView
                                            provider={PROVIDER_GOOGLE}
                                            style={{
                                                marginTop: 15,
                                                height: 202
                                            }}
                                            initialRegion={{
                                                latitude: +attendance?.locationIn.lat,
                                                longitude: +attendance?.locationIn.lng,
                                                latitudeDelta: 0.022,
                                                longitudeDelta: 0.001,
                                            }}
                                            showsUserLocation={true}
                                            zoomControlEnabled
                                            scrollEnabled={false}
                                        >
                                            <Marker
                                                coordinate={{
                                                    latitude: +attendance?.locationIn.lat,
                                                    longitude: +attendance?.locationIn.lng,
                                                    latitudeDelta: 0.0922,
                                                    longitudeDelta: 0.0421,
                                                }}
                                                title={"Location In"}
                                            />
                                        </MapView>
                                    </>
                                )}
                                {attendance?.locationOut && (
                                    <View style={{ marginTop: 20 }} >
                                        <Text>Lokasi absen pulang</Text>
                                        <MapView
                                            provider={PROVIDER_GOOGLE}
                                            style={{
                                                marginTop: 15,
                                                height: 202
                                            }}
                                            initialRegion={{
                                                latitude: +attendance?.locationOut.lat,
                                                longitude: +attendance?.locationOut.lng,
                                                latitudeDelta: 0.022,
                                                longitudeDelta: 0.001,
                                            }}
                                            showsUserLocation={true}
                                            zoomControlEnabled
                                            scrollEnabled={false}
                                        >
                                            <Marker
                                                coordinate={{
                                                    latitude: +attendance?.locationOut.lat,
                                                    longitude: +attendance?.locationOut.lng,
                                                    latitudeDelta: 0.0922,
                                                    longitudeDelta: 0.0421,
                                                }}
                                                title={"Location In"}
                                            />
                                        </MapView>
                                    </View>
                                )}
                            </View>
                        </View>
                    </View>
                )}
            </ScrollView>
        </View>
    )
};

export default DetailAttendanceEmployee;
