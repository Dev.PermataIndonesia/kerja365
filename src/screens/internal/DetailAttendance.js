import React, { useEffect } from 'react';
import { Text, View, TouchableOpacity, ScrollView, Image, Dimensions, ActivityIndicator } from 'react-native';
import { ILChevrontL, ILMoreVErtical } from '../../assets';
import MapView, { Marker, PROVIDER_GOOGLE } from 'react-native-maps'
import { useDispatch, useSelector } from 'react-redux';
import { fetchGetAttendance } from '../../store/reducer/attendanceReducer';
import dateConvert from '../../helpers/dateConvert';

const DetailAttendance = ({ navigation, route }) => {
    const dispatch = useDispatch()
    const { id } = route.params

    const token = useSelector(({ user }) => user.Token)
    const attendance = useSelector(({ attendance }) => attendance.GetAttendance)
    const loading = useSelector(({ attendance }) => attendance.Loading)

    useEffect(() => {
        if (token) dispatch(fetchGetAttendance(token, id))
    }, [token])

    return (
        <View style={{ flex: 1, backgroundColor: "#ffff" }} >
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 20, paddingHorizontal: 20 }}>
                <TouchableOpacity
                    onPress={() => navigation.goBack()}
                    style={{ width: 50 }}
                >
                    <ILChevrontL />
                </TouchableOpacity>
                {attendance?.dateIn && (
                    <View>
                        <Text style={{ fontSize: 16 }} >{dateConvert(new Date(attendance?.dateIn))}</Text>
                    </View>
                )}
                <View style={{ width: 50 }} />
            </View>
            {loading ? (
                <View style={{ padding: 20, justifyContent: 'center', alignItems: 'center' }} >
                    <ActivityIndicator size="large" color="#ff9901" />
                </View>
            ) : (
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.5)' }}
                >
                    <View style={{ paddingHorizontal: 20, marginTop: 10 }} >
                        <View style={{
                            padding: 20, borderRadius: 20, backgroundColor: '#ffff', flexDirection: 'row'
                        }} >
                            {attendance?.photo && (
                                <Image source={{ uri: attendance.photo }} style={{ width: 120, borderRadius: 120 / 2, height: 120 }} />
                            )}
                            <View style={{ padding: 20, alignSelf: 'center', flex: 1 }} >
                                <View style={{ flexDirection: "row" }} >
                                    <Text style={{ fontSize: 12, flex: 1 }} >Masuk</Text>
                                    <Text style={{ marginLeft: 10, flex: 1, fontSize: 12 }} > {new Date(attendance?.dateIn).getHours()}:{String(new Date(attendance?.dateIn).getMinutes()).length < 2 ? `0${new Date(attendance?.dateIn).getMinutes()}` : `${new Date(attendance?.dateIn).getMinutes()}`}</Text>
                                </View>
                                <View style={{ flexDirection: "row" }} >
                                    <Text style={{ fontSize: 12, flex: 1 }} >Pulang</Text>
                                    {attendance?.dateOut ? (
                                        <Text style={{ marginLeft: 10, flex: 1, fontSize: 12 }} > {new Date(attendance?.dateOut).getHours()}:{String(new Date(attendance?.dateOut).getMinutes()).length < 2 ? `0${new Date(attendance?.dateOut).getMinutes()}` : `${new Date(attendance?.dateOut).getMinutes()}`}</Text>
                                    ) : (
                                        <Text style={{ marginLeft: 10, flex: 1, fontSize: 12 }} > -------</Text>
                                    )}
                                </View>
                                <View style={{ flexDirection: "row" }} >
                                    <Text style={{ fontSize: 12, flex: 1 }} >Status</Text>
                                    <Text style={{ marginLeft: 10, flex: 1, fontSize: 12 }} > {attendance?.goIn ? "Masuk" : "Izin"}</Text>
                                </View>
                                {!attendance?.goIn && (
                                    <View style={{ marginTop: 10 }} >
                                        <Text style={{ fontSize: 12 }} >Desc: </Text>
                                        <Text style={{ textAlign: "justify", fontSize: 12, flex: 1 }} >{attendance?.desc}</Text>
                                    </View>
                                )}
                            </View>
                            <ILMoreVErtical />
                        </View>
                    </View>
                    <View style={{ padding: 20, marginTop: 20 }} >
                        {attendance?.locationIn && (
                            <View style={{
                                backgroundColor: "#ffff", borderRadius: 11, padding: 10
                            }} >
                                <Text style={{ color: "#2ecc71", fontWeight: "700" }} >Lokasi absen masuk</Text>
                                <MapView
                                    provider={PROVIDER_GOOGLE}
                                    style={{
                                        marginTop: 15,
                                        width: Dimensions.get('window').width - 60,
                                        height: 202
                                    }}
                                    initialRegion={{
                                        latitude: +attendance?.locationIn.lat,
                                        longitude: +attendance?.locationIn.lng,
                                        latitudeDelta: 0.022,
                                        longitudeDelta: 0.001,
                                    }}
                                    showsUserLocation={true}
                                    zoomControlEnabled
                                    scrollEnabled={false}
                                >
                                    <Marker
                                        coordinate={{
                                            latitude: +attendance?.locationIn.lat,
                                            longitude: +attendance?.locationIn.lng,
                                            latitudeDelta: 0.0922,
                                            longitudeDelta: 0.0421,
                                        }}
                                        title={"Location In"}
                                    />
                                </MapView>
                            </View>
                        )}
                        {attendance?.locationOut && (
                            <View style={{
                                backgroundColor: "#ffff", marginTop: 20, borderRadius: 11, padding: 10
                            }} >
                                <Text style={{ color: "#ff9901", fontWeight: "700" }} >Lokasi absen pulang</Text>
                                <MapView
                                    provider={PROVIDER_GOOGLE}
                                    style={{
                                        marginTop: 15,
                                        width: Dimensions.get('window').width - 60,
                                        height: 202
                                    }}
                                    initialRegion={{
                                        latitude: +attendance.locationOut.lat,
                                        longitude: +attendance.locationOut.lng,
                                        latitudeDelta: 0.022,
                                        longitudeDelta: 0.001,
                                    }}
                                    showsUserLocation={true}
                                    zoomControlEnabled
                                    scrollEnabled={false}
                                >
                                    <Marker
                                        coordinate={{
                                            latitude: +attendance.locationOut.lat,
                                            longitude: +attendance.locationOut.lng,
                                            latitudeDelta: 0.0922,
                                            longitudeDelta: 0.0421,
                                        }}
                                        title={"Location In"}
                                    />
                                </MapView>
                            </View>
                        )}
                    </View>
                </ScrollView>
            )}
        </View>
    )
};

export default DetailAttendance;
