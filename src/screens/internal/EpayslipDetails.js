import React, { useEffect, useState } from 'react';
import { SafeAreaView, ScrollView, Text, View, Dimensions, TouchableOpacity, Platform } from 'react-native';
import { ILChevrontL, ILDownload } from '../../assets';
import NumberFormat from 'react-number-format';
import { showMessage } from 'react-native-flash-message';
import instance from '../../config/axios';
import { useSelector } from 'react-redux';
import RNFS from 'react-native-fs';
import { ActivityIndicator } from 'react-native';
import RNFetchBlob from 'rn-fetch-blob';

const EpayslipDetails = ({ navigation, route }) => {
    const { date, slip } = route.params

    const token = useSelector(({ user }) => user.Token)

    const [download, setDownload] = useState(false)

    const createPdf = () => {
        setDownload(true)
        const fileName = `${slip.payslip.nrk} - ${slip.payslip.bulan_penggajian}.pdf`

        let dirs = RNFetchBlob.fs.dirs
        const path = Platform.OS == 'ios' ? dirs.DocumentDir + '/' + fileName : `${RNFS.ExternalStorageDirectoryPath}/Download/` + fileName

        RNFetchBlob.fs.exists(path)
            .then(async exist => {
                try {
                    if (exist) {
                        showMessage({
                            type: "info",
                            message: 'File already exists',
                        })
                    } else {
                        await instance.post(`/epayslip/generate-to-pdf`, slip, {
                            headers: {
                                access_token: token
                            }
                        })
                        await RNFetchBlob
                            .config({
                                fileCache: true,
                                appendExt: 'pdf',
                                title: fileName,
                                addAndroidDownloads: {
                                    useDownloadManager: true,
                                    description: 'File downloading',
                                    title: fileName,
                                    mime: 'application/pdf',
                                    notification: true,
                                    path: path,
                                }
                            })
                            .fetch('GET', `https://k365.permataindonesia.com/epayslip/file/${slip.payslip.nrk} - ${slip.payslip.bulan_penggajian}.pdf`, {})

                        showMessage({
                            type: "success",
                            message: "Success download",
                        })
                    }
                } catch (error) {
                    showMessage({
                        type: 'warning',
                        message: 'Oops something wrong',
                    })
                }
            })

        setDownload(false)
    }

    return (
        <>
            <SafeAreaView
                style={{ flex: 1, backgroundColor: '#ffff' }}
            >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 50, paddingHorizontal: 20 }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={{ width: 50 }}
                    >
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 16 }} >{date}</Text>
                    </View>
                    <View style={{ width: 50 }} />
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                >
                    <View
                        style={{ paddingVertical: 20, paddingHorizontal: 40, }}
                    >
                        <>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                                <Text style={{ fontWeight: '700', flex: 1 }} >Rutin</Text>
                                <View style={{ flex: 2, marginLeft: 50, borderRadius: 10, height: 7, backgroundColor: '#eeee' }} />
                            </View>
                            <View style={{ marginTop: 20 }} >
                                {slip?.payslip?.pendapatan?.map(el => (
                                    <View
                                        key={el.payroll_code}
                                        style={{ flexDirection: 'row', justifyContent: 'space-between' }}
                                    >
                                        <Text style={{ flex: 2 }}>{el.payroll_code}</Text>
                                        <View style={{ flex: 1 }} >
                                            <NumberFormat
                                                value={el.payroll_value}
                                                displayType={'text'}
                                                thousandSeparator={true}
                                                prefix='Rp. '
                                                renderText={val => (
                                                    <Text style={{}} >{val}</Text>
                                                )}
                                            />
                                        </View>
                                    </View>
                                ))}
                            </View>
                        </>
                        <View style={{ marginTop: 50 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                                <Text style={{ fontWeight: '700', flex: 1 }} >Tunjangan</Text>
                                <View style={{ flex: 2, marginLeft: 50, borderRadius: 10, height: 7, backgroundColor: '#eeee' }} />
                            </View>
                            {slip?.payslip?.tunjangan?.map(el => (
                                <View
                                    key={el.payroll_code}
                                    style={{ flexDirection: 'row', marginTop: 20, justifyContent: 'space-between' }}
                                >
                                    <Text style={{ flex: 2 }}>{el.payroll_code}</Text>
                                    <View style={{ flex: 1 }} >
                                        <NumberFormat
                                            value={el.payroll_value}
                                            displayType={'text'}
                                            thousandSeparator={true}
                                            prefix='Rp. '
                                            renderText={val => (
                                                <Text style={{}} >{val}</Text>
                                            )}
                                        />
                                    </View>
                                </View>
                            ))}
                        </View>
                        <View style={{ marginTop: 50 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                                <Text style={{ fontWeight: '700', flex: 1 }} >Tidak Rutin</Text>
                                <View style={{ flex: 2, marginLeft: 50, borderRadius: 10, height: 7, backgroundColor: '#eeee' }} />
                            </View>
                            {slip?.payslip?.lembur?.map(el => (
                                <View
                                    key={el.payroll_code}
                                    style={{ flexDirection: 'row', marginTop: 20, justifyContent: 'space-between' }}
                                >
                                    <Text style={{ flex: 2 }}>{el.payroll_code}</Text>
                                    <NumberFormat
                                        value={el.payroll_value}
                                        displayType={'text'}
                                        thousandSeparator={true}
                                        prefix='Rp. '
                                        renderText={val => (
                                            <Text style={{ flex: 1 }} >{val}</Text>
                                        )}
                                    />
                                </View>
                            ))}
                        </View>

                        <View style={{ marginTop: 50 }} >
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                                <Text style={{ fontWeight: '700', flex: 1 }} >Potongan</Text>
                                <View style={{ flex: 2, marginLeft: 50, borderRadius: 10, height: 7, backgroundColor: '#eeee' }} />
                            </View>
                            {slip?.payslip?.potongan?.map(el => (
                                <View
                                    key={el.payroll_code}
                                    style={{ flexDirection: 'row', marginTop: 20, justifyContent: 'space-between' }}
                                >
                                    <Text style={{ flex: 2 }}>{el.payroll_code}</Text>
                                    <NumberFormat
                                        value={el.payroll_value}
                                        displayType={'text'}
                                        thousandSeparator={true}
                                        prefix='Rp. '
                                        renderText={val => (
                                            <Text style={{ flex: 1 }} >{val}</Text>
                                        )}
                                    />
                                </View>
                            ))}
                        </View>

                        <View style={{ marginTop: 50 }} >
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                                <Text style={{ fontWeight: '700' }} >Total</Text>
                                <View style={{ flex: 1, marginLeft: 50, borderRadius: 10, height: 7, backgroundColor: '#eeee' }} />
                            </View>
                            <NumberFormat
                                value={+slip?.payslip?.thp}
                                displayType={'text'}
                                thousandSeparator={true}
                                prefix='Rp. '
                                renderText={val => (
                                    <Text style={{ alignSelf: 'flex-end', marginTop: 20, fontSize: 18, fontWeight: '700' }} >{val}</Text>
                                )}
                            />
                        </View>

                        <View>
                            {download ? (
                                <ActivityIndicator size='large' color='#ff9901' style={{ marginTop: 30 }} />
                            ) : (
                                <TouchableOpacity
                                    onPress={createPdf}
                                    style={{ height: 70, backgroundColor: '#eeee', borderRadius: 50, marginTop: 30, justifyContent: 'center', alignItems: 'center' }}
                                >
                                    <ILDownload />
                                    <Text style={{ fontWeight: '700' }} >Download file</Text>
                                </TouchableOpacity>
                            )}
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </>

    )
};

export default EpayslipDetails;
