import React, { useState } from 'react';
import { Text, View, TouchableOpacity, ScrollView, TextInput, Image, ActivityIndicator } from 'react-native';
import { ILChevrontL, ILCalendarY, ILSeacrhW, ILMoreVErtical, ILDownload } from '../../assets';
import DateTimePicker from '@react-native-community/datetimepicker';
import dateConvert from '../../helpers/dateConvert';
import { useInput } from '../../customHook';
import Avatar from '../../assets/img/avatar.png'
import { useDispatch, useSelector } from 'react-redux';
import { fetchEmployeeActtendanceRecaps } from '../../store/reducer/employeeReducer';

const AttendanceRecap = ({ navigation }) => {
    const dispatch = useDispatch()

    const token = useSelector(({ user }) => user.Token)
    const attendances = useSelector(({ employees }) => employees.AttendaceRecaps)
    const loading = useSelector(({ employees }) => employees.Loading)

    const startDate = useInput(new Date())
    const endDate = useInput(new Date())
    const [name, setName] = useState('')

    const onPress = async () => {
        if (token) dispatch(fetchEmployeeActtendanceRecaps(token, startDate.date, endDate.date, name))

        console.log(startDate.date, endDate.date, name)
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#ffff' }} >
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 50, paddingHorizontal: 20 }}>
                <TouchableOpacity
                    onPress={() => navigation.goBack()}
                    style={{ width: 50 }}
                >
                    <ILChevrontL />
                </TouchableOpacity>
                <View>
                    <Text style={{ fontSize: 16 }} >Rekap Absen</Text>
                </View>
                <View style={{ width: 50 }} />
            </View>
            <View style={{ padding: 20 }} >
                <View style={{ flexDirection: 'row' }} >
                    <View style={{ flex: 1 }} >
                        <TouchableOpacity
                            onPress={startDate.showDatePicker}
                            style={{ flexDirection: 'row', alignItems: 'center', borderWidth: 1, borderColor: '#eeee', backgroundColor: '#ffff', padding: 20, borderRadius: 11 }}
                        >
                            <ILCalendarY />
                            <Text style={{ marginLeft: 10, color: '#ff9901' }} >{dateConvert(startDate.date)}</Text>
                        </TouchableOpacity>
                        {startDate.show && (
                            <DateTimePicker
                                testID="dateTimePicker1"
                                value={startDate.date}
                                mode={startDate.mode}
                                is24Hour={true}
                                display='spinner'
                                onChange={startDate.onChange}
                            />
                        )}
                    </View>
                    <View style={{ width: 20 }} />
                    <View style={{ flex: 1 }} >
                        <TouchableOpacity
                            onPress={endDate.showDatePicker}
                            style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#ffff', borderWidth: 1, borderColor: '#eeee', padding: 20, borderRadius: 11 }}
                        >
                            <ILCalendarY />
                            <Text style={{ marginLeft: 10, color: '#ff9901' }} >{dateConvert(endDate.date)}</Text>
                        </TouchableOpacity>
                        {endDate.show && (
                            <DateTimePicker
                                testID="dateTimePicker1"
                                value={endDate.date}
                                mode={endDate.mode}
                                is24Hour={true}
                                display='spinner'
                                onChange={endDate.onChange}
                            />
                        )}
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center' }} >
                    <View style={{ flex: 1 }} >
                        <TextInput
                            style={{ borderColor: '#eeee', borderWidth: 1, borderRadius: 10, height: 61, paddingHorizontal: 10, color: 'black' }}
                            placeholder="Cari nama"
                            placeholderTextColor="#c4c4c4"
                            value={name}
                            onChangeText={val => setName(val)}
                        />
                    </View>
                    <View style={{ width: 20 }} />
                    <TouchableOpacity
                        onPress={onPress}
                        style={{ width: 71, height: 61, borderRadius: 10, backgroundColor: '#ff9901', justifyContent: 'center', alignItems: 'center' }}
                    >
                        <ILSeacrhW />
                    </TouchableOpacity>
                </View>
            </View>
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
            >
                {loading ? (
                    <View style={{ padding: 20, justifyContent: 'center', alignItems: 'center' }} >
                        <ActivityIndicator size="large" color="#ff9901" />
                        <View style={{ marginTop: 20 }} >
                            <Text>Loading ....</Text>
                        </View>
                    </View>
                ) : (
                    <View style={{ padding: 20 }} >
                        {Object.keys(attendances).length > 0 && (
                            <View
                                style={{ backgroundColor: '#ffff', padding: 20, borderRadius: 10 }}
                            >
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
                                    <View>
                                        <Image source={Avatar} style={{ width: 60, height: 60, borderRadius: 60 / 2 }} />
                                    </View>
                                    <View style={{ flex: 1, paddingHorizontal: 10 }} >
                                        <Text style={{ fontSize: 16, fontWeight: '700' }} >{attendances?.name}</Text>
                                        <Text style={{ fontSize: 12 }} >{attendances?.position}</Text>
                                        <Text style={{ fontSize: 12 }}>{attendances?.region}</Text>
                                    </View>
                                    <View>
                                        <ILMoreVErtical />
                                    </View>
                                </View>
                                <View style={{ marginTop: 30, paddingHorizontal: 40, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
                                    <View style={{ height: 60, alignItems: 'center', justifyContent: 'space-between' }} >
                                        <Text>Hadir</Text>
                                        <Text>{attendances?.presents?.length}</Text>
                                    </View>
                                    <View style={{ height: 50, width: 7, backgroundColor: '#E5E5E5', borderRadius: 10 }} />
                                    <View style={{ height: 60, alignItems: 'center', justifyContent: 'space-between' }}>
                                        <Text>Izin</Text>
                                        <Text>{attendances?.permissions?.length}</Text>
                                    </View>
                                    <View style={{ height: 50, width: 7, backgroundColor: '#E5E5E5', borderRadius: 10 }} />
                                    <View style={{ height: 60, alignItems: 'center', justifyContent: 'space-between' }}>
                                        <Text>Cuti</Text>
                                        <Text>{attendances?.paid_leave?.length}</Text>
                                    </View>
                                </View>
                            </View>

                        )}
                    </View>
                )}

            </ScrollView>
        </View>
    )
};

export default AttendanceRecap;
