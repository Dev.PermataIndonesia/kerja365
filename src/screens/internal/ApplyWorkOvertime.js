import React, { useState } from 'react';
import { Text, View, TouchableOpacity, ScrollView, TextInput } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import { useDispatch, useSelector } from 'react-redux';
import { ILChevrontL } from '../../assets';
import instance from '../../config/axios';
import { fetchOvertimes } from '../../store/reducer/workOvertimeReducer';

const ApplyWorkOvertime = ({ navigation }) => {
    const dispatch = useDispatch()

    const [title, setTitle] = useState('')
    const [desc, setDesc] = useState('')

    const token = useSelector(({ user }) => user.Token)

    const submit = async () => {
        try {
            if (!title || !desc) throw { message: 'Field required' }
            const { data } = await instance.post('/work-overtime', { title, desc }, {
                headers: {
                    access_token: token
                }
            })
            if (data?.waiting) {
                throw { message: data?.message }
            } else {
                dispatch(fetchOvertimes(token))
                showMessage({
                    type: 'success',
                    message: 'Apply success'
                })
                navigation.goBack()
            }

        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops!! something wrong',
                description: error.message,
                duration: 5000
            })
        }
    }

    return (
        <>
            <View style={{ flex: 1, backgroundColor: '#ffff' }} >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 50, paddingHorizontal: 20 }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={{ width: 50 }}
                    >
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 16 }} >Pengajuan Lembur</Text>
                    </View>
                    <View style={{ width: 50 }} />
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.5)' }}
                >
                    <View style={{ padding: 20 }} >
                        <View>
                            <TextInput
                                style={{ height: 64, borderRadius: 10, flex: 1, backgroundColor: '#ffff', paddingHorizontal: 20, color: 'black' }}
                                placeholder="Keterangan singkat"
                                placeholderTextColor="#c4c4c4"
                                value={title}
                                onChangeText={val => setTitle(val)}
                            />
                        </View>
                        <View style={{ marginTop: 20 }} >
                            <TextInput
                                style={{ height: 346, borderRadius: 10, flex: 1, backgroundColor: '#ffff', paddingHorizontal: 20, color: 'black' }}
                                placeholder="Deskripsi Pekerjaan"
                                placeholderTextColor="#c4c4c4"
                                textAlignVertical='top'
                                value={desc}
                                onChangeText={val => setDesc(val)}
                            />
                        </View>
                    </View>
                </ScrollView>
                <View style={{ padding: 20 }} >
                    <TouchableOpacity
                        onPress={submit}
                        style={{ padding: 20, backgroundColor: '#ff9901', borderRadius: 10, justifyContent: 'center', alignItems: 'center' }}
                    >
                        <Text style={{ fontWeight: '700', color: '#ffff' }} >Ajukan Lembur</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </>
    )
};

export default ApplyWorkOvertime;
