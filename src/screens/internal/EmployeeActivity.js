import React, { useEffect, useRef, useState } from 'react';
import { Text, View, TouchableOpacity, Animated, ScrollView, TextInput, Image, ActivityIndicator } from 'react-native';
import { ILChevrontL, ILCalendarY, ILSeacrhW, ILMoreVErtical, ILEllipse4, ILChevrontDY, ILChevrontRY, } from '../../assets';
import DateTimePicker from '@react-native-community/datetimepicker';
import dateConvert from '../../helpers/dateConvert';
import { useInput } from '../../customHook';
import Avatar from '../../assets/img/avatar.png'
import { useDispatch, useSelector } from 'react-redux';
import { fetchEmployeeActivities, fetchEmployeeActivitiesSearch } from '../../store/reducer/employeeReducer';

const EmployeeActivity = ({ navigation }) => {
    const dispatch = useDispatch()

    const startDate = useInput(new Date())
    const endDate = useInput(new Date())

    const token = useSelector(({ user }) => user.Token)
    const activities = useSelector(({ employees }) => employees.Activities)
    const loading = useSelector(({ employees }) => employees.Loading)

    const [name, setName] = useState('')
    const [id, setId] = useState('')

    useEffect(() => {
        if (token) dispatch(fetchEmployeeActivities(token))
    }, [token])

    const onPress = () => {
        if (token) dispatch(fetchEmployeeActivitiesSearch(token, startDate.date, endDate.date, name))
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#ffff' }} >
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingBottom: 10, paddingHorizontal: 20 }}>
                <TouchableOpacity
                    onPress={() => navigation.goBack()}
                    style={{ width: 50 }}
                >
                    <ILChevrontL />
                </TouchableOpacity>
                <View>
                    <Text style={{ fontSize: 16 }} >Aktifitas Karyawan</Text>
                </View>
                <View style={{ width: 50 }} />
            </View>
            <View style={{ padding: 20 }} >
                <View style={{ flexDirection: 'row' }} >
                    <View style={{ flex: 1 }} >
                        <TouchableOpacity
                            onPress={startDate.showDatePicker}
                            style={{ flexDirection: 'row', alignItems: 'center', borderWidth: 1, borderColor: '#eeee', backgroundColor: '#ffff', padding: 20, borderRadius: 11 }}
                        >
                            <ILCalendarY />
                            <Text style={{ marginLeft: 10, color: '#ff9901' }} >{dateConvert(startDate.date)}</Text>
                        </TouchableOpacity>
                        {startDate.show && (
                            <DateTimePicker
                                testID="dateTimePicker1"
                                value={startDate.date}
                                mode={startDate.mode}
                                is24Hour={true}
                                display='spinner'
                                onChange={startDate.onChange}
                            />
                        )}
                    </View>
                    <View style={{ width: 20 }} />
                    <View style={{ flex: 1 }} >
                        <TouchableOpacity
                            onPress={endDate.showDatePicker}
                            style={{ flexDirection: 'row', alignItems: 'center', backgroundColor: '#ffff', borderWidth: 1, borderColor: '#eeee', padding: 20, borderRadius: 11 }}
                        >
                            <ILCalendarY />
                            <Text style={{ marginLeft: 10, color: '#ff9901' }} >{dateConvert(endDate.date)}</Text>
                        </TouchableOpacity>
                        {endDate.show && (
                            <DateTimePicker
                                testID="dateTimePicker1"
                                value={endDate.date}
                                mode={endDate.mode}
                                is24Hour={true}
                                display='spinner'
                                onChange={endDate.onChange}
                            />
                        )}
                    </View>
                </View>
                <View style={{ flexDirection: 'row', marginTop: 20, alignItems: 'center' }} >
                    <View style={{ flex: 1 }} >
                        <TextInput
                            style={{ borderColor: '#eeee', color: 'black', borderWidth: 1, borderRadius: 10, height: 61, paddingHorizontal: 10 }}
                            placeholder="Cari nama"
                            placeholderTextColor="#c4c4c4"
                            value={name}
                            onChangeText={val => setName(val)}
                        />
                    </View>
                    <View style={{ width: 20 }} />
                    <TouchableOpacity
                        onPress={onPress}
                        style={{ width: 71, height: 61, borderRadius: 10, backgroundColor: '#ff9901', justifyContent: 'center', alignItems: 'center' }}
                    >
                        <ILSeacrhW />
                    </TouchableOpacity>
                </View>
            </View>
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
            >
                {loading ? (
                    <View style={{ padding: 20, justifyContent: 'center', alignItems: 'center' }} >
                        <ActivityIndicator size="large" color="#ff9901" />
                        <View style={{ marginTop: 20 }} >
                            <Text>Loading ....</Text>
                        </View>
                    </View>
                ) : (
                    <>
                        {activities?.length > 0 && activities?.map(item => (
                            <View
                                key={item._id}
                                style={{ padding: 20 }}
                            >
                                <View
                                    style={{ backgroundColor: '#ffff', padding: 20, borderRadius: 10 }}
                                >
                                    <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
                                        <View>
                                            <Image source={Avatar} style={{ width: 60, height: 60, borderRadius: 60 / 2 }} />
                                        </View>
                                        <View style={{ flex: 1, paddingHorizontal: 10 }} >
                                            <Text style={{ fontSize: 16, fontWeight: '700' }} >{item?.user?.name}</Text>
                                            <Text style={{ fontSize: 12 }} >{item?.user?.position}</Text>
                                            <Text style={{ fontSize: 12 }}>{item?.user?.region}</Text>
                                        </View>
                                        <View>
                                            <ILMoreVErtical />
                                        </View>
                                    </View>
                                    <View style={{ marginTop: 30, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }} >
                                        <View style={{ height: 50, width: 7, backgroundColor: '#E5E5E5', borderRadius: 10 }} />
                                        <View style={{ flex: 1, paddingHorizontal: 10 }} >
                                            <Text>{item?.title}</Text>
                                            <Text>{new Date(item?.createdAt).getDate()} - {new Date(item?.createdAt).getMonth() + 1} - {new Date(item?.createdAt).getFullYear()}</Text>
                                        </View>
                                        <View>
                                            <ILEllipse4 />
                                        </View>
                                    </View>

                                    <View>
                                        <View style={{ marginTop: 20, flexDirection: 'row', alignItems: 'center' }} >
                                            <View style={{ flex: 1, height: 1, backgroundColor: '#eeee' }} />
                                            <View style={{ width: 10 }} />
                                            <TouchableOpacity
                                                onPress={() => {
                                                    if (id === item._id) setId('')
                                                    else setId(item._id)
                                                }
                                                }
                                                style={{ flexDirection: 'row', alignItems: 'center' }}
                                            >
                                                <Text style={{ color: '#ff9901' }} >Show details</Text>
                                                {id === item._id ? (
                                                    <ILChevrontDY />
                                                ) : (
                                                    <ILChevrontRY />
                                                )}
                                            </TouchableOpacity>
                                        </View>
                                        {id === item._id && (
                                            <View style={{ paddingVertical: 10 }} ><Text style={{ paddingVertical: 5 }} >{item?.desc}</Text>
                                            </View>
                                        )}
                                    </View>
                                    <View style={{ marginTop: 20, flexDirection: 'row', justifyContent: 'flex-end' }} >
                                        <View
                                            style={{ width: 75, height: 30, alignItems: 'center', justifyContent: 'center', borderWidth: 1, borderRadius: 5, borderColor: item?.status === 'Done' ? '#42B442' : '#EA2027' }} >
                                            <Text style={{ color: item?.status === 'Done' ? '#42B442' : '#EA2027' }} >{item?.status}</Text>
                                        </View>
                                        <View style={{ width: 10 }} />
                                    </View>
                                </View>
                            </View>
                        ))}
                    </>
                )}


            </ScrollView>
        </View>
    )
};

export default EmployeeActivity;
