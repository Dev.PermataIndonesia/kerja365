import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, Keyboard } from 'react-native';
import {
    CodeField,
    Cursor,
    useBlurOnFulfill,
    useClearByFocusCell,
} from 'react-native-confirmation-code-field';
import { setData } from '../../utils/localStorage';
import FlashMessage, { showMessage } from 'react-native-flash-message';
import auth from '@react-native-firebase/auth';
import { useDispatch, useSelector } from 'react-redux';
import { fetchUserByEmail } from '../../store/reducer/userReducer';
import { Loading } from '../../components'
import instance from '../../config/axios'
import messaging from '@react-native-firebase/messaging';

const CELL_COUNT = 6;

const ConfirmCode = ({ navigation, route }) => {
    const dispatch = useDispatch()
    const { phoneNumber, token, email, dataInternal } = route.params;
    const [value, setValue] = useState('');
    const ref = useBlurOnFulfill({ value, cellCount: CELL_COUNT });
    const [props, getCellOnLayoutHandler] = useClearByFocusCell({
        value,
        setValue,
    });
    const user = useSelector(({ user }) => user.User)
    const confirm = useSelector(({ user }) => user.Confirm)

    const [loading, setLoading] = useState(false)
    const [fcmToken, setFcmToken] = useState(null)

    useEffect(() => {
        (async () => {
            setFcmToken(await messaging().getToken())
        })()
    }, [])

    useEffect(() => {
        if (Object.keys(confirm).length < 1) setLoading(true)
        else {
            if (confirm?.code) setValue(`${confirm.code}`)
            setLoading(false)
        }

        if (dataInternal) dispatch({ type: 'SET_USER', payload: dataInternal })
        else dispatch(fetchUserByEmail(email))
    }, [confirm])

    const saveTokenToDatabase = async (fcmToken, token, user) => {
        let id
        if (user.type === 'internal' || user.type === 'tko') {
            id = user.nrk
        }
        else id = user._id

        await instance.patch(`/user/fcm-token/${id}`, { fcmToken }, {
            headers: {
                access_token: token
            }
        })

    }

    const onPress = async () => {
        setLoading(true)
        try {
            if (!user || !token) throw { message: 'You dont have permission to login' }

            const credential = auth.PhoneAuthProvider.credential(confirm.verificationId, value)
            if (credential) {
                await auth().signInWithCredential(credential)
                setData('user', user)
                setData('token', token)


                if (user?.type === 'fulltimer') {
                    saveTokenToDatabase(fcmToken, token, user)
                    navigation.replace('MainApp')
                }

                if (dataInternal?.type === 'internal' || dataInternal?.type === 'tko') {
                    saveTokenToDatabase(fcmToken, token, dataInternal)
                    navigation.replace('InternalApp')
                }

            } else throw { message: 'Invalid Credential' }
        } catch (error) {
            showMessage({
                message: error.message,
                type: 'danger'
            })
        }
        setLoading(false)
    }

    const resend = async () => {
        const authWPhone = await auth().verifyPhoneNumber(phoneNumber)
        dispatch({ type: 'SET_CONFIRM', payload: authWPhone })
    }

    return (
        <>
            <View style={{ flex: 1, justifyContent: 'space-between', alignItems: 'center', paddingVertical: 57, backgroundColor: '#ffff' }} >
                <FlashMessage position='top' />
                <View>
                    <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 30, textAlign: 'center' }} >Auth</Text>
                    <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 20, color: '#6B6969', textAlign: 'center' }} >Please insert your code</Text>
                </View>

                <View>
                    <CodeField
                        ref={ref}
                        {...props}
                        value={value}
                        autoFocus
                        onChangeText={setValue}
                        cellCount={CELL_COUNT}
                        rootStyle={styles.codeFieldRoot}
                        keyboardType="number-pad"
                        textContentType="oneTimeCode"
                        renderCell={({ index, symbol, isFocused }) => (
                            <Text
                                key={index}
                                style={[styles.cell, isFocused && styles.focusCell]}
                                onLayout={getCellOnLayoutHandler(index)}>
                                {symbol || (isFocused ? <Cursor /> : null)}
                            </Text>
                        )}
                    />
                </View>

                <TouchableOpacity onPress={resend} >
                    <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 20, color: '#6B6969' }} >Resend code</Text>
                </TouchableOpacity>


                {confirm !== null && (
                    <TouchableOpacity
                        onPress={onPress}
                        style={{
                            paddingVertical: 20, width: 200, backgroundColor: '#ff9901', borderRadius: 25
                        }}
                    >
                        <Text style={{ fontFamily: 'DMSans-Bold', color: '#ffff', textAlign: 'center' }} >Submit</Text>
                    </TouchableOpacity>
                )}
            </View>
            {loading && <Loading />}
        </>
    )
};

export default ConfirmCode;

const styles = StyleSheet.create({
    root: { flex: 1, padding: 20 },
    title: { textAlign: 'center', fontSize: 30 },
    codeFieldRoot: { marginTop: 20 },
    cell: {
        width: 40,
        height: 40,
        lineHeight: 38,
        fontSize: 24,
        borderWidth: 1,
        borderColor: '#ffff',
        textAlign: 'center',
    },
    focusCell: {
        borderColor: '#ff9901',
    },
});
