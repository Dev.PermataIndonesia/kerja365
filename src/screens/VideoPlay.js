import React, { useState, useRef, useCallback, useEffect } from 'react';
import { Text, View, Alert, TouchableOpacity, Image, ScrollView, ActivityIndicator } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import YoutubePlayer from 'react-native-youtube-iframe'
import { ILChevrontL, ILMoreVErtical } from '../assets';
import { useDispatch, useSelector } from 'react-redux';
import { fetchVideos } from '../store/reducer/videosReducer';


const VideoPlay = ({ navigation, route }) => {
    const dispatch = useDispatch()

    const { video } = route.params
    const [getVideo, setVideo] = useState(JSON.parse(video))

    const [playing, setPlaying] = useState(false);

    const videos = useSelector(({ videos }) => videos.Videos)
    let loading = useSelector(({ videos }) => videos.Loading)

    useEffect(() => {
        dispatch(fetchVideos(0, 0))
    }, [getVideo])

    const onStateChange = useCallback((state) => {
        if (state === "ended") {
            setPlaying(false);
            Alert.alert("video has finished playing!");
        }
    }, []);

    const togglePlaying = useCallback(() => {
        setPlaying((prev) => !prev);
    }, []);


    const onError = (err) => {
        showMessage({
            type: 'warning',
            message: 'Oops something wrong',
            description: err.message
        })
    }

    return (
        <View
            style={{ flex: 1, background: "#ffff" }}
        >
            <View style={{ flexDirection: 'row', padding: 20, backgroundColor: '#ffff', justifyContent: 'space-between', alignItems: 'center' }}>
                <TouchableOpacity style={{ width: 30 }} onPress={() => navigation.goBack()}>
                    <ILChevrontL />
                </TouchableOpacity>
                <View style={{ flex: 1 }} >
                    <Text style={{ fontSize: 16, textAlign: 'center', }} >Videos</Text>
                </View>
                <View style={{ width: 30 }} />
            </View>
            {loading ? (
                <ActivityIndicator size="large" color="#ff9901" />
            ) : (
                <View style={{ padding: 20, backgroundColor: '#ffff' }} >
                    <View style={{}} >
                        <YoutubePlayer
                            height={220}
                            play={playing}
                            videoId={`${getVideo?.embed}`}
                            onChangeState={onStateChange}
                            onError={onError}
                        />
                        <Text>{getVideo?.title}</Text>
                    </View>
                </View>
            )}
            <ScrollView showsVerticalScrollIndicator={false} style={{ flex: 1, backgroundColor: 'rgba(238,238,238,0.7)', paddingHorizontal: 20 }} >
                {/* <Button title={playing ? "pause" : "play"} onPress={togglePlaying} /> */}
                {loading ? (
                    <View style={{ marginTop: 20, justifyContent: 'center', alignItems: 'center' }} >
                        <Text style={{ fontSize: 16, color: '#ff9901', fontWeight: '700' }} >Loading....</Text>
                    </View>
                ) : (
                    <>
                        {videos && videos?.filter(el => el.embed !== getVideo.embed).map((video, i) => (
                            <TouchableOpacity
                                key={i}
                                onPress={() => setVideo(video)}
                                style={{ padding: 10, marginTop: 20, backgroundColor: '#ffff', borderRadius: 11, flexDirection: 'row' }}
                            >
                                <Image source={{ uri: video.thumbnail }} resizeMode="cover" style={{ height: 120, borderRadius: 7, flex: 2 }} />
                                <View style={{ flex: 1, justifyContent: 'space-between', paddingHorizontal: 5 }} >
                                    <Text style={{ fontSize: 12, fontWeight: '400', color: '#2d3436' }} >{((video.title).length > 30) ? (((video.title).substring(0, 30 - 3)) + '...') : video.title}</Text>
                                    <View style={{ alignSelf: 'flex-end' }} >
                                        <ILMoreVErtical />
                                    </View>
                                </View>
                            </TouchableOpacity>
                        ))}
                    </>
                )}
            </ScrollView>
        </View>
    )
};

export default VideoPlay;
