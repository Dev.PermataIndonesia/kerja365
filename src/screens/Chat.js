import React, { useState, useEffect } from 'react';
import { Image } from 'react-native';
import { TextInput } from 'react-native';
import { Text, View, ScrollView, TouchableOpacity, Keyboard } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ILChevrontL, ILSend1 } from '../assets';
import Avatar from '../assets/img/avatar.png';
import { fetchChatting } from '../store/reducer/chattingReducer';
import { getData } from '../utils/localStorage';
import database from '@react-native-firebase/database';
import { showMessage } from 'react-native-flash-message';
import { IsMe, Other } from '../components/mikro/ChatItem';
import Skeleton from 'react-native-skeleton-placeholder';
import instance from '../config/axios';

const ChatItem = ({ isMe, chat, getUser }) => {
    if (isMe) {
        return <IsMe text={chat.chatContent} date={chat.chatTime} />
    }
    return <Other text={chat.chatContent} date={chat.chatTime} photo={getUser?.photo ? { uri: getUser?.photo } : Avatar} />
}

const Chat = ({ navigation, route }) => {
    const dispatch = useDispatch()

    const { partner } = route.params

    const [message, setMessage] = useState('')

    const currentUser = useSelector(({ user }) => user.User)
    const token = useSelector(({ user }) => user.Token)
    const chattings = useSelector(({ chattings }) => chattings.Chats)
    const loading = useSelector(({ chattings }) => chattings.Loading)

    useEffect(() => {
        (async () => {
            const data = await getData('user')
            dispatch({ type: 'SET_USER', payload: data })
        })()
        if (currentUser.type === 'company' || currentUser.type === 'internal') {
            let id
            currentUser.type === 'internal' ? id = currentUser.nrk : id = currentUser._id
            let partnerId
            partner.type === 'tko' ? partnerId = partner.nrk : partnerId = partner._id
            dispatch(fetchChatting(id, partnerId))
        } else {
            let id
            partner?.nrk ? id = partner.nrk : id = partner._id
            let currentUserId
            currentUser.type === 'tko' ? currentUserId = currentUser.nrk : currentUserId = currentUser._id
            dispatch(fetchChatting(id, currentUserId))
        }
    }, [dispatch, token])

    const sendMessage = async () => {

        const today = new Date()
        const hour = today.getHours()
        const minute = today.getMinutes()
        const date = today.getDate()
        const month = today.getMonth() + 1
        const year = today.getFullYear()
        const data = {
            sendBy: currentUser.type === 'internal' ? currentUser.nrk : currentUser._id,
            chatDate: new Date().getTime(),
            chatTime: `${hour}:${String(minute).length > 1 ? minute : `0${minute}`} ${hour > 12 ? 'PM' : 'AM'}`,
            chatContent: message
        }
        let chatId
        let urlMessageCompany
        let urlMessageJobseeker

        let historyChatCompany
        let historyChatForJobseeker

        if (currentUser.type === 'company' || currentUser.type === 'internal') {
            let id
            if (currentUser.type === 'internal') id = currentUser.nrk
            else id = currentUser._id

            let partnerId
            partner?.nrk ? partnerId = partner.nrk : partnerId = partner._id

            chatId = `${id}_${partnerId}`
            urlMessageCompany = `messages/${id}/${chatId}`
            urlMessageJobseeker = `messages/${partnerId}/${chatId}`
            historyChatCompany = {
                lastContentChat: message,
                lastChatDate: today.getTime(),
                uidPartner: partnerId
            }
            historyChatForJobseeker = {
                lastContentChat: message,
                lastChatDate: today.getTime(),
                uidPartner: id
            }
        } else {
            let id
            if (partner?.nrk) id = partner.nrk
            else id = partner._id
            let currentUserId
            currentUser.type === 'tko' ? currentUserId = currentUser.nrk : currentUserId = currentUser._id
            chatId = `${id}_${currentUserId}`
            urlMessageCompany = `messages/${id}/${chatId}`
            urlMessageJobseeker = `messages/${currentUserId}/${chatId}`
            historyChatForJobseeker = {
                lastContentChat: message,
                lastChatDate: today.getTime(),
                uidPartner: id
            }
            historyChatCompany = {
                lastContentChat: message,
                lastChatDate: today.getTime(),
                uidPartner: currentUserId
            }
        }

        const notification = {
            title: currentUser.type === 'internal' || currentUser.type === 'tko' ? currentUser.nama : currentUser.user_name,
            message: message,
            fcmToken: partner.fcmToken
        }

        try {
            setMessage('')
            Keyboard.dismiss()
            await database()
                .ref(
                    `chatting/${chatId}/allChat/${year}-${month}-${date}`
                )
                .push(data)

            await instance.post('/notification', notification, {
                headers: {
                    access_token: token
                }
            })
            await database().ref(`${urlMessageCompany}`).set(historyChatCompany)
            await database().ref(`${urlMessageJobseeker}`).set(historyChatForJobseeker)

        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops! something wrong',
                description: error.message
            })
        }
    }

    const onChangeText = (val) => {
        val = String(val).replace(/62|08|87|@mail/g, "***")
        setMessage(val)
    }

    return (
        <>
            {loading ? (
                <View style={{ flex: 1, backgroundColor: '#ffff' }} >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', padding: 20 }} >
                        {/* <View style={{ flex: 1 }} /> */}
                        <View style={{ justifyContent: 'center' }} >
                            <Skeleton>
                                <View style={{ width: 170, height: 20, borderRadius: 10 }} />
                            </Skeleton>
                            <Skeleton>
                                <View style={{ width: 150, marginTop: 10, height: 20, borderRadius: 10 }} />
                            </Skeleton>
                        </View>
                        <View>
                            <Skeleton>
                                <View style={{ width: 50, height: 50, borderRadius: 50 / 2 }} />
                            </Skeleton>
                        </View>
                    </View>
                    <View style={{ padding: 20 }} >
                        <View style={{ alignSelf: 'flex-end', height: 60 }} >
                            <Skeleton>
                                <View style={{ width: 170, height: 50, borderRadius: 10, borderBottomRightRadius: 0 }} />
                            </Skeleton>
                        </View>
                        <View style={{ alignSelf: 'flex-start', height: 80 }} >
                            <Skeleton>
                                <View style={{ width: 190, height: 70, borderRadius: 10, borderBottomLeftRadius: 0 }} />
                            </Skeleton>
                        </View>
                        <View style={{ alignSelf: 'flex-start', height: 140 }} >
                            <Skeleton>
                                <View style={{ width: 170, height: 130, borderRadius: 10, borderBottomLeftRadius: 0 }} />
                            </Skeleton>
                        </View>
                        <View style={{ alignSelf: 'flex-end', height: 60 }} >
                            <Skeleton>
                                <View style={{ width: 170, height: 50, borderRadius: 10, borderBottomRightRadius: 0 }} />
                            </Skeleton>
                        </View>
                        <View style={{ alignSelf: 'flex-end', height: 60 }} >
                            <Skeleton>
                                <View style={{ width: 100, height: 50, borderRadius: 10, borderBottomRightRadius: 0 }} />
                            </Skeleton>
                        </View>
                        <View style={{ alignSelf: 'flex-start', height: 40 }} >
                            <Skeleton>
                                <View style={{ width: 170, height: 35, borderRadius: 10, borderBottomLeftRadius: 0 }} />
                            </Skeleton>
                        </View>
                    </View>
                </View>
            ) : (
                <View
                    style={{ flex: 1, backgroundColor: '#ffff' }}
                >
                    <View
                        style={{
                            flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', padding: 20, backgroundColor: '#ffff', borderBottomWidth: 2, borderRightWidth: 2, borderLeftWidth: 2, borderColor: '#eeee'
                        }}
                    >
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                            style={{ width: 60 }}
                        >
                            <ILChevrontL />
                        </TouchableOpacity>
                        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                            <Text style={{ fontSize: 22, fontFamily: 'DMSans-Bold' }} >{partner?.user_name}</Text>
                            <Text style={{ fontFamily: 'DMSans-Regular' }} >{partner?.type === 'company' ? 'Company' : partner?.user_profession}</Text>
                        </View>
                        <View style={{ width: 60, justifyContent: 'center', alignItems: 'center' }}>
                            <Image source={partner?.photo === '' ? Avatar : { uri: partner?.photo }} style={{ width: 50, height: 50, borderRadius: 50 / 2 }} />
                        </View>
                    </View>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        style={{ backgroundColor: 'rgba(238, 238, 238, 0.3)', borderTopRightRadius: 50, borderTopLeftRadius: 50 }}
                        ref={scroll => {
                            this.scroll = scroll
                        }}
                        onContentSizeChange={() => this.scroll.scrollToEnd()}
                    >
                        <View style={{ paddingVertical: 20, paddingHorizontal: 20 }} >

                            {chattings && chattings.map(chatting => (
                                <View key={chatting.id} >
                                    <Text style={{ fontFamily: 'DMSans-Bold', color: 'rgba(0,0,0,0.5)', textAlign: 'center' }} >{chatting.id}</Text>
                                    {chatting.data.map(chat => {
                                        const isMe = currentUser?.nrk ? chat.data.sendBy === currentUser.nrk : chat.data.sendBy === currentUser._id
                                        return (
                                            <ChatItem
                                                key={chat.id}
                                                isMe={isMe}
                                                chat={chat.data}
                                                getUser={partner}
                                                currentUser={currentUser}
                                            />
                                        )
                                    })}
                                </View>
                            )
                            )}

                        </View>
                    </ScrollView>
                    <View style={{ backgroundColor: '#ffff' }} >
                        <View style={{ flexDirection: 'row', padding: 20 }} >
                            <TextInput
                                placeholder={`Send message to ${partner.user_name ? partner?.user_name?.substring(0, partner.user_name.indexOf(' ')) : partner?.user_name?.substring(0, partner.user_name.indexOf(' '))}`}
                                style={{ padding: 10, flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.3)', borderRadius: 11, color: 'black' }}
                                multiline={true}
                                value={message}
                                onChangeText={onChangeText}
                                placeholderTextColor='#c4c4c4'
                            />
                            <View style={{ width: 10 }} />
                            {message?.length > 0 ? (
                                <TouchableOpacity
                                    onPress={sendMessage}
                                    style={{
                                        backgroundColor: '#81ecec', width: 50, borderRadius: 11, justifyContent: 'center', alignItems: 'center'
                                    }}
                                >
                                    <ILSend1 />
                                </TouchableOpacity>
                            ) : (
                                <View style={{ backgroundColor: '#eeee', width: 50, borderRadius: 11, justifyContent: 'center', alignItems: 'center' }} >
                                    <ILSend1 />
                                </View>
                            )}
                        </View>
                    </View>
                </View>
            )}
        </>
    )
};

export default Chat;
