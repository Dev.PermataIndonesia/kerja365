import React, { useEffect, useState } from 'react';
import { ActivityIndicator, ScrollView, Text, Dimensions, View, TextInput, TouchableOpacity } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import FlashMessage, { showMessage } from 'react-native-flash-message'
import { ILChevrontL, ILFileText, ILStarWhite, ILTrash } from '../../../assets';
import instance from '../../../config/axios';
import { fetchBankAccounts, fetchDefaultBankAccount } from '../../../store/reducer/bankAccountReducer';
import useForm from '../../../helpers/useForm';

const BankAccount = ({ navigation }) => {
    const dispatch = useDispatch()

    const token = useSelector(({ user }) => user.Token)
    const bankAccounts = useSelector(({ bankAccounts }) => bankAccounts.BankAccounts)

    const [form, setForm] = useForm({
        bankName: '',
        accountNumber: '',
        accountHolder: ''
    })

    useEffect(() => {
        if (token) {
            dispatch(fetchBankAccounts(token))
        }
    }, [token])

    const onPress = async () => {
        try {
            if (bankAccounts?.length < 1) form.default = true
            else form.default = false

            await instance.post('/bank-account', form, {
                headers: {
                    access_token: token
                }
            })
            dispatch(fetchBankAccounts(token))
            if (bankAccounts?.length < 1) {
                dispatch(fetchDefaultBankAccount(token))
            }
            setForm('reset')
        } catch (error) {
            showMessage({
                type: 'danger',
                message: 'Oops something wrong'
            })
        }
    }

    const setAsDefault = async (id) => {
        try {
            await instance.patch(`/bank-account/${id}`, { default: true }, {
                headers: {
                    access_token: token
                }
            })
            dispatch(fetchBankAccounts(token))
            dispatch(fetchDefaultBankAccount(token))
            showMessage({
                type: 'success',
                message: 'Success set as default'
            })
        } catch (error) {
            showMessage({
                type: 'danger',
                message: 'Oops something wrong'
            })
        }
    }

    const compare = (a, b) => {
        return b.default - a.default
    }

    return (
        <View style={{ flex: 1, backgroundColor: '#ffff' }} >
            <FlashMessage position='top' />
            <View style={{ backgroundColor: '#ffff', marginTop: 20, paddingHorizontal: 20, paddingBottom: 60 }} >
                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                    <TouchableOpacity onPress={() => navigation.goBack()} >
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text>Bank Account</Text>
                    </View>
                    <View />
                </View>
            </View>
            <ScrollView style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.3)' }} >
                <View style={{ justifyContent: 'space-between', paddingHorizontal: 20, marginTop: 38 }} >
                    <View style={{ backgroundColor: '#ffff', padding: 20, borderRadius: 10 }} >
                        <Text>Bank accounts</Text>
                        <ScrollView
                            horizontal
                            showsHorizontalScrollIndicator={false}
                        >
                            {bankAccounts?.length > 0 && bankAccounts?.sort(compare).map((bankAccount, i) => (
                                <View key={bankAccount._id} style={{ marginLeft: i > 0 ? 20 : 0 }} >
                                    <View
                                        style={{ marginTop: 13, backgroundColor: '#FF9901', height: 168, borderRadius: 10, width: Dimensions.get('screen').width - 130 }} >
                                        <View style={{ padding: 20 }} >
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                                                <Text style={{ fontFamily: 'DMSans-Bold', color: 'white' }} >{bankAccount.bankName}</Text>
                                                <ILStarWhite />
                                            </View>
                                            <View style={{ marginTop: 30 }} >
                                                <Text style={{ fontFamily: 'DMSans-Bold', color: '#ffff', fontSize: 20 }} >{bankAccount.accountNumber}</Text>
                                            </View>
                                            <View style={{ marginTop: 30, alignSelf: 'flex-end' }} >
                                                <Text style={{ fontFamily: 'DMSans-Bold', color: '#ffff' }} >{bankAccount.accountHolder}</Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{ marginTop: 16, flexDirection: 'row', justifyContent: 'space-between' }} >
                                        {bankAccount.default === true ? (
                                            <Text>default</Text>
                                        ) : (
                                            <TouchableOpacity
                                                onPress={() => setAsDefault(bankAccount._id)}
                                            >
                                                <Text>Set as default</Text>
                                            </TouchableOpacity>
                                        )}
                                        <TouchableOpacity>
                                            <ILTrash />
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            ))}

                        </ScrollView>
                    </View>
                    <View style={{ marginTop: 24 }} >
                        <Text style={{ marginLeft: 20, fontFamily: 'DMSans-Bold' }} >Details bank account</Text>
                        <View style={{ marginTop: 22, padding: 10, backgroundColor: '#ffff', flexDirection: 'row', borderRadius: 10, alignItems: 'center' }} >
                            <ILFileText />
                            <TextInput
                                placeholder='Account Number'
                                placeholderTextColor='black'
                                style={{ marginLeft: 10 }}
                                value={form.accountNumber}
                                onChangeText={val => setForm('accountNumber', val)}
                            />
                        </View>
                        <View style={{ marginTop: 22, padding: 10, backgroundColor: '#ffff', flexDirection: 'row', borderRadius: 10, alignItems: 'center' }} >
                            <ILFileText />
                            <TextInput
                                placeholder='Account holder'
                                placeholderTextColor='black'
                                style={{ marginLeft: 10 }}
                                value={form.accountHolder}
                                onChangeText={val => setForm('accountHolder', val)}
                            />
                        </View>
                        <View style={{ marginTop: 22, padding: 10, backgroundColor: '#ffff', flexDirection: 'row', borderRadius: 10, alignItems: 'center' }} >
                            <ILFileText />
                            <TextInput
                                placeholder='Bank name'
                                placeholderTextColor='black'
                                style={{ marginLeft: 10 }}
                                value={form.bankName}
                                onChangeText={val => setForm('bankName', val)}
                            />
                        </View>
                    </View>
                    <View style={{ marginTop: 69, alignItems: 'center', paddingBottom: 50 }} >
                        <TouchableOpacity
                            onPress={onPress}
                            style={{ backgroundColor: '#FF9901', justifyContent: 'center', alignItems: 'center', width: 200, height: 50, borderRadius: 25 }}
                        >
                            <Text style={{ fontFamily: 'DMSans-Bold', color: '#ffff' }} >Submit</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
};

export default BankAccount;
