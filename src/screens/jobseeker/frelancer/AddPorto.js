import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ScrollView, Alert } from 'react-native';
import DateTimePicker from '@react-native-community/datetimepicker';
import FlashMessage, { showMessage } from 'react-native-flash-message'
import { useInput } from '../../../customHook'
import dateConvert from '../../../helpers/dateConvert'
import { ILCalender, ILStarColor } from '../../../assets/img/icons';
import { Button, Form, Loading } from '../../../components';
import { ILChevrontL, ILTrashW } from '../../../assets';
import instance from '../../../config/axios';
import useForm from '../../../helpers/useForm';
import { useDispatch, useSelector } from 'react-redux';
import { fetchPortofolios } from '../../../store/reducer/portofolioReducer';

const AddPorto = ({ navigation, route }) => {
    const dispatch = useDispatch()

    const { edit, portofolio } = route.params

    const token = useSelector(({ user }) => user.Token)

    const [loading, setLoading] = useState(false)
    const [form, setForm] = useForm(edit ? portofolio : {
        title: '',
        desc: '',
        link: '',
        startDate: '',
        endDate: ''
    })

    const startDate = useInput(portofolio ? new Date(String(portofolio?.startDate).substring(6, 10), +String(portofolio?.startDate).substring(3, 5) - 1, +String(portofolio?.startDate).substring(0, 2) + 1) : new Date())
    const endDate = useInput(portofolio ? new Date(String(portofolio?.endDate).substring(6, 10), +String(portofolio?.endDate).substring(3, 5) - 1, +String(portofolio?.endDate).substring(0, 2) + 1) : new Date())

    const onSubmit = async () => {
        setLoading(true)
        try {
            const project = {
                title: form.title,
                desc: form.desc,
                link: form.link,
                startDate: dateConvert(startDate.date),
                endDate: dateConvert(endDate.date)
            }

            await instance.post('/user/freelance/portofolio', project, {
                headers: {
                    access_token: token
                }
            })
            dispatch(fetchPortofolios(token))
            setLoading(false)
            navigation.goBack()
        } catch (error) {
            showMessage({
                message: 'Error',
                description: error.message,
                type: 'danger'
            })
        }
    }

    const update = async () => {
        const project = {
            title: form.title,
            desc: form.desc,
            link: form.link,
            startDate: dateConvert(startDate.date),
            endDate: dateConvert(endDate.date)
        }
        try {
            await instance.put('/user/freelance/portofolio', project, {
                headers: {
                    access_token: token
                }
            })
        } catch (error) {
            showMessage({
                message: 'Error',
                description: error.message,
                type: 'danger'
            })
        }
    }

    const deletePorto = async () => {
        setLoading(true)
        try {
            await instance.delete(`user/freelance/portofolio/${portofolio._id}`, {
                headers: {
                    access_token: token
                }
            })
            dispatch(fetchPortofolios(token))
            setLoading(false)
            navigation.goBack()
        } catch (error) {
            showMessage({
                type: 'danger',
                message: 'Error',
                description: error.message
            })
            setLoading(false)
            console.log(error.message);
        }
    }

    const confirmAlert = () => {
        Alert.alert(
            "Delete Portofolio",
            "Are you sure ?",
            [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Yes',
                    onPress: deletePorto
                },
            ],
            {
                cancelable: true
            }
        )
    }

    return (
        <>
            <View style={{ flex: 1, backgroundColor: '#ffff' }} >
                <FlashMessage position='top' />
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 20, marginTop: 20 }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 16 }} >{edit ? 'Edit' : 'Add'} Portofolio</Text>
                    </View>
                    <View></View>
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
                >
                    <View style={{ padding: 20 }} >
                        <View style={styles.form} >
                            <ILStarColor />
                            <View style={{ flex: 1, marginLeft: 20 }} >
                                <Form
                                    placeholder='Enter name of project'
                                    onChangeText={val => setForm('title', val)}
                                    value={form.title}
                                />
                            </View>
                        </View>
                        <View style={styles.form} >
                            <ILStarColor />
                            <View style={{ flex: 1, marginLeft: 20 }} >
                                <Form
                                    placeholder='Enter description'
                                    onChangeText={val => setForm('desc', val)}
                                    value={form.desc}
                                />
                            </View>
                        </View>
                        <View style={styles.form} >
                            <ILStarColor />
                            <View style={{ flex: 1, marginLeft: 20 }} >
                                <Form
                                    placeholder='Link project'
                                    onChangeText={val => setForm('link', val)}
                                    value={form.link}
                                />
                            </View>
                        </View>
                        <View style={styles.form} >
                            <ILCalender />
                            <View style={{ flex: 1, marginLeft: 20 }} >
                                <TouchableOpacity
                                    onPress={startDate.showDatePicker}
                                    style={styles.touchable_date}
                                >
                                    <Text>{dateConvert(startDate.date)}</Text>
                                    <Text style={{ marginLeft: 20, color: '#c4c4c4' }} >Start Project</Text>
                                </TouchableOpacity>
                                {startDate.show && (
                                    <DateTimePicker
                                        testID="dateTimePicker1"
                                        value={startDate.date}
                                        mode={startDate.mode}
                                        is24Hour={true}
                                        display='spinner'
                                        onChange={startDate.onChange}
                                    />
                                )}
                            </View>
                        </View>
                        <View style={styles.form} >
                            <ILCalender />
                            <View style={{ flex: 1, marginLeft: 20 }} >
                                <TouchableOpacity
                                    onPress={endDate.showDatePicker}
                                    style={styles.touchable_date}
                                >
                                    <Text>{dateConvert(endDate.date)}</Text>
                                    <Text style={{ marginLeft: 20, color: '#c4c4c4' }} >End Project</Text>
                                </TouchableOpacity>
                                {endDate.show && (
                                    <DateTimePicker
                                        testID="dateTimePicker1"
                                        value={endDate.date}
                                        mode={endDate.mode}
                                        is24Hour={true}
                                        display='spinner'
                                        onChange={endDate.onChange}
                                    />
                                )}
                            </View>
                        </View>
                        <View style={styles.btn(edit)}>
                            {edit && (
                                <TouchableOpacity
                                    onPress={confirmAlert}
                                    style={{ padding: 14, borderRadius: 11, backgroundColor: '#e74c3c' }}
                                >
                                    <ILTrashW />
                                </TouchableOpacity>
                            )}
                            <Button
                                title={edit ? 'Update' : 'Submit'}
                                type='submit-form'
                                onPress={edit ? update : onSubmit}
                            />
                        </View>
                    </View>
                </ScrollView>
            </View>
            {loading && <Loading />}
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        paddingHorizontal: 20
    },
    form: {
        flexDirection: 'row',
        marginTop: 23,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    btn: (edit) => ({
        marginTop: 50,
        alignItems: 'center',
        justifyContent: edit ? 'space-between' : 'center',
        flexDirection: 'row'
    }),
    touchable_date: {
        height: 57,
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#C4C4C4',
        fontSize: 14,
        fontFamily: 'DMSans_400Regular',
        paddingLeft: 20,
        flexDirection: 'row',
        alignItems: 'center'
    }
});

export default AddPorto
