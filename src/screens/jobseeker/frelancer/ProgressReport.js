import React, { useEffect, useState } from 'react';
import { Text, View, TouchableOpacity, Image, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import FlashMessage, { showMessage } from 'react-native-flash-message';
import database from '@react-native-firebase/database';
import { ILChevrontL, ILEllipse2, ILLayers, ILMessageCircle, ILMoreVErtical, ILPaperclip } from '../../../assets';
import instance from '../../../config/axios';
import { fetchTargets } from '../../../store/reducer/projectTargetReducer';
import { Loading } from '../../../components'
import Logo from '../../../assets/company.png'

const Badge = ({ status }) => {
    if (status === 'Approve') {
        return (
            <View
                style={{ padding: 7, backgroundColor: '#088E6B', borderRadius: 5 }}
            >
                <Text style={{ fontFamily: 'DMSans-Regular', color: '#ffff', fontSize: 12 }} >{status}</Text>
            </View>
        )
    }

    if (status === 'On progress' || status === 'Reported') {
        return (
            <View
                style={{ padding: 7, backgroundColor: '#EA2027', borderRadius: 5 }}
            >
                <Text style={{ fontFamily: 'DMSans-Regular', color: '#ffff', fontSize: 12 }} >{status}</Text>
            </View>
        )
    }

    if (status === 'Report' || status === 'Waiting') {
        return (
            <View
                style={{ padding: 7, backgroundColor: '#FF9901', borderRadius: 5 }}
            >
                <Text style={{ fontFamily: 'DMSans-Regular', color: '#ffff', fontSize: 12 }} >{status}</Text>
            </View>
        )
    }

    return null
}

const ProgressReport = ({ navigation, route }) => {
    const { project } = route.params

    const dispatch = useDispatch()

    const user = useSelector(({ user }) => user.User)
    const token = useSelector(({ user }) => user.Token)
    const targets = useSelector(({ targets }) => targets.Targets)

    const [data, setData] = useState({})
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        if (token, project) {
            const parseProject = JSON.parse(project)
            setData(parseProject)
            dispatch(fetchTargets(token, parseProject.jobId))
        }
    }, [token])

    const startProject = async () => {
        setLoading(true)
        try {
            const projectUpdate = {
                updateProject: {
                    title: data.title,
                    jobId: data.jobId,
                    userId: data.userId,
                    status: 'Started',
                    companyId: data.companyId,
                    startedAt: new Date()
                },
                notification: {
                    fcmToken: data.fcmToken,
                    body: `Started ${data.title} project`
                }
            }

            const notification = {
                title: `${user.user_name} has start the project`,
                date: new Date()
            }

            const updateTarget = {
                status: 'On progress',
                updatedAt: new Date()
            }
            await instance.put(`/job/${targets[0]._id}/target`, updateTarget, {
                headers: {
                    access_token: token
                }
            })
            await instance.put(`/project/${data._id}`, projectUpdate, {
                headers: {
                    access_token: token
                }
            })
            await database().ref(`/notifications/${data.userId}`).push(notification)
            setLoading(false)
            navigation.goBack()
        } catch (error) {
            console.log(error);
            showMessage({
                type: 'warning',
                message: 'Error',
                description: 'Opps somthing wrong'
            })
            setLoading(false)
        }
    }

    return (
        <>
            <View style={{ flex: 1, backgroundColor: '#ffff' }} >
                <FlashMessage position='top' />
                <View
                    style={{
                        flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, marginTop: 20
                    }}
                >
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 16 }} >Progress Report</Text>
                    </View>
                    <View></View>
                </View>
                <View style={{ backgroundColor: '#ffff', paddingBottom: 30 }} >
                    <View
                        style={{
                            borderRadius: 10, marginTop: 8, backgroundColor: '#ffff', padding: 20
                        }}
                    >
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }} >
                            <View style={{ justifyContent: 'center' }} >
                                <Image source={data?.photo ? { uri: data?.photo } : Logo} style={{ width: 40, height: 40, borderRadius: 40 / 2 }} />
                            </View>
                            <View style={{ justifyContent: 'center', marginLeft: 23, flex: 1 }}>
                                <Text style={{ fontSize: 14, fontFamily: 'DMSans-Bold', maxWidth: 225 }} >{data.title}</Text>
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', maxWidth: 150 }} >
                                    <Text
                                        style={{
                                            fontSize: 12, color: '#6B6969', fontFamily: 'DMSans-Bold', paddingRight: 5
                                        }}
                                    >
                                        {user.type === 'company' ? data?.freelancer_name : data?.company_name}
                                    </Text>
                                    <ILEllipse2 />
                                    <View style={{ width: 5 }} />
                                    <Text
                                        style={{
                                            fontSize: 12, color: '#6B6969', fontFamily: 'DMSans-Bold', paddingRight: 5
                                        }}
                                    >Freelance</Text>
                                </View>
                            </View>
                            <View style={{ borderWidth: 1, borderColor: '#088E6B', padding: 5, borderRadius: 5 }}>
                                <Text style={{ fontFamily: 'DMSans-Regular', color: '#088E6B', fontSize: 12 }} >{data.status === 'Started' ? 'On progress' : data.status}</Text>
                            </View>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 20, alignItems: 'center' }} >
                        <Text style={{ fontFamily: 'DMSans-Bold' }} >History report</Text>
                        <View style={{ height: 8, borderRadius: 10, width: '70%', backgroundColor: '#FF9901' }} />
                    </View>
                </View>
                <ScrollView
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                    style={{ backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
                >
                    <View style={{ padding: 20 }} >
                        {data?.status === 'Started' && (
                            <View style={{ flexDirection: 'row', marginTop: 20 }} >
                                <View style={{ width: 40 }} >
                                    <Text style={{ fontFamily: 'DMSans-Bold', fontSize: 16 }} >{new Date(data.startedAt).toDateString().substring(8, 11)}</Text>
                                    <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 15 }} >{new Date(data.startedAt).toDateString().substring(4, 8)}</Text>
                                    <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 12 }} >{new Date(data.startedAt).toDateString().substring(11, 16)}</Text>
                                </View>
                                <View style={{ marginLeft: 9, alignItems: 'center' }} >
                                    <View style={{ width: 15, height: 15, borderWidth: 3, borderColor: '#088E6B', borderRadius: 15 / 2 }} />
                                    <View style={{ width: 4, flex: 1, backgroundColor: '#088E6B' }} />
                                </View>
                                <View style={{ flex: 1, paddingBottom: 20 }} >
                                    <View style={{ marginLeft: 13, backgroundColor: '#ffff', borderRadius: 10, padding: 20 }} >
                                        <Text style={{ color: 'black' }} >Project Start</Text>
                                    </View>
                                </View>
                            </View>
                        )}

                        {/* Progress */}
                        {targets && targets.map((el, i) => (
                            <View key={el._id} >
                                <View style={{ flexDirection: 'row' }} >
                                    <View style={{ width: 40 }} >
                                        <Text style={{ fontFamily: 'DMSans-Bold', fontSize: 16 }} >{new Date(el.date.date).toDateString().substring(8, 11)}</Text>
                                        <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 15 }} >{new Date(el.date.date).toDateString().substring(4, 8)}</Text>
                                        <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 12 }} >{new Date(el.date.date).toDateString().substring(11, 16)}</Text>
                                    </View>
                                    <View style={{ marginLeft: 9, alignItems: 'center' }} >
                                        <View style={{ width: 15, height: 15, borderWidth: 3, borderColor: el.status === 'Approve' ? '#088E6B' : '#FF9901', borderRadius: 15 / 2 }} />
                                        <View style={{ width: el.status === 'Approve' ? 4 : 2, flex: 1, backgroundColor: el.status === 'Approve' ? '#088E6B' : '#c4c4c4' }} />
                                    </View>
                                    <View style={{ flex: 1, paddingBottom: 20 }} >
                                        {data.status !== 'Started' || el.status === 'Waiting' ? (
                                            <View
                                                onPress={() => navigation.navigate('DetailReport', { status: el.status.toLowerCase(), target: el, photo: data.photo, num: i })}
                                                style={{
                                                    marginLeft: 13, backgroundColor: '#eeee', borderRadius: 10, padding: 20
                                                }}
                                            >
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                                                    <View>
                                                        <Text style={{ fontFamily: 'DMSans-Bold' }} >Staging {i + 1}</Text>
                                                        <Text style={{ fontFamily: 'DMSans-Regular', maxWidth: 200, marginTop: 9 }} >{el.desc}</Text>
                                                    </View>
                                                    <View style={{ marginTop: -10 }} >
                                                        <ILMoreVErtical />
                                                    </View>
                                                </View>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 20 }} >
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', }} >
                                                        <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                                            <ILPaperclip />
                                                            <Text style={{ fontFamily: 'DMSans-Regular', marginLeft: 8, color: '#8F8E94' }} >0</Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 25 }} >
                                                            <ILMessageCircle />
                                                            <Text style={{ fontFamily: 'DMSans-Regular', marginLeft: 8, color: '#8F8E94' }} >0</Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 25 }} >
                                                            <ILLayers />
                                                            <Text style={{ fontFamily: 'DMSans-Regular', marginLeft: 8, color: '#8F8E94' }} >0</Text>
                                                        </View>
                                                    </View>
                                                    <View>
                                                        <Badge
                                                            status={el.status}
                                                        />
                                                    </View>
                                                </View>
                                            </View>
                                        ) : (
                                            <TouchableOpacity
                                                onPress={() => navigation.navigate('DetailReport', { status: el.status.toLowerCase(), target: el, photo: data.photo, num: i, companyId: data.companyId, userId: data.userId, targets: targets, project: data, fcmToken: data.fcmToken })}
                                                style={{
                                                    marginLeft: 13, backgroundColor: '#ffff', borderRadius: 10, padding: 20
                                                }}
                                            >
                                                <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                                                    <View>
                                                        <Text style={{ fontFamily: 'DMSans-Bold' }} >Staging {i + 1}</Text>
                                                        <Text style={{ fontFamily: 'DMSans-Regular', maxWidth: 200, marginTop: 9 }} >{el.desc}</Text>
                                                    </View>
                                                    <View style={{ marginTop: -10 }} >
                                                        <ILMoreVErtical />
                                                    </View>
                                                </View>
                                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 20 }} >
                                                    <View style={{ flexDirection: 'row', alignItems: 'center', }} >
                                                        <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                                            <ILPaperclip />
                                                            <Text style={{ fontFamily: 'DMSans-Regular', marginLeft: 8, color: '#8F8E94' }} >0</Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 25 }} >
                                                            <ILMessageCircle />
                                                            <Text style={{ fontFamily: 'DMSans-Regular', marginLeft: 8, color: '#8F8E94' }} >{el?.comments?.length}</Text>
                                                        </View>
                                                        <View style={{ flexDirection: 'row', alignItems: 'center', marginLeft: 25 }} >
                                                            <ILLayers />
                                                            <Text style={{ fontFamily: 'DMSans-Regular', marginLeft: 8, color: '#8F8E94' }} >0</Text>
                                                        </View>
                                                    </View>
                                                    <View>
                                                        <Badge
                                                            status={el.status}
                                                        />
                                                    </View>
                                                </View>
                                            </TouchableOpacity>

                                        )}
                                    </View>
                                </View>
                            </View>
                        ))}
                        {/* End progress */}

                        {/* Project Done */}
                        {data.status === 'Finished' && (
                            <View style={{ flexDirection: 'row' }} >
                                <View style={{ width: 40, paddingBottom: 20 }} >
                                    <Text style={{ fontFamily: 'DMSans-Bold', fontSize: 16 }} >{new Date(data.finishAt).toDateString().substring(8, 11)}</Text>
                                    <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 15 }} >{new Date(data.finishAt).toDateString().substring(4, 8)}</Text>
                                    <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 12 }} >{new Date(data.finishAt).toDateString().substring(11, 16)}</Text>
                                </View>
                                <View style={{ marginLeft: 9, alignItems: 'center' }} >
                                    <View style={{ width: 15, height: 15, borderWidth: 3, borderColor: '#088E6B', borderRadius: 15 / 2 }} />
                                </View>
                                <View style={{ flex: 1, }} >
                                    <View style={{ marginLeft: 13, backgroundColor: '#ffff', borderRadius: 10, padding: 20 }} >
                                        <Text>Project Done</Text>
                                    </View>
                                </View>
                            </View>
                        )}

                    </View>
                </ScrollView>
                {user.type === 'company' && data.status !== 'Finished' && (
                    <View style={{ padding: 20, justifyContent: 'space-between', flexDirection: 'row' }} >
                        {data.status !== 'Started' && targets?.length > 0 && (
                            <TouchableOpacity
                                onPress={startProject}
                                style={{ flex: 1, padding: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#0093BA', borderRadius: 10 }}
                            >
                                <Text style={{ color: '#ffff' }} >Start Project</Text>
                            </TouchableOpacity>

                        )}
                        <View style={{ width: 20 }} />
                        <TouchableOpacity
                            onPress={() => navigation.navigate('ProjectTarget', { jobId: data.jobId })}
                            style={{ flex: 1, padding: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ff9901', borderRadius: 10 }}
                        >
                            <Text style={{ color: '#ffff' }} >Add Target</Text>
                        </TouchableOpacity>
                    </View>
                )}
            </View>
            {loading && <Loading />}
        </>
    )
};

export default ProgressReport;
