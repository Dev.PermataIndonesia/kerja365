import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, SafeAreaView, ScrollView, TouchableOpacity, Dimensions, Image } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ILChevrontL, ILMoreVErtical, ILSkills } from '../../../assets';
import { TabView, TabBar } from 'react-native-tab-view';
import { fetchJob } from '../../../store/reducer/jobsReducer';
import Logo from '../../../assets/company.png'

const Button = ({ title }) => {
    return (
        <TouchableOpacity style={{ paddingVertical: 5, paddingHorizontal: 7, borderWidth: 1, borderColor: '#088E6B', borderRadius: 5 }} >
            <Text style={{ fontFamily: 'DMSans-Regular', color: '#088E6B', fontSize: 12 }} >{title}</Text>
        </TouchableOpacity>
    )
}

const ProjectDesc = ({ navigation, job }) => {
    return (
        <View style={{ padding: 10 }} >
            <View style={{ padding: 10, backgroundColor: 'rgba(238,238,238, 0.3)', paddingBottom: 79 }} >
                <Text style={styles.h3} >Project Description</Text>
                <Text style={{ marginTop: 13, fontFamily: 'DMSans-Regular', fontSize: 13, textAlign: 'justify' }} >
                    {job?.jobResponsibility}
                </Text>
                <Text style={styles.h3} >Skills Requirements</Text>
                <View style={{ flexDirection: 'row', flexWrap: 'wrap', marginTop: 10, justifyContent: 'flex-start' }} >
                    {job.jobRequirments?.map((item, i) => {
                        return (
                            <View key={i} style={{ padding: 7, marginTop: 10, borderRadius: 5, borderColor: '#FF9901', borderWidth: 1, marginRight: 5 }} >
                                <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 12, color: '#FF9901' }} >{item}</Text>
                            </View>
                        )
                    })}
                </View>
            </View>
        </View>
    )
}

const Company = () => {
    return (
        <View style={{ padding: 20 }} >
            <Text>Company</Text>
        </View>
    )
}

const initialLayout = { width: Dimensions.get('window').width };

const ProjectDetail = ({ navigation, route }) => {
    const { project } = route.params

    const dispatch = useDispatch()

    const [index, setIndex] = useState(0)
    const [routes] = useState([
        { key: 'first', title: 'Descriptions' },
        { key: 'second', title: 'Company' },
    ]);

    const job = useSelector(({ jobs }) => jobs.Job)
    const user = useSelector(({ user }) => user.User)
    const token = useSelector(({ user }) => user.Token)

    useEffect(() => {
        if (token) {
            dispatch(fetchJob(project.jobId, token))
        }
    }, [token])

    const renderScene = ({ route }) => {
        switch (route.key) {
            case 'first':
                return <ProjectDesc navigation={navigation} job={job} />
            case 'second':
                return <Company />
            default:
                return null
        }
    }

    return (
        <>
            <SafeAreaView style={{ flex: 1, backgroundColor: '#ffff' }}>
                <View
                    style={{
                        flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 60, paddingHorizontal: 20, marginTop: 20
                    }}
                >
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 16 }} >Project Detail</Text>
                    </View>
                    <View></View>
                </View>
                <ScrollView
                    showsHorizontalScrollIndicator={false} showsVerticalScrollIndicato={false}
                    style={{ flex: 1 }}
                >
                    <View style={{ alignItems: 'center', backgroundColor: '#ffff', paddingVertical: 10 }}>
                        <Image source={job?.company?.company_logo ? { uri: job?.company?.company_logo } : Logo} style={{ width: 70, height: 70, borderRadius: 70 / 2 }} />
                        <Text style={styles.h3} >{job?.title}</Text>
                        <Text style={{ marginTop: 10, fontFamily: 'DMSans-Regular', fontSize: 12 }}>Rp. {job?.salary}</Text>
                        <View style={styles.space} />
                        <Button title='on progress' />
                    </View>

                    <TabView
                        navigationState={{ index, routes }}
                        renderScene={renderScene}
                        onIndexChange={setIndex}
                        initialLayout={initialLayout}
                        renderTabBar={props => <TabBar {...props}
                            style={{ flex: 1, backgroundColor: '#ffff', elevation: 0, paddingBottom: 31 }}
                            inactiveColor='black'
                            activeColor='black'
                            renderLabel={({ route }) => (
                                <Text style={{ color: 'black', paddingHorizontal: 10, fontFamily: 'DMSans-Bold' }} >{route.title}</Text>
                            )}
                            labelStyle={{ fontSize: 14, fontWeight: '700' }}
                            indicatorStyle={{ backgroundColor: '#FF9901', width: 70, height: 8, borderRadius: 10, marginLeft: '12%', marginBottom: 20 }}
                        />}
                    />

                </ScrollView>
                <View style={{ justifyContent: 'center', alignItems: 'center', paddingVertical: 20 }} >
                    <TouchableOpacity
                        onPress={() => navigation.navigate('ProgressReport', { project: JSON.stringify(project) })}
                        style={{
                            width: 355, height: 52, backgroundColor: '#FF9901', justifyContent: 'center', alignItems: 'center', borderRadius: 5
                        }}
                    >
                        <Text style={{ color: '#ffff', fontFamily: 'DMSans-Bold' }} >{user.type === 'company' ? 'See report' : 'Submit report'}</Text>
                    </TouchableOpacity>
                </View>
            </SafeAreaView>
        </>
    )
};

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        paddingHorizontal: 20,
        paddingTop: 70
    },
    image: {
        width: '100%',
        height: 200,
    },
    title: {
        fontSize: 24,
    },
    h3: {
        fontSize: 14,
        marginTop: 16,
        fontFamily: 'DMSans-Bold'
    },
    paragraph: {
        textAlign: 'justify'
    },
    space: {
        height: 20,
        width: 20
    },
    scene: {
        flex: 1
    }
})

export default ProjectDetail;
