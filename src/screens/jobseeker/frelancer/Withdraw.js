import React, { useState, useRef, useEffect } from 'react';
import { ActivityIndicator, Animated, ScrollView, Text, View, SafeAreaView, TextInput, TouchableOpacity, Dimensions } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ILChevrontL, ILFileText, ILPlus, ILStarWhite } from '../../../assets';
import instance from '../../../config/axios';
import { fetchDefaultBankAccount } from '../../../store/reducer/bankAccountReducer';
import { Loading } from '../../../components'

const Withdraw = ({ navigation }) => {
    const dispatch = useDispatch()

    const token = useSelector(({ user }) => user.Token)
    const bankAccount = useSelector(({ bankAccounts }) => bankAccounts.DefaultBankAccount)

    const [validate, setValidate] = useState(false)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        if (token) {
            dispatch(fetchDefaultBankAccount(token))
        }
    }, [token])

    const onPress = () => {
        setLoading(true)
        if (validate) {
            setLoading(false)
            alert('Success Withdraw')
        } else {
            setTimeout(() => {
                setLoading(false)
                setValidate(true)
            }, 4000)
        }
    }

    return (
        <>
            <SafeAreaView style={{ flex: 1, zIndex: 1, backgroundColor: '#ffff' }} >
                <View style={{ backgroundColor: '#ffff', marginTop: 20, paddingHorizontal: 20, paddingBottom: 60 }} >
                    <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                        <TouchableOpacity onPress={() => navigation.goBack()} >
                            <ILChevrontL />
                        </TouchableOpacity>
                        <View>
                            <Text>Withdraw</Text>
                        </View>
                        <View />
                    </View>
                </View>
                <ScrollView style={{ flex: 1, backgroundColor: 'rgba(238, 238, 238, 0.3)' }} >
                    <View style={{ justifyContent: 'space-between', paddingHorizontal: 20, marginTop: 38 }} >
                        <View>
                            <View style={{ backgroundColor: '#ffff', height: 256, padding: 20, borderRadius: 10 }} >
                                <Text>Bank account</Text>
                                {Object.keys(bankAccount).length > 0 && (
                                    <View
                                        style={{
                                            marginTop: 13, backgroundColor: '#FF9901', height: 168, borderRadius: 10, paddingHorizontal: 10,
                                        }}
                                    >
                                        <View style={{ padding: 20 }} >
                                            <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                                                <Text style={{ fontFamily: 'DMSans-Bold', color: 'white' }} >{bankAccount?.bankName}</Text>
                                                <ILStarWhite />
                                            </View>
                                            <View style={{ marginTop: 30 }} >
                                                <Text style={{ fontFamily: 'DMSans-Bold', color: '#ffff', fontSize: 20 }} >{bankAccount?.accountNumber}</Text>
                                            </View>
                                            <View style={{ marginTop: 30, alignSelf: 'flex-end' }} >
                                                <Text style={{ fontFamily: 'DMSans-Bold', color: '#ffff' }} >{bankAccount?.accountHolder}</Text>
                                            </View>
                                        </View>
                                    </View>

                                )}

                            </View>
                            <View>
                                <TouchableOpacity
                                    onPress={() => navigation.navigate('BankAccount')}
                                    style={{ width: 50, height: 50, backgroundColor: '#FF9901', borderRadius: 50 / 2, position: 'absolute', top: -25, right: 20, justifyContent: 'center', alignItems: 'center' }} >
                                    <ILPlus />
                                </TouchableOpacity>
                            </View>
                            <View>
                                <View style={{ marginTop: 60, backgroundColor: '#ffff', borderRadius: 10 }} >
                                    <View style={{ padding: 20 }} >
                                        <View style={{ flexDirection: 'row', alignItems: 'center' }} >
                                            <ILFileText />
                                            <Text style={{ fontFamily: 'DMSans-Regular', marginLeft: 10 }} >Withdraw amount</Text>
                                        </View>
                                        <View style={{ marginTop: 18 }} >
                                            <TextInput
                                                placeholder='Amount'
                                                placeholderTextColor='#c4c4c4'
                                                keyboardType='number-pad'
                                                style={{ width: 300, borderBottomRightRadius: 10, borderTopRightRadius: 10, color: 'black', paddingHorizontal: 20, alignSelf: 'flex-end' }}
                                                textAlign='right'
                                            />
                                        </View>
                                    </View>
                                </View>
                            </View>

                            {loading && (
                                <ActivityIndicator
                                    color='#FF9901'
                                    size='large'
                                    style={{ marginTop: 20 }}
                                />
                            )}

                            {validate && (
                                <View style={{ backgroundColor: '#ffff', padding: 20, marginTop: 10 }} >
                                    <View>
                                        <Text style={{ fontFamily: 'DMSans-Bold' }} >Account Information</Text>
                                        <View style={{ height: 1, backgroundColor: '#eeee', marginTop: 13 }} />
                                    </View>
                                    <View style={{ marginTop: 16 }} >
                                        <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 12, color: '#6B6969' }} >Account number</Text>
                                        <Text style={{ fontFamily: 'DMSans-Bold', color: '#FF9901', fontSize: 16, marginTop: 10 }} >8085 1236 9125 1402</Text>
                                    </View>
                                    <View style={{ marginTop: 16 }} >
                                        <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 12, color: '#6B6969' }} >Account holder</Text>
                                        <Text style={{ fontFamily: 'DMSans-Bold', color: '#FF9901', fontSize: 16, marginTop: 10 }} >Sdri Poetri Lazuardi</Text>
                                    </View>
                                    <View style={{ marginTop: 16 }} >
                                        <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 12, color: '#6B6969' }} >Bank name</Text>
                                        <Text style={{ fontFamily: 'DMSans-Bold', color: '#FF9901', fontSize: 16, marginTop: 10 }} >BNI</Text>
                                    </View>
                                </View>
                            )}
                        </View>
                        <View style={{ marginTop: 69, alignItems: 'center', paddingBottom: 50 }} >
                            <TouchableOpacity
                                onPress={onPress}
                                style={{ backgroundColor: '#FF9901', justifyContent: 'center', alignItems: 'center', width: 200, height: 50, borderRadius: 25 }}
                            >
                                <Text style={{ fontFamily: 'DMSans-Bold', color: '#ffff' }} >{!validate ? 'Validate' : 'Withdraw'}</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
            {loading && <Loading />}
        </>
    )
};

export default Withdraw;
