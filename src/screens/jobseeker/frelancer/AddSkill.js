import React, { useEffect, useState } from 'react';
import { Text, View, StyleSheet, TouchableOpacity, ScrollView, Alert } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ILChevrontL, ILPlus, ILTrashW, ILXsvg } from '../../../assets';
import { ILStarColor } from '../../../assets/img/icons';
import { Button, Form, Loading } from '../../../components';
import instance from '../../../config/axios';
import { fetchSkills } from '../../../store/reducer/skillsReducer';
import FlashMessage, { showMessage } from 'react-native-flash-message'

const AddSkill = ({ navigation, route }) => {
    const dispatch = useDispatch()

    const { edit, skillsParams } = route.params

    const token = useSelector(({ user }) => user.Token)
    const [skills, setSkills] = useState([])
    const [skill, setSkill] = useState('')
    const [visible, setVisible] = useState(false)
    const [loading, setLoading] = useState(false)
    const [message, setMessage] = useState('')

    useEffect(() => {
        if (edit) {
            setSkills(skillsParams)
        }
    }, [edit])

    const onChangeText = (value) => {
        setSkill(value)
    }

    const deleteSkill = (i) => {
        const deleteSkill = skills.splice(i, 1)
        const newSkill = skills.filter(el => el !== deleteSkill)
        setSkills(newSkill)
    }


    const onSubmit = async () => {
        setLoading(true)
        const insertSkills = skills.map(el => {
            return { title: el }
        })
        try {
            await instance.post('/skill', insertSkills, {
                headers: {
                    access_token: token
                }
            })
            setVisible(true)
            setLoading(false)
            dispatch(fetchSkills(token))
            navigation.navigate('MainApp', { screen: 'Profile' })
        } catch (error) {
            console.log(error.message);
            setMessage('Oops!, Something error')
            setVisible(true)
            setLoading(false)
        }
    }

    const deleteSkillFromDatabase = async (id) => {
        try {
            await instance.delete(`/skill/${id}`, {
                headers: {
                    access_token: token
                }
            })
            const newSkills = skills.filter(el => el._id !== id)
            setSkills(newSkills)
            showMessage({
                type: 'success',
                message: 'Success',
                description: 'Delete skill'
            })
        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops',
                description: 'Someting wrong'
            })
        }
    }

    const confirmAlert = (id) => {
        Alert.alert(
            "Delete Skill",
            `Are you sure ?`,
            [
                {
                    text: 'Cancel'
                },
                {
                    text: 'Yes',
                    onPress: () => deleteSkillFromDatabase(id)
                },
            ],
            {
                cancelable: true
            }
        )
    }

    const done = () => {
        dispatch(fetchSkills(token))
        navigation.goBack()
    }

    const addSkill = () => {
        const newSKill = skills.concat(skill)
        setSkills(newSKill)
        setSkill('')
    }

    return (
        <>
            <View style={styles.container} >
                {visible && <Text>{message}</Text>}
                <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 16 }} >Add skill</Text>
                    </View>
                    <View></View>
                </View>
                {!edit && (
                    <>
                        <View style={styles.form} >
                            <ILStarColor />
                            <View style={{ flex: 1, marginLeft: 20 }} >
                                <Form
                                    placeholder='Enter for add skill'
                                    onChangeText={onChangeText}
                                    value={skill}
                                />
                            </View>
                            <View
                                style={{ width: 50, height: 57, justifyContent: 'center', alignItems: 'center' }}
                            >
                                {skill.length > 1 && (
                                    <TouchableOpacity
                                        onPress={addSkill}
                                        style={{ backgroundColor: '#2ecc71', padding: 12, borderRadius: 3 }}
                                    >
                                        <ILPlus />
                                    </TouchableOpacity>
                                )}
                            </View>
                        </View>

                        <View style={{ flexDirection: 'row', flexWrap: 'wrap', justifyContent: 'center', marginTop: 30 }} >
                            {skills && skills.map((item, i) => (
                                <View key={i} style={{ flexDirection: 'row', alignItems: 'center', marginTop: 10, borderColor: '#eeee', borderWidth: 1, marginHorizontal: 5, borderRadius: 5, padding: 5 }} >
                                    <TouchableOpacity onPress={() => deleteSkill(i)} >
                                        <ILXsvg />
                                    </TouchableOpacity>
                                    <Text>{item}</Text>
                                </View>
                            ))}
                        </View>

                        {skills?.length > 0 && (
                            <View style={styles.btn} >
                                <Button
                                    title='Submit'
                                    type='submit-form'
                                    onPress={onSubmit}
                                />
                            </View>
                        )}
                    </>
                )}
                {edit && (
                    <View style={{ flex: 1, marginTop: 20, justifyContent: 'space-between' }} >
                        <ScrollView style={{ marginTop: 20, paddingHorizontal: 20 }} >
                            {skills.map(el => (
                                <View key={el._id} style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20 }} >
                                    <Text>{el.title}</Text>
                                    <TouchableOpacity
                                        onPress={() => confirmAlert(el._id)}
                                        style={{ padding: 5, backgroundColor: '#e74c3c' }}
                                    >
                                        <ILTrashW />
                                    </TouchableOpacity>
                                </View>
                            ))}
                        </ScrollView>
                        <View style={{ padding: 20 }} >
                            <TouchableOpacity
                                onPress={done}
                                style={{ padding: 20, justifyContent: 'center', alignItems: 'center', backgroundColor: '#ff9901' }}
                            >
                                <Text style={{ color: '#ffff' }} >Done</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                )}
            </View>
            {loading && <Loading />}
        </>
    )
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#ffffff',
        paddingHorizontal: 20
    },
    form: {
        flexDirection: 'row',
        marginTop: 23,
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    btn: {
        marginTop: 50,
        alignItems: 'center'
    }
});

export default AddSkill;
