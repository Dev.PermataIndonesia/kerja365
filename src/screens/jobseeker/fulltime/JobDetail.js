import React, { useEffect, useState } from 'react';
import { View, Text, StyleSheet, ScrollView, TouchableOpacity, ActivityIndicator, Dimensions, Image } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { showMessage } from 'react-native-flash-message';
import { TabView, TabBar } from 'react-native-tab-view';
import { ILChevrontL, ILBookmark2, ILBookmarkWhite } from '../../../assets';
import { JobDescriptions, Loading } from '../../../components';
import { fetchApplication } from '../../../store/reducer/jobAppliedReducer';
import instance from '../../../config/axios';
import { fetchBookmark, fetchBookmarks } from '../../../store/reducer/jobBookmarkReducer';
import Logo from '../../../assets/company.png'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { fetchJob } from '../../../store/reducer/jobsReducer';

const Company = ({ job }) => (
    <View style={{ padding: 20 }} >
        <View>
            <Text style={{ marginTop: 10, textAlign: 'justify' }} >{job?.company?.desc}</Text>
            {job?.company?.company_address && (
                <View style={{ marginTop: 10, flexDirection: 'row', alignItems: 'center' }} >
                    <Text style={{ flex: 1, marginTop: 10, color: 'black', textAlign: 'justify' }} >{job?.company?.company_address}</Text>
                </View>
            )}
        </View>

    </View>
)

const Button = ({ title }) => {
    return (
        <TouchableOpacity style={{ paddingVertical: 5, paddingHorizontal: 7, borderWidth: 1, borderColor: '#C4C4C4', borderRadius: 5 }} >
            <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 12 }} >{title}</Text>
        </TouchableOpacity>
    )
}

const initialLayout = { width: Dimensions.get('window').width }

const JobDetail = ({ navigation, route }) => {
    const dispatch = useDispatch()
    let { jobId } = route.params
    const [index, setIndex] = useState(0)
    const [routes, setRoutes] = useState([])
    const [isModalVisible, setModalVisible] = useState(false)
    const [loading, setLoading] = useState(false)

    const user = useSelector(({ user }) => user.User)
    const token = useSelector(({ user }) => user.Token)
    const application = useSelector(({ applications }) => applications.Application)
    const loadingApplication = useSelector(({ applications }) => applications.Loading)
    const bookmark = useSelector(({ bookmarks }) => bookmarks.Bookmark)
    const loadingBookmark = useSelector(({ bookmarks }) => bookmarks.Loading)
    const job = useSelector(({ jobs }) => jobs.Job)

    useEffect(() => {
        if (token) {
            dispatch(fetchJob(jobId, token))
            dispatch(fetchApplication(token, jobId))
            dispatch(fetchBookmark(token, jobId))
        }

        if (user.type === 'fulltimer') {
            setRoutes([
                { key: 'first', title: 'Descriptions' },
                { key: 'third', title: 'Company' }
            ])
        }
    }, [dispatch, token])

    const RenderScene = () => {
        return (
            <>
                {index === 0 && <JobDescriptions
                    job={job}
                    user={user}
                    navigation={navigation}
                />}
                {index === 1 && <Company job={job} />}
            </>
        )
    }

    const renderTabBar = props => {
        return (
            <View style={{ paddingHorizontal: 20, flex: 1, backgroundColor: '#ffff' }} >
                <TabBar {...props}
                    style={{
                        elevation: 0, paddingBottom: 31, backgroundColor: '#ffff'
                    }}
                    inactiveColor='black'
                    activeColor='black'
                    renderLabel={({ route }) => (
                        <Text style={{ color: 'black', fontFamily: 'DMSans-Bold' }} >{route.title}</Text>
                    )}
                    labelStyle={{
                        fontSize: 14, fontWeight: '700'
                    }}
                    indicatorStyle={{
                        backgroundColor: '#FF9901', height: 8, borderRadius: 10, marginBottom: 20
                    }}
                />
            </View>
        )
    }

    const bookmarkJob = async () => {
        setLoading(true)
        try {
            const data = {
                jobId: job._id,
                companyId: job.company._id,
                job: job
            }
            await instance.post('/bookmark', data, {
                headers: {
                    access_token: token
                }
            })
            dispatch(fetchBookmark(token, job._id))
            dispatch(fetchBookmarks(token))
            setLoading(false)
        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops!! something wrong'
            })
            setLoading(false)
        }
    }

    const deleteBookmark = async () => {
        setLoading(true)
        try {
            if (!bookmark?._id) throw { type: 'error' }
            await instance.delete(`/bookmark/${bookmark._id}`, {
                headers: {
                    access_token: token
                }
            })
            dispatch(fetchBookmark(token, job._id))
            dispatch(fetchBookmarks(token))
        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops!! something wrong'
            })
        }
        setLoading(false)
    }

    if (isModalVisible === true) {
        setTimeout(() => {
            setModalVisible(false)
        }, 3000)
    }


    return (
        <>
            {Object.keys(job).length < 1 ? (
                <SkeletonPlaceholder>
                    <View style={{ justifyContent: 'center', alignItems: 'center', marginTop: 40 }} >
                        <View style={{ width: 100, height: 100, borderRadius: 100 / 2 }} />
                        <View style={{ width: 100, height: 20, borderRadius: 11, marginTop: 16 }} />
                        <View style={{ width: 120, height: 20, borderRadius: 11, marginTop: 16 }} />
                        <View style={{ width: 70, height: 30, borderRadius: 11, marginTop: 16 }} />
                        <View style={{ height: 30, width: Dimensions.get('screen').width - 40, marginTop: 50, borderRadius: 11 }} />
                    </View>
                    <View style={{ paddingHorizontal: 20 }} >
                        <View style={{ width: 120, height: 20, borderRadius: 11, marginTop: 30 }} />
                        <View style={{ width: Dimensions.get('screen').width - 40, height: 200, borderRadius: 11, marginTop: 20 }} />
                        <View style={{ width: 150, height: 20, borderRadius: 11, marginTop: 50 }} />
                        <View style={{ width: Dimensions.get('screen').width - 40, height: 150, borderRadius: 11, marginTop: 20 }} />
                    </View>
                </SkeletonPlaceholder>
            ) : (
                <View style={{ flex: 1, backgroundColor: '#ffff' }}>
                    <View style={[styles.container, { flexDirection: 'row', justifyContent: 'space-between', paddingBottom: 10, marginTop: 20 }]}>
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                            style={{ width: 50 }}
                        >
                            <ILChevrontL />
                        </TouchableOpacity>
                        <View>
                            <Text style={{ fontSize: 16 }} >Job Detail</Text>
                        </View>
                        <View style={{ width: 50 }} />
                    </View>
                    <ScrollView
                        showsVerticalScrollIndicator={false}
                        style={{
                            flex: 1, backgroundColor: 'rgba(238,238,238, 0.3)'
                        }}
                    >
                        <View style={{ alignItems: 'center', backgroundColor: '#ffff', paddingVertical: 20 }}>
                            <Image source={job?.company?.company_logo ? { uri: job?.company?.company_logo } : Logo} style={{ width: 100, height: 100, borderRadius: 100 / 2, borderWidth: 1, borderColor: '#eeee' }} />
                            <Text style={styles.h3} >{job?.title}</Text>
                            <Text style={{ marginTop: 10, fontFamily: 'DMSans-Regular', fontSize: 12 }}>Rp. {job?.salary}</Text>
                            <View style={styles.space} />
                            <Button title={job?.type} />
                        </View>
                        <TabView
                            navigationState={{ index, routes }}
                            renderScene={() => null}
                            onIndexChange={setIndex}
                            initialLayout={initialLayout}
                            renderTabBar={renderTabBar}
                        />
                        <RenderScene />
                    </ScrollView>
                    <View style={{ flexDirection: 'row', padding: 20, justifyContent: 'space-between' }}>
                        {bookmark || loadingBookmark ? (
                            <TouchableOpacity
                                onPress={deleteBookmark}
                                style={{ width: 58, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgba(46, 204, 113,1)', borderRadius: 11, height: 52 }} >
                                <ILBookmarkWhite />
                            </TouchableOpacity>
                        ) : (
                            <TouchableOpacity
                                onPress={bookmarkJob}
                                style={{ width: 58, justifyContent: 'center', alignItems: 'center', borderWidth: 3, borderColor: '#eeee', borderRadius: 11, height: 52 }} >
                                <ILBookmark2 />
                            </TouchableOpacity>
                        )}
                        <View style={{ width: 8 }} />
                        <View style={{ flex: 1 }}>
                            {loadingApplication ? (
                                <SkeletonPlaceholder>
                                    <View style={{ height: 52, borderRadius: 11 }} />
                                </SkeletonPlaceholder>
                            ) : (
                                <>
                                    {!application ?
                                        (
                                            <View>
                                                <TouchableOpacity
                                                    onPress={() => navigation.navigate('ConfirmApplications', {
                                                        job: JSON.stringify(job)
                                                    })}
                                                    style={{ backgroundColor: '#ff9901', height: 52, borderRadius: 11, alignItems: 'center', justifyContent: 'center' }}
                                                >
                                                    <Text style={{ fontFamily: 'DMSans-Bold', color: '#ffff', textAlign: 'center' }} >Apply</Text>
                                                </TouchableOpacity>
                                            </View>
                                        ) : (
                                            <View style={{ justifyContent: 'center', alignItems: 'center', height: 52, backgroundColor: 'rgba(46, 204, 113,1.0)', borderRadius: 11 }}>
                                                <Text style={{ fontFamily: 'DMSans-Bold', color: '#ffff', fontSize: 20 }} >Applied</Text>
                                            </View>
                                        )
                                    }
                                </>
                            )}
                        </View>
                    </View>
                </View>
            )}
            {loading && <Loading />}
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        justifyContent: 'center',
        paddingHorizontal: 20,
    },
    image: {
        width: '100%',
        height: 200,
    },
    title: {
        fontSize: 24,
    },
    h3: {
        fontSize: 14,
        marginTop: 16,
        fontFamily: 'DMSans-Bold'
    },
    paragraph: {
        textAlign: 'justify'
    },
    space: {
        height: 20,
        width: 20
    },
    scene: {
        flex: 1
    }
})

export default JobDetail;
