import React, { useEffect, useState } from 'react';
import { ActivityIndicator } from 'react-native';
import { SafeAreaView, Text, View, TouchableOpacity } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { ILChevrontL, ILMoreVErtical, ILSliders } from '../../../assets';
import { JobCard } from '../../../components';
import { fetchFilterJobs } from '../../../store/reducer/jobsReducer';

const FilterJobs = ({ navigation, route }) => {
    let { category, area } = route.params
    const dispatch = useDispatch()

    const [newCategory, setCategory] = useState(category)
    const [newArea, setArea] = useState(area)

    const token = useSelector(({ user }) => user.Token)
    const jobs = useSelector(({ jobs }) => jobs.FilterJobs)
    const loading = useSelector(({ jobs }) => jobs.JobsLoading)

    useEffect(() => {
        setCategory(category)
        setArea(area)
    }, [token])

    const onPress = () => {
        dispatch(fetchFilterJobs(token, jobs.length, newCategory, newArea, false))
    }

    return (
        <>
            <SafeAreaView
                style={{ backgroundColor: '#ffff', flex: 1 }}
            >
                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', marginTop: 20, paddingHorizontal: 20 }}>
                    <TouchableOpacity
                        onPress={() => navigation.goBack()}
                        style={{ width: 50 }}
                    >
                        <ILChevrontL />
                    </TouchableOpacity>
                    <View>
                        <Text style={{ fontSize: 16 }} >Filter Jobs</Text>
                    </View>
                    <View style={{ width: 50 }} >
                        <TouchableOpacity
                            onPress={() => navigation.navigate('FilterPage')}
                            style={{ height: 52, width: 52, backgroundColor: '#FF9901', alignItems: 'center', justifyContent: 'center', borderRadius: 5 }}
                        >
                            <ILSliders />
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, marginTop: 20, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
                >
                    <View
                        style={{ padding: 20 }}
                    >

                        {!jobs || jobs?.length < 1 && (
                            <View style={{ justifyContent: 'center', alignItems: 'center' }} >
                                <Text>Not found</Text>
                            </View>
                        )}
                        {jobs && jobs.map(job => (
                            <JobCard
                                key={job._id}
                                navigation={navigation}
                                ILMoreVErtical={ILMoreVErtical}
                                job={job}
                            />
                        ))}

                        {loading && (
                            <ActivityIndicator color='#ff9901' size='small' style={{ marginTop: 20 }} />
                        )}

                        <View style={{ marginTop: 50, justifyContent: 'center', alignItems: 'center' }} >
                            {jobs?.length >= 5 && !loading && (
                                <TouchableOpacity
                                    onPress={onPress}
                                    style={{ width: 200, height: 50, borderRadius: 50, backgroundColor: '#ff9901', justifyContent: 'center', alignItems: 'center' }}
                                >
                                    <Text style={{ color: '#ffff' }} >Find More Jobs</Text>
                                </TouchableOpacity>
                            )}
                        </View>
                    </View>
                </ScrollView>
            </SafeAreaView>
        </>
    )
};

export default FilterJobs;
