import React, { useState, useEffect, useRef } from 'react';
import { View, Text, ScrollView, StyleSheet, Dimensions, TouchableOpacity, Animated } from 'react-native';
import { Loading, Profile } from '../../../components';
import { TabView, TabBar } from 'react-native-tab-view';
import { TabProfile } from '../../../components';
import AsyncStorage from '@react-native-async-storage/async-storage';
import { useDispatch, useSelector } from 'react-redux';
import { fetchWorkExperiences } from '../../../store/reducer/workExperienceRedux';
import { fetchEducations } from '../../../store/reducer/educationReducer';
import { fetchCertifications } from '../../../store/reducer/certificationReducer';
import { fetchSkills } from '../../../store/reducer/skillsReducer';
import { fetchPortofolios } from '../../../store/reducer/portofolioReducer';
import auth from '@react-native-firebase/auth';
import { setToken } from '../../../store/action/userAction';

const SecondRoute = () => (
    <View style={{ flex: 1, padding: 20 }} >
    </View>
);


const initialLayout = { width: Dimensions.get('window').width };

const UserProfile = ({ navigation }) => {
    const dispatch = useDispatch()

    const user = useSelector(({ user }) => user.User)
    const token = useSelector(({ user }) => user.Token)

    const [index, setIndex] = useState(0);
    const [routes] = useState([
        { key: 'first', title: 'Profile' },
        { key: 'second', title: 'Activity' },
    ]);
    const [loading, setLoading] = useState(false)

    const translateX = useRef(new Animated.Value(Dimensions.get('window').width)).current

    useEffect(() => {
        if (token) {
            dispatch(fetchWorkExperiences(token))
            dispatch(fetchEducations(token))
            dispatch(fetchCertifications(token))
            dispatch(fetchSkills(token))
            dispatch(fetchPortofolios(token))
        }
    }, [dispatch, token])


    const logout = () => {
        setLoading(true)
        dispatch(setToken(''))
        auth().signOut()
            .then(async () => {
                await AsyncStorage.clear()
                navigation.replace('SignIn')
            })
            .finally(() => setLoading(false))
    }


    return (
        <>
            <View style={{ backgroundColor: 'white', flex: 1 }} >
                <ScrollView
                    showsHorizontalScrollIndicator={false} showsVerticalScrollIndicato={false}
                    style={{ backgroundColor: 'rgba(238,238,238,0.3)', flex: 1 }}
                >
                    <Profile
                        styles={styles}
                        navigation={navigation}
                        user={user}
                        logout={logout}
                    />
                    <View>
                        <TabView
                            navigationState={{ index, routes }}
                            renderScene={() => null}
                            onIndexChange={setIndex}
                            initialLayout={initialLayout}
                            renderTabBar={props => <TabBar {...props}
                                style={{ backgroundColor: '#ffff', elevation: 0 }}
                                inactiveColor='black'
                                activeColor='black'
                                indicatorStyle={{ backgroundColor: '#FF9901', width: 70, height: 8, borderRadius: 10, marginLeft: '12%' }}
                                labelStyle={{ fontSize: 14, fontWeight: '700' }}
                                renderLabel={({ route }) => (
                                    <Text style={{ color: 'black', paddingHorizontal: 10, fontFamily: 'DMSans-Bold' }} >{route.title}</Text>
                                )}
                            />}
                        />
                        {index === 0 && <TabProfile navigation={navigation} user={user} />}
                        {index === 1 && <SecondRoute />}
                    </View>
                </ScrollView>
            </View>
            {loading && <Loading />}
        </>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingHorizontal: 20,
    },
    card: {
        flexDirection: 'row',
        backgroundColor: '#ffff',
        justifyContent: 'space-between',
        alignItems: 'center',
        paddingHorizontal: 20,
        paddingVertical: 20,
        borderRadius: 10,
        marginTop: 14
    },
    title: {
        fontSize: 14,
        fontWeight: '700',
        flex: 1,
        paddingHorizontal: 25
    },
    button: {
        borderWidth: 2,
        borderColor: '#FF9901',
        height: 25,
        width: 75,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 5
    },
    buttonTitle: {
        fontSize: 12,
        fontWeight: '400',
        color: '#FF9901'
    },
    seting: {
        flex: 1,
        position: 'absolute',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        height: Dimensions.get('screen').height,
        width: '100%',
        right: 0,
        top: 0
    }
})

export default UserProfile;
