import React from 'react';
import { useEffect, useState } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, ScrollView, SafeAreaView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import FlashMessage from 'react-native-flash-message';
import { ILChevrontL } from '../../../assets';
import { FormWorkExperience, Loading } from '../../../components';
import { fetchUser } from '../../../store/reducer/userReducer';
import { fetchWorkExperience } from '../../../store/reducer/workExperienceRedux';

const WorkExperience = ({ navigation, route }) => {
    const dispatch = useDispatch()
    const { edit, id } = route.params

    const user = useSelector(({ user }) => user.User)
    const token = useSelector(({ user }) => user.Token)
    const experience = useSelector(({ experiences }) => experiences.WorkExperince)
    const [loading, setLoading] = useState(false)

    useEffect(() => {
        if (edit && token && id) {
            dispatch(fetchWorkExperience(token, id))
        }

        dispatch(fetchUser(user._id, token))
    }, [token])

    return (
        <>
            <View style={{ flex: 1 }} >
                <FlashMessage position='top' />
                <ScrollView style={{ backgroundColor: '#ffff' }}>
                    <View style={[styles.container, { flexDirection: 'row', justifyContent: 'space-between', marginTop: 30 }]}>
                        <TouchableOpacity onPress={() => navigation.goBack()}>
                            <ILChevrontL />
                        </TouchableOpacity>
                        <View>
                            <Text style={{ fontSize: 16 }} >Add work experience</Text>
                        </View>
                        <View></View>
                    </View>
                    <FormWorkExperience
                        navigation={navigation}
                        token={token}
                        experience={experience}
                        edit={edit}
                        id={id}
                        loading={loading}
                        setLoading={setLoading}
                    />
                </ScrollView>
            </View>
            {loading && <Loading />}
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingHorizontal: 20
    }
});

export default WorkExperience;
