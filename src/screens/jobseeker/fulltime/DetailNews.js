import React, { useEffect, useState } from 'react';
import { View, Text, ImageBackground, Linking, ScrollView, TouchableOpacity } from 'react-native';
import { useSelector } from 'react-redux';
import { ILEllipse } from '../../../assets';

const DetailNews = ({ navigation, route }) => {
    const { news } = route.params
    const [date, setDate] = useState('');

    useEffect(() => {
        setDate(new Date(news.pubDate.split(' ')[0]).toLocaleString('id-ID', { month: 'long' }))
    }, [])

    return (
        <ScrollView style={{ flex: 1, backgroundColor: '#ffff' }} >
            <View style={{ padding: 20 }} >
                {news && (
                    <View>
                        <Text style={{ fontSize: 20, fontFamily: 'DMSans-Regular', marginTop: 15 }} >{news.title}</Text>
                        <View style={{ flexDirection: 'row', marginTop: 15, alignItems: 'center' }} >
                            <ILEllipse />
                            <View style={{ width: 9 }} />
                            <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 12, color: '#6B6969' }} >{date}</Text>
                        </View>
                        <View style={{ height: 7, marginTop: 15, backgroundColor: '#eeee', borderRadius: 50 }} />
                        <View>
                            <Text style={{ fontFamily: 'DMSans-Regular', textAlign: 'justify', marginTop: 15, fontSize: 14, letterSpacing: 1 }} >{news.content}</Text>
                        </View>
                    </View>
                )
                }
            </View>
        </ScrollView>
    )
}

export default DetailNews;
