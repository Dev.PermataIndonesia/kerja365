import React, { useState, useEffect } from 'react';
import { Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { useSelector, useDispatch } from 'react-redux';
import { fetchCertifications } from '../../../store/reducer/certificationReducer';
import { fetchEducations } from '../../../store/reducer/educationReducer';
import { fetchSkills } from '../../../store/reducer/skillsReducer';
import { fetchUser } from '../../../store/reducer/userReducer';
import { fetchWorkExperiences } from '../../../store/reducer/workExperienceRedux';
import { Slider } from 'react-native-elements';
import DocumentPicker from 'react-native-document-picker';
import RNFetchBlob from 'rn-fetch-blob';
import { Loading } from '../../../components';
import { showMessage } from 'react-native-flash-message';
import instance from '../../../config/axios';
import { ILThumbsUp, } from '../../../assets';
import { fetchApplication, fetchApplications } from '../../../store/reducer/jobAppliedReducer';
import { getData, setData } from '../../../utils';

const ConfirmApplications = ({ navigation, route }) => {
    const dispatch = useDispatch()

    let { job } = route.params
    job = JSON.parse(job)

    const [resumeComplete, setResumeComplete] = useState(0)
    const [document, setDocument] = useState(null)
    const [cv, setCV] = useState(null)
    const [loading, setLoading] = useState(false)

    const user = useSelector(({ user }) => user.User)
    const token = useSelector(({ user }) => user.Token)
    const experiences = useSelector(({ experiences }) => experiences.WorkExperinces)
    const educations = useSelector(({ educations }) => educations.Educations)
    const skills = useSelector(({ skills }) => skills.Skills)
    const certifications = useSelector(({ certifications }) => certifications.Certifications)

    useEffect(() => {
        (async () => {
            const cv = await getData('cv')
            if (cv) setCV(JSON.parse(cv))
        })()
    }, [])

    useEffect(() => {
        const lengthExperience = experiences?.length > 0 ? 1 : 0
        const lengthEducation = educations?.length > 0 ? 1 : 0
        const lengthSkill = skills?.length >= 3 ? 1 : 0
        const lengthCertification = certifications?.length > 0 ? 1 : 0
        const lengthProfile = Object?.keys(user)?.length > 10 ? 1 : 0

        const total = lengthExperience + lengthEducation + lengthSkill + lengthCertification + lengthProfile

        setResumeComplete(total * 100 / 5.5)

        if (token) {
            dispatch(fetchWorkExperiences(token))
            dispatch(fetchEducations(token))
            dispatch(fetchSkills(token))
            dispatch(fetchCertifications(token))
            dispatch(fetchUser(user._id, token))
        }
    }, [token])

    const apply = async () => {
        setLoading(true)
        try {
            if (!cv) setData('cv', JSON.stringify(document))
            const formData = new FormData()
            const data = {
                jobId: job._id,
                companyId: job.company._id,
                job: job,
                applicant: {
                    experiences,
                    educations,
                    skills,
                    certifications,
                }
            }
            formData.append('data', JSON.stringify(data))
            if (document) formData.append('cv', document)
            await instance.post('/application', formData, {
                headers: {
                    access_token: token,
                    'Content-Type': 'multipart/form-data'
                }
            })
            showMessage({
                type: 'success',
                message: 'Success',
                description: 'Application has send to recruiter',
                duration: 5000
            })
            dispatch(fetchApplication(token, job._id))
            dispatch(fetchApplications(token))
            navigation.navigate('JobDetail', { job })
        } catch (error) {
            console.log(error.message);
            showMessage({
                type: 'warning',
                message: 'Oops!! something wrong',
            })
        }
        setLoading(false)
    }

    const pickCV = async () => {
        try {
            const res = await DocumentPicker.pick({
                type: [DocumentPicker.types.pdf],
            })
            setDocument({
                uri: res.uri,
                type: res.type,
                name: res.name,
                size: res.size
            });

        } catch (error) {
            if (DocumentPicker.isCancel(error)) {
                showMessage({
                    type: 'warning',
                    message: 'Cancel upload CV'
                })
            } else {
                showMessage({
                    type: 'warning',
                    message: 'Oops something error'
                })
            }
        }
    }

    return (
        <>
            <View style={{ flex: 1, backgroundColor: '#ffff' }} >
                <View style={{ backgroundColor: '#ffff', padding: 20, flexDirection: 'row', justifyContent: 'space-between' }} >
                    <View style={{ width: 50 }}>
                        <TouchableOpacity
                            onPress={() => {
                                navigation.goBack()
                            }}
                        >
                            <Text style={{ color: '#e74c3c', fontWeight: '700' }} >Close</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }} >
                        <Text style={{ fontSize: 16, color: '#3498db', fontWeight: '700' }} >Apply</Text>
                    </View>
                    <View style={{ width: 50 }} >
                        <TouchableOpacity
                            onPress={() => navigation.navigate('MainApp', { screen: 'Profile' })}
                        >
                            <Text style={{ color: '#ff9901', fontWeight: '700' }}>Edit</Text>
                        </TouchableOpacity>
                    </View>
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ flex: 1, backgroundColor: 'rgba(238,238,238,0.3)' }}
                >
                    <View style={{ backgroundColor: '#3498db', padding: 20 }} >
                        <Text style={{ color: '#ffff' }} >Confirm your resume</Text>
                    </View>

                    <View style={{ padding: 20 }} >
                        <View>
                            <Text>{resumeComplete.toFixed()}%</Text>
                            <Slider
                                value={resumeComplete ? resumeComplete.toFixed() : 10}
                                minimumValue={0}
                                maximumValue={100}
                                trackStyle={{ height: 10, borderRadius: 10 }}
                                thumbStyle={{ height: 10, width: 10, backgroundColor: '#2ecc71' }}
                                maximumTrackTintColor='#eeee'
                                minimumTrackTintColor='#2ecc71'
                                disabled
                            />
                        </View>
                        <TouchableOpacity
                            onPress={pickCV}
                            style={{ backgroundColor: '#ffff', flexDirection: 'row', justifyContent: 'space-between', borderRadius: 11, padding: 20, marginTop: 20 }}
                        >
                            <Text style={{ fontWeight: '700' }} >Upload your CV</Text>
                            {cv ? (
                                <Text style={{ fontSize: 12, color: '#3498db' }} >{((cv?.name).length > 15) ? (((cv?.name).substring(0, 15 - 3)) + '.....pdf') : cv?.name}</Text>
                            ) : (
                                <Text style={{ fontSize: 12, color: '#3498db' }} >{document?.name ? ((document?.name).length > 15) ? (((document?.name).substring(0, 15 - 3)) + '.....pdf') : document?.name : 'Upload'}</Text>
                            )}
                        </TouchableOpacity>
                        <View style={{ backgroundColor: '#ffff', borderRadius: 11, padding: 20, marginTop: 20 }} >
                            <Text style={{ fontWeight: '700' }} >Contact Info</Text>
                            <View style={{ marginTop: 10 }} >
                                <Text style={{ fontSize: 12, color: 'black' }} >{user.user_name}</Text>
                                <Text style={{ fontSize: 12, color: 'black' }} >{user.user_email}</Text>
                                <Text style={{ fontSize: 12, color: 'black' }} >{user.user_phonenumber}</Text>
                            </View>
                        </View>
                        <View style={{ backgroundColor: '#ffff', borderRadius: 11, padding: 20, marginTop: 20 }} >
                            <Text style={{ fontWeight: '700' }} >Work Experiences</Text>
                            <View style={{ flex: 1 }} >
                                {experiences?.map(el => (
                                    <View
                                        key={el._id}
                                        style={{ paddingVertical: 10 }}
                                    >
                                        <Text style={{ fontSize: 12, color: 'black' }} >{el.position}</Text>
                                        <Text style={{ fontSize: 12, color: 'black' }} >{el.company}</Text>
                                        <Text style={{ fontSize: 12, color: 'black' }} >{el.startDate} - {el.endDate}</Text>
                                    </View>
                                ))}
                            </View>
                        </View>
                        <View style={{ backgroundColor: '#ffff', borderRadius: 11, padding: 20, marginTop: 20 }} >
                            <Text style={{ fontWeight: '700' }} >Educations</Text>
                            <View style={{ flex: 1 }} >
                                {educations?.map(el => (
                                    <View
                                        key={el._id}
                                        style={{ paddingVertical: 10 }}
                                    >
                                        <Text style={{ fontSize: 12, color: 'black' }} >{el.school}</Text>
                                        <Text style={{ fontSize: 12, color: 'black' }} >{el.major}</Text>
                                        <Text style={{ fontSize: 12, color: 'black' }} >{el.startDate} - {el.endDate}</Text>
                                    </View>
                                ))}
                            </View>
                        </View>
                        <View style={{ backgroundColor: '#ffff', borderRadius: 11, padding: 20, marginTop: 20 }} >
                            <Text style={{ fontWeight: '700' }} >Certifications</Text>
                            <View>
                                {certifications?.map(el => (
                                    <View
                                        key={el._id}
                                        style={{ paddingVertical: 10 }}
                                    >
                                        <Text style={{ fontSize: 12, color: 'black' }} >{el.title}</Text>
                                        <Text style={{ fontSize: 12, color: 'black' }} >{el.institution}</Text>
                                        <Text style={{ fontSize: 12, color: 'black' }} >{el.dateCertification}</Text>
                                    </View>
                                ))}
                            </View>
                        </View>
                        <View style={{ backgroundColor: '#ffff', borderRadius: 11, padding: 20, marginTop: 20 }} >
                            <Text style={{ fontWeight: '700' }} >Skills</Text>
                            <View style={{ flexDirection: 'row', flexWrap: 'wrap', flex: 1 }} >
                                {skills?.map((el, i) => (
                                    <View
                                        key={el._id}
                                        style={{
                                            borderWidth: 1, marginLeft: i > 0 ? 5 : 0, marginTop: 7, borderColor: '#FF9901', borderRadius: 5, padding: 3
                                        }}
                                    >
                                        <Text style={{ fontSize: 12, color: '#ff9901' }} >{el.title}</Text>
                                    </View>
                                ))}
                            </View>
                        </View>
                    </View>
                </ScrollView>
                <View style={{ padding: 20, backgroundColor: '#ffff' }} >
                    {resumeComplete >= 70 ? (
                        <TouchableOpacity
                            onPress={apply}
                            style={{ backgroundColor: '#ff9901', padding: 20, justifyContent: 'center', alignItems: 'center', borderRadius: 11 }}
                        >
                            <Text style={{ color: '#ffff', fontWeight: '700' }} >Submit</Text>
                        </TouchableOpacity>
                    ) : (
                        <View
                            style={{ backgroundColor: '#eeee', padding: 20, justifyContent: 'center', alignItems: 'center', borderRadius: 11 }}
                        >
                            <Text style={{ color: '#ff9901', fontWeight: '700' }} >Complete your resume</Text>
                        </View>
                    )}
                </View>
            </View>
            {loading && <Loading />}
        </>
    )
};

export default ConfirmApplications;
