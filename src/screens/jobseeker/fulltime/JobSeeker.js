import React, { useState, useEffect, useCallback } from 'react'
import { View, StyleSheet, ScrollView, Text, RefreshControl, TouchableOpacity, TextInput, Dimensions } from 'react-native'
import { useDispatch, useSelector } from 'react-redux'
import {
    ILSliders,
    ILMoreVErtical,
    ILSearch
} from '../../../assets'
import { JobCard, JobNews, UserSection, GroupButton, FulltimeJob, Filter, Video } from '../../../components'
import { fetchInterestUser, fetchJobs } from '../../../store/reducer/jobsReducer'
import { getData } from '../../../utils/localStorage'
import { fetchNotifications } from '../../../store/reducer/messagesReducer'
import { fetchNews } from '../../../store/reducer/newsReducer'
import { fetchVideos } from '../../../store/reducer/videosReducer'
import SkeletonPlaceholder from 'react-native-skeleton-placeholder'

const wait = (timeout) => {
    return new Promise(resolve => setTimeout(resolve, timeout));
}

const JobSeeker = ({ navigation }) => {
    const dispatch = useDispatch()
    const [search, setSearch] = useState('')
    const [refreshing, setRefreshing] = useState(false)

    const user = useSelector(({ user }) => user.User)
    const token = useSelector(({ user }) => user.Token)
    const jobs = useSelector(({ jobs }) => jobs.Jobs)
    const loading = useSelector(({ jobs }) => jobs.JobsLoading)
    const interestJobs = useSelector(({ jobs }) => jobs.InterestJobs)

    useEffect(() => {
        (async () => {
            const user = await getData('user')
            const token = await getData('token')
            dispatch({ type: 'SET_USER', payload: user })
            dispatch({ type: 'SET_TOKEN', payload: token })
        })()
        if (token) {
            if (jobs.length < 1) {
                dispatch(fetchJobs(token, 0))
            }
            if (interestJobs.length < 1 && user?.interestCategory?.length > 0) {
                dispatch(fetchInterestUser(token, 0, user.interestCategory))
            }

            dispatch(fetchNotifications(user.id))
            dispatch(fetchNews())
            dispatch(fetchVideos(0, 3))
        }
    }, [refreshing, token])

    const onPress = () => {
        dispatch(fetchJobs(token, jobs.length))
    }

    const onRefreshing = useCallback(() => {
        setRefreshing(true)
        wait(100).then(() => setRefreshing(false))
    }, [])

    return (
        <>
            <View style={{ flex: 1, backgroundColor: 'white' }}>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    showsHorizontalScrollIndicator={false}
                    style={{ backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
                    refreshControl={
                        <RefreshControl
                            refreshing={refreshing}
                            onRefresh={onRefreshing}
                        />
                    }
                >
                    <UserSection
                        user={user}
                        navigation={navigation}
                    />
                    <View style={{ backgroundColor: '#ffff', padding: 20 }} >
                        <GroupButton user={user} navigation={navigation} />
                    </View>
                    <View style={{ paddingHorizontal: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', marginTop: 20 }}>
                        <View style={{ backgroundColor: '#ffff', height: 50, width: 40, justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }} >
                            <ILSearch />
                        </View>
                        <View style={{ flex: 1, justifyContent: 'center' }}>
                            <TextInput
                                placeholder='Search Job'
                                placeholderTextColor='#c4c4c4'
                                style={{ backgroundColor: '#ffff', color: 'black', height: 50 }}
                                onChangeText={val => setSearch(val)}
                                value={search}
                            />
                        </View>
                        <View>
                            {search.length > 3 ? (
                                <TouchableOpacity
                                    onPress={() => navigation.navigate('SearchJob', { search: search })}
                                    style={{ backgroundColor: '#ff9901', height: 50, paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center', borderTopRightRadius: 10, borderBottomRightRadius: 10 }}
                                >
                                    <Text style={{ fontSize: 10, color: '#ffff' }} >Search</Text>
                                </TouchableOpacity>
                            ) : (
                                <View
                                    style={{ backgroundColor: '#ffff', height: 50, paddingHorizontal: 10, justifyContent: 'center', alignItems: 'center', borderTopRightRadius: 10, borderBottomRightRadius: 10 }}
                                >
                                </View>
                            )}
                        </View>
                        <View style={{ width: 10 }} />
                        <TouchableOpacity
                            onPress={() => navigation.navigate('FilterPage')}
                            style={{ height: 52, width: 52, backgroundColor: '#FF9901', alignItems: 'center', justifyContent: 'center', borderRadius: 5 }}
                        >
                            <ILSliders />
                        </TouchableOpacity>
                    </View>

                    <View style={{ marginTop: 20, paddingHorizontal: 20 }} >
                        <View style={{ flexDirection: 'row', justifyContent: 'space-between' }} >
                            <Text style={{ fontWeight: '700' }} >Recomended for you</Text>
                            <TouchableOpacity
                                onPress={() => navigation.navigate('InterestJobs')}
                            >
                                <Text style={{ color: '#ff9901' }} >Show more</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{ marginTop: 20 }} >
                            {loading ? (
                                <SkeletonPlaceholder>
                                    <View style={{ height: 70, borderRadius: 11 }} />
                                    <View style={{ height: 70, borderRadius: 11, marginTop: 10 }} />
                                    <View style={{ height: 70, borderRadius: 11, marginTop: 10 }} />
                                </SkeletonPlaceholder>
                            ) : (
                                <>
                                    {interestJobs && interestJobs.slice(0, 3).map(job => (
                                        <JobCard
                                            key={job._id}
                                            ILMoreVErtical={ILMoreVErtical}
                                            job={job}
                                            navigation={navigation}
                                        />
                                    ))}
                                    {!interestJobs || interestJobs?.length < 1 && (
                                        <View style={{ justifyContent: 'center', alignItems: 'center', padding: 20 }} >
                                            <Text>No data</Text>
                                        </View>
                                    )}
                                </>
                            )}
                        </View>
                    </View>
                    <View style={styles.space} />
                    <FulltimeJob
                        search={search}
                        setSearch={setSearch}
                        navigation={navigation}
                        jobs={jobs}
                        limit={5}
                        onPress={onPress}
                        token={token}
                    />
                    <View style={styles.space} />
                    <Video
                        navigation={navigation}
                    />
                    <JobNews
                        navigation={navigation}
                    />
                </ScrollView>
            </View>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        paddingTop: 60,
        paddingHorizontal: 20
    },
    text: {
        fontSize: 16,
        fontFamily: 'DMSans-Bold'
    },
    space: {
        height: 15,
        width: 15
    },
    textInput: {
        borderRadius: 150
    },
    filter: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    cardHorizontalView: {
        width: '80%'
    },
    logo: {
        width: 50,
        height: 50
    },
    moreVertical: {
        width: 20,
        height: 20
    },
    btn: {
        marginTop: 37,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgba(238, 238, 238, 0.5)',
        paddingVertical: 16,
        borderRadius: 50
    },
    btnTitle: {
        color: '#FF9901',
        fontFamily: 'DMSans-Regular',
        fontSize: 14
    }
})

export default JobSeeker
