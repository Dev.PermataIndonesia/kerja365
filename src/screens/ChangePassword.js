import React, { useState } from 'react';
import { Text, View, ScrollView, TouchableOpacity, TextInput } from 'react-native';
import { showMessage } from 'react-native-flash-message';
import { useSelector } from 'react-redux';
import { ILChevrontL, ILEye } from '../assets';
import { Loading } from '../components';
import instance from '../config/axios';
import useForm from '../helpers/useForm';

const ChangePassword = ({ navigation }) => {
    const token = useSelector(({ user }) => user.Token)

    const [form, setForm] = useForm({
        oldPassword: '',
        newPassword: '',
        confirmPassword: ''
    })

    const [confirm, setConfirm] = useState('')
    const [loading, setLoading] = useState(false)

    const [visibleOldPassword, setVisibleOldPassword] = useState(true)
    const [visibleNewPassword, setVisibleNewPassword] = useState(true)
    const [visibleConfirmPassword, setVisibleConfirmPassword] = useState(true)

    const onPress = async () => {
        setLoading(true)
        try {
            if (!form.oldPassword || !form.newPassword || !form.confirmPassword) {
                throw { message: 'Field required' }
            }

            if (form.oldPassword?.toLowerCase() === form.newPassword?.toLowerCase()) {
                throw { message: 'The password must be different from the old password' }
            }

            if (form.newPassword !== form.confirmPassword) {
                throw { message: 'Password not matched' }
            }

            const { data } = await instance.post('/user/change-password', { oldPassword: form.oldPassword, newPassword: form.newPassword }, {
                headers: {
                    access_token: token
                }
            })
            if (data.message === 'Wrong password') {
                throw data
            } else {
                showMessage({
                    type: 'success',
                    message: 'Change password success',
                    description: data.message
                })
                navigation.goBack()
            }
        } catch (error) {
            showMessage({
                type: 'warning',
                message: 'Oops! something wrong',
                description: error.message
            })
        }
        setLoading(false)
    }

    const confirmPassword = (val) => {
        setForm('confirmPassword', val)
        if (form.newPassword === val) setConfirm('Password Matched')
        else setConfirm('Password not matched')
    }

    const newPassword = (val) => {
        setForm('newPassword', val)
        if (form.confirmPassword === val) setConfirm('Password Matched')
        else setConfirm('Password not matched')
    }

    return (
        <>
            <View
                style={{ flex: 1, backgroundColor: '#ffff' }}
            >
                <View>
                    <View style={{ marginTop: 20, paddingBottom: 50, paddingHorizontal: 20, justifyContent: 'space-between', flexDirection: 'row' }} >
                        <TouchableOpacity
                            onPress={() => navigation.goBack()}
                            style={{ width: 50 }}
                        >
                            <ILChevrontL />
                        </TouchableOpacity>
                        <View>
                            <Text style={{ fontSize: 16, color: 'black' }} >Change Password</Text>
                        </View>
                        <View style={{ width: 50 }} />
                    </View>
                </View>
                <ScrollView
                    showsVerticalScrollIndicator={false}
                    style={{ padding: 20 }}
                >
                    <View style={{ flexDirection: 'row', borderWidth: 1, borderColor: '#c4c4c4', borderRadius: 11, padding: 5 }} >
                        <TextInput
                            style={{ flex: 1, color: 'black' }}
                            placeholder='Old Password'
                            placeholderTextColor='#c4c4c4'
                            onChangeText={val => setForm('oldPassword', val)}
                            value={form?.oldPassword}
                            secureTextEntry={visibleOldPassword}
                        />
                        <View style={{ justifyContent: 'center', width: 50, alignItems: 'center' }} >
                            <TouchableOpacity
                                onPress={() => visibleOldPassword ? setVisibleOldPassword(false) : setVisibleOldPassword(true)}
                            >
                                <ILEye />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ marginTop: 20, height: 20 }} >
                        {form?.confirmPassword?.length > 0 && (
                            <Text style={{ color: confirm === 'Password Matched' ? '#ff9901' : 'red' }} >{confirm}</Text>
                        )}
                    </View>
                    <View>
                        <Text>Set new password</Text>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, borderWidth: 1, borderColor: '#c4c4c4', borderRadius: 11, padding: 5 }} >
                        <TextInput
                            style={{ flex: 1, color: 'black' }}
                            placeholder='New Password'
                            placeholderTextColor='#c4c4c4'
                            onChangeText={val => newPassword(val)}
                            value={form?.newPassword}
                            secureTextEntry={visibleNewPassword}
                        />
                        <View style={{ justifyContent: 'center', width: 50, alignItems: 'center' }} >
                            <TouchableOpacity
                                onPress={() => visibleNewPassword ? setVisibleNewPassword(false) : setVisibleNewPassword(true)}
                            >
                                <ILEye />
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{ flexDirection: 'row', marginTop: 20, borderWidth: 1, borderColor: '#c4c4c4', borderRadius: 11, padding: 5 }} >
                        <TextInput
                            style={{ flex: 1, color: 'black' }}
                            placeholder='Confirm Password'
                            placeholderTextColor='#c4c4c4'
                            onChangeText={val => confirmPassword(val)}
                            value={form?.confirmPassword}
                            secureTextEntry={visibleConfirmPassword}
                        />
                        <View style={{ justifyContent: 'center', width: 50, alignItems: 'center' }} >
                            <TouchableOpacity
                                onPress={() => visibleConfirmPassword ? setVisibleConfirmPassword(false) : setVisibleConfirmPassword(true)}
                            >
                                <ILEye />
                            </TouchableOpacity>
                        </View>
                    </View>
                    {confirm === 'Password Matched' && (
                        <TouchableOpacity
                            onPress={onPress}
                            style={{ backgroundColor: '#ff9901', padding: 20, marginTop: 30, borderRadius: 11, justifyContent: 'center', alignItems: 'center' }}
                        >
                            <Text style={{ color: '#ffff', fontWeight: '700' }} >Submit</Text>
                        </TouchableOpacity>
                    )}
                </ScrollView>
            </View>
            {loading && <Loading />}
        </>
    )
};

export default ChangePassword;
