import React, { useEffect, useState } from 'react';
import { Text, Image, View, TouchableOpacity, ScrollView } from 'react-native';
import { useDispatch, useSelector } from 'react-redux';
import { ILChevrontL } from '../assets';
import { fetchVideos } from '../store/reducer/videosReducer';

const Videos = ({ navigation }) => {
    const dispatch = useDispatch()

    const videos = useSelector(({ videos }) => videos.Videos)

    useEffect(() => {
        dispatch(fetchVideos(0, 0))
    }, [])

    return (
        <View
            style={{ backgroundColor: '#ffff', flex: 1 }}
        >
            <View style={{ flexDirection: 'row', backgroundColor: '#ffff', padding: 20, justifyContent: 'space-between', paddingHorizontal: 20 }}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <ILChevrontL />
                </TouchableOpacity>
                <View>
                    <Text style={{ fontSize: 16 }} >Videos</Text>
                </View>
                <View></View>
            </View>
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={{ paddingHorizontal: 20, flex: 1, backgroundColor: 'rgba(238,238,238,0.7)' }}
            >
                {videos && videos?.map((video, i) => (

                    <TouchableOpacity
                        key={video._id}
                        onPress={() => navigation.navigate('VideoPlay', { video: JSON.stringify(video) })}
                        style={{ padding: 10, marginTop: 20, shadowColor: "#000", shadowOffset: { width: 0, height: 1 }, shadowOpacity: 0.22, shadowRadius: 2.22, elevation: 3, backgroundColor: '#ffff', borderRadius: 11 }}
                    >
                        <Image source={{ uri: video.thumbnail }} resizeMode="cover" style={{ height: 180, borderRadius: 7 }} />
                        <View style={{ flexDirection: 'row', marginTop: 10 }} >
                            <Text style={{ fontSize: 11, width: 200 }} >{video.title}</Text>
                        </View>
                    </TouchableOpacity>
                ))}
            </ScrollView>
        </View>
    )
};

export default Videos;
