import React, { useEffect, useState } from 'react';
import { ActivityIndicator } from 'react-native';
import { SafeAreaView, Text, TextInput, View, TouchableOpacity, Image } from 'react-native';
import { ScrollView } from 'react-native-gesture-handler';
import { useDispatch, useSelector } from 'react-redux';
import { ILChevrontL, ILMoreVErtical, ILSearch, ILSliders } from '../assets';
import Avatar from '../assets/img/avatar.png';
import { searchJobseeker } from '../store/reducer/userReducer';

const SearchJobseeker = ({ navigation, route }) => {
    let { search } = route.params
    const dispatch = useDispatch()

    const [newSearch, setSearch] = useState(search)

    const token = useSelector(({ user }) => user.Token)
    const getJobseeker = useSelector(({ user }) => user.SearchJobseeker)
    const loading = useSelector(({ jobs }) => jobs.JobsLoading)

    useEffect(() => {
        dispatch(searchJobseeker(newSearch, token))
    }, [token, newSearch])

    const searchJob = (val) => {
        setSearch(val)
    }

    return (
        <SafeAreaView
            style={{ backgroundColor: '#ffff', flex: 1 }}
        >
            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, paddingHorizontal: 20 }}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <ILChevrontL />
                </TouchableOpacity>
                <View>
                    <Text style={{ fontSize: 16 }} >Search Jobseeker</Text>
                </View>
                <View></View>
            </View>
            <View style={{ paddingHorizontal: 20, marginTop: 20, flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center' }}>
                <View style={{ backgroundColor: '#ffff', height: 50, width: 40, justifyContent: 'center', alignItems: 'center', borderTopLeftRadius: 10, borderBottomLeftRadius: 10 }} >
                    <ILSearch />
                </View>
                <View style={{ flex: 1, justifyContent: 'center' }}>
                    <TextInput
                        placeholder='Search Job'
                        placeholderTextColor='#c4c4c4'
                        style={{ backgroundColor: '#ffff', color: 'black', height: 50, borderTopRightRadius: 10, borderBottomRightRadius: 10 }}
                        onChangeText={searchJob}
                        value={newSearch}
                        autoFocus
                    />
                </View>
                <View style={{ width: 10 }} />
                <TouchableOpacity style={{ height: 52, width: 52, backgroundColor: '#FF9901', alignItems: 'center', justifyContent: 'center', borderRadius: 5 }} >
                    <ILSliders />
                </TouchableOpacity>
            </View>
            <ScrollView
                showsVerticalScrollIndicator={false}
                style={{ flex: 1, marginTop: 20, backgroundColor: 'rgba(238, 238, 238, 0.3)' }}
            >
                <View
                    style={{ padding: 20 }}
                >
                    {getJobseeker && getJobseeker.filter(el => el.user_name.toLowerCase().includes(newSearch?.toLowerCase())).map(jobseeker => (
                        <TouchableOpacity
                            key={jobseeker._id}
                            onPress={() => navigation.navigate('DetailJobseeker', { id: jobseeker._id })}
                            style={{
                                flexDirection: 'row', justifyContent: 'space-between', marginTop: 20, backgroundColor: '#ffff', borderRadius: 10, alignItems: 'center', padding: 20
                            }}
                        >
                            <Image source={jobseeker.photo ? { uri: jobseeker.photo } : Avatar} style={{ width: 50, height: 50, borderRadius: 50 / 2 }} />
                            <View style={{ flex: 1, marginLeft: 12 }} >
                                <Text style={{ fontFamily: 'DMSans-Bold', fontSize: 16 }} >{jobseeker.user_name}</Text>
                                <Text style={{ fontFamily: 'DMSans-Regular', fontSize: 12 }} >{jobseeker?.type}</Text>
                            </View>
                            <ILMoreVErtical />
                        </TouchableOpacity>
                    ))}
                    {loading && <ActivityIndicator color='#ff9901' size='small' style={{ marginTop: 20 }} />}
                </View>
            </ScrollView>
        </SafeAreaView>
    )
};

export default SearchJobseeker;
