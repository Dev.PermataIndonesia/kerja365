import axios from 'axios'

// 'https://k365.permataindonesia.com'

const instance = axios.create({
    baseURL: 'https://whispering-castle-17019.herokuapp.com'
})

export default instance
