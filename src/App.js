/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect, useState } from 'react';
import 'react-native-gesture-handler';
import { Provider } from 'react-redux';
import FlashMessage from 'react-native-flash-message';
import { NavigationContainer } from '@react-navigation/native';
import { StatusBar, LogBox } from 'react-native';
import messaging from '@react-native-firebase/messaging';
import { showMessage } from 'react-native-flash-message';
import store from './store';
import Router from './routes';
import { getData } from './utils/localStorage';
import instance from './config/axios';
import NotifService from './NotifService';

const saveTokenToDatabase = async (fcmToken) => {
  const getUserFromStorage = await getData('user')
  let id
  if (getUserFromStorage) {
    if (getUserFromStorage.type === 'internal' || getUserFromStorage.type === 'tko') id = getUserFromStorage.nrk
    else id = getUserFromStorage._id
    const authToken = await getData('token')
    await instance.patch(`/user/fcm-token/${id}`, { fcmToken }, {
      headers: {
        access_token: authToken
      }
    })
  }
}

const App = () => {
  LogBox.ignoreLogs(['Setting a timer'])

  const [registerToken, setRegisterToken] = useState('');
  const [fcmRegistered, setFcmRegistered] = useState(false);
  const [token, setToken] = useState('');

  const onRegister = (token) => {
    setRegisterToken(token.token);
    setFcmRegistered(true);
  };
  const onNotif = (notif) => {
    showMessage({
      type: 'info',
      message: notif.title,
      description: notif.message
    })
  };

  const notif = new NotifService(onRegister, onNotif)

  useEffect(() => {
    (async () => {
      const authToken = await getData('token')
      setToken(authToken)
      const fcmToken = await messaging().getToken()
      saveTokenToDatabase(fcmToken)
    })()
  }, [token])

  useEffect(() => {
    return messaging().onMessage(async remoteMessage => {
      notif.localNotif('sample.mp3', remoteMessage.notification)
      // console.log(remoteMessage.notification, "<<< fcm")
    });
  }, [])

  useEffect(() => {
    return messaging().onTokenRefresh(token => {
      saveTokenToDatabase(token)
    })
  }, [])

  return (
    <Provider store={store} >
      <StatusBar barStyle='light-content' />
      <NavigationContainer>
        <Router />
      </NavigationContainer>
      <FlashMessage position='top' />
    </Provider>
  )
};


export default App;
