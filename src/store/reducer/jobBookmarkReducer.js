import instance from "../../config/axios"

const initialState = {
    Bookmarks: [],
    Bookmark: {},
    Loading: false
}

export default function jobBookmarkReducer(state = initialState, action) {
    switch (action.type) {
        case 'SET_BOOKMARKS':
            return { ...state, Bookmarks: action.payload }
        case 'SET_BOOKMARK':
            return { ...state, Bookmark: action.payload }
        case 'SET_BOOKMARK_LOADING':
            return { ...state, Loading: action.payload }
        default:
            return state
    }
}

export function fetchBookmarks(token) {
    return async (dispatch) => {
        dispatch({ type: 'SET_BOOKMARK_LOADING', payload: true })
        const { data } = await instance.get('/bookmark', {
            headers: {
                access_token: token
            }
        })
        dispatch({ type: 'SET_BOOKMARKS', payload: data })
        dispatch({ type: 'SET_BOOKMARK_LOADING', payload: false })
    }
}

export function fetchBookmark(token, jobId) {
    return async (dispatch) => {
        dispatch({ type: 'SET_BOOKMARK_LOADING', payload: true })
        const { data } = await instance.get(`/bookmark/marked/${jobId}`, {
            headers: {
                access_token: token
            }
        })
        if (data) dispatch({ type: 'SET_BOOKMARK', payload: data })
        else dispatch({ type: 'SET_BOOKMARK', payload: data })

        dispatch({ type: 'SET_BOOKMARK_LOADING', payload: false })
    }
}
