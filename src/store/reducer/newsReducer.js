import instance from "../../config/axios"

const initialState = {
    News: [],
    Loading: false
}

export default function newsReducer(state = initialState, action) {
    switch (action.type) {
        case 'SET_NEWS':
            return { ...state, News: action.payload }
        case 'SET_NEWS_LOADING':
            return { ...state, Loading: action.payload }
        default:
            return state
    }
}

export function fetchNews() {
    return async (dispatch) => {
        dispatch({ type: 'SET_NEWS_LOADING', payload: true })

        const { data } = await instance.get('/news')
        if (data) dispatch({ type: 'SET_NEWS', payload: data })
        else dispatch({ type: 'SET_NEWS', payload: [] })

        dispatch({ type: 'SET_NEWS_LOADING', payload: false })
    }
}
