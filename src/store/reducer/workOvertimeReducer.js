import instance from "../../config/axios"

const initialState = {
    Overtimes: [],
    Loading: false
}

export default function overtimeReducers(state = initialState, action) {
    switch (action.type) {
        case 'SET_OVERTIMES':
            return { ...state, Overtimes: action.payload }
        case 'SET_OVERTIMES_LOADING':
            return { ...state, Loading: action.payload }
        default:
            return state
    }
}

export function fetchOvertimes(token) {
    return async (dispatch) => {
        dispatch({ type: 'SET_OVERTIMES_LOADING', payload: true })
        const { data } = await instance.get('/work-overtime', {
            headers: {
                access_token: token
            }
        })
        dispatch({ type: 'SET_OVERTIMES', payload: data })
        dispatch({ type: 'SET_OVERTIMES_LOADING', payload: false })
    }
}
