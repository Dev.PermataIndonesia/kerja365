import instance from '../../config/axios';

const initialState = {
    Jobs: [],
    InterestJobs: [],
    SearchJobs: [],
    Job: {},
    JobsCategory: {},
    JobsLoading: false,
    JobsCategoryLoading: false,
    FilterJobs: []
}

export default function jobsReducer(state = initialState, action) {
    switch (action.type) {
        case 'SET_JOBS':
            if (state.Jobs.length < 1) {
                return { ...state, Jobs: action.payload }
            } else {
                const concatJob = state.Jobs.concat(action.payload)
                return { ...state, Jobs: concatJob }
            }
        case 'SET_JOB':
            return { ...state, Job: action.payload }
        case 'SET_JOBS_CATEGORY':
            if (Object.keys(state.JobsCategory).length < 1 || !state.JobsCategory[`${action.payload.key}`]) {
                return { ...state, JobsCategory: action.payload.data }
            } else {
                const obj = {}
                const concatJob = state.JobsCategory[`${action.payload.key}`].concat(action.payload.data[`${action.payload.key}`])
                obj[`${action.payload.key}`] = concatJob
                return { ...state, JobsCategory: obj }
            }
        case 'SET_INTEREST_JOB':
            if (state.InterestJobs.length < 1) {
                return { ...state, InterestJobs: action.payload }
            } else {
                const concatJob = state.InterestJobs.concat(action.payload)
                return { ...state, InterestJobs: concatJob }
            }
        case 'SET_SEARCH_JOB':
            return { ...state, SearchJobs: action.payload }
        case 'SET_FILTER_JOB':
            if (state.FilterJobs.length < 1 || action.payload.reset) {
                return { ...state, FilterJobs: action.payload.data }
            } else {
                const concatJob = state.FilterJobs.concat(action.payload.data)
                return { ...state, FilterJobs: concatJob }
            }
        case 'SET_JOB_LOADING':
            return { ...state, JobsLoading: action.payload }
        case 'SET_JOB_CATEGORY_LOADING':
            return { ...state, JobsCategoryLoading: action.payload }
        default:
            return state
    }
}


export function fetchJobs(token, querySkip) {
    return async (dispatch) => {
        dispatch({ type: 'SET_JOB_CATEGORY_LOADING', payload: true })
        const { data } = await instance.get(`/job?skip=${querySkip}`, {
            headers: {
                access_token: token
            }
        })
        dispatch({ type: 'SET_JOBS', payload: data })
        dispatch({ type: 'SET_JOB_CATEGORY_LOADING', payload: false })

    }
}

export function fetchJob(id, token) {
    return async (dispatch) => {
        const { data } = await instance.get(`/job/${id}`, {
            headers: {
                access_token: token
            }
        })
        if (data) {
            dispatch({ type: 'SET_JOB', payload: data })
        } else {
            dispatch({ type: 'SET_JOB', payload: {} })
        }
    }
}

export function fetchJobsByCategory(category, token, querySkip) {
    return async (dispatch) => {
        dispatch({ type: 'SET_JOB_CATEGORY_LOADING', payload: true })
        const { data } = await instance.get(`/job/category/${category.title}?skip=${querySkip}`, {
            headers: {
                access_token: token
            }
        })
        const jobs = {}
        jobs[`${category.key}`] = data
        if (data) {
            dispatch({ type: 'SET_JOBS_CATEGORY', payload: { data: jobs, key: category.key } })
        } else {
            dispatch({ type: 'SET_JOBS_CATEGORY', payload: { data: [], key: category.key } })
        }

        dispatch({ type: 'SET_JOB_CATEGORY_LOADING', payload: false })
    }
}

export function fetchSearchJobs(token, queryTitle) {
    return async (dispatch) => {
        dispatch({ type: 'SET_JOB_LOADING', payload: true })
        const { data } = await instance.get(`/job/search?title=${queryTitle}`, {
            headers: {
                access_token: token
            }
        })
        if (data) {
            dispatch({ type: 'SET_SEARCH_JOB', payload: data })
        } else {
            dispatch({ type: 'SET_SEARCH_JOB', payload: [] })
        }

        dispatch({ type: 'SET_JOB_LOADING', payload: false })
    }
}

export function fetchInterestUser(token, paramSkip, queryCategories) {
    return async (dispatch) => {
        dispatch({ type: 'SET_JOB_LOADING', payload: true })
        const interested = queryCategories.map(item => {
            return { category: item }
        })
        if (interested) {
            const { data } = await instance.get(`/job/interested/${paramSkip}?interest=${JSON.stringify(interested)}`, {
                headers: {
                    access_token: token
                }
            })
            dispatch({ type: 'SET_INTEREST_JOB', payload: data })
            dispatch({ type: 'SET_JOB_LOADING', payload: false })
        }
    }
}

export function fetchFilterJobs(token, skip, category, area, reset) {
    return async (dispatch) => {
        dispatch({ type: 'SET_JOB_LOADING', payload: true })
        const { data } = await instance.get(`/job/filter?skip=${skip}&category=${category}&area=${area}`, {
            headers: {
                access_token: token
            }
        })
        dispatch({ type: 'SET_FILTER_JOB', payload: { data: data, reset: reset } })
        dispatch({ type: 'SET_JOB_LOADING', payload: false })
    }
}
