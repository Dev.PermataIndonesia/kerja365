import instance from "../../config/axios"

const initialState = {
    Permissions: [],
    Permission: {},
    Loading: false
}

export default function permissionReducer(state = initialState, action) {
    switch (action.type) {
        case "SET_PERMISSIONS":
            return { ...state, Permissions: action.payload }
        case "SET_PERMISSION":
            return { ...state, Permission: action.payload }
        case "SET_PERMISSIONS_LOADING":
            return { ...state, Loading: action.payload }
        default:
            return state
    }
}

export function fetchPermissions(token) {
    return async (dispatch) => {
        dispatch({ type: "SET_PERMISSIONS_LOADING", payload: true })

        const { data } = await instance.get('/permission', {
            headers: {
                access_token: token
            }
        })
        dispatch({ type: "SET_PERMISSIONS", payload: data })
        dispatch({ type: "SET_PERMISSIONS_LOADING", payload: false })
    }
}

export function fetchPermission(token) {
    return async (dispatch) => {
        dispatch({ type: "SET_PERMISSIONS_LOADING", payload: true })

        const { data } = await instance.get('/permission/waiting', {
            headers: {
                access_token: token
            }
        })
        dispatch({ type: "SET_PERMISSION", payload: data })
        dispatch({ type: "SET_PERMISSIONS_LOADING", payload: false })
    }
}
