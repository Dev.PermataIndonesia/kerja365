import instance from "../../config/axios"

const initialState = {
    Activities: [],
    Loading: false
}

export default function activityReducers(state = initialState, action) {
    switch (action.type) {
        case 'SET_ACTIVITIES':
            return { ...state, Activities: action.payload }
        case 'SET_ACTIVITIES_LOADING':
            return { ...state, Loading: action.payload }
        default:
            return state
    }
}

export function fetchActivities(token) {
    return async (dispatch) => {
        dispatch({ type: 'SET_ACTIVITIES_LOADING', payload: true })
        const { data } = await instance.get('/activity', {
            headers: {
                access_token: token
            }
        })
        dispatch({ type: 'SET_ACTIVITIES', payload: data })
        dispatch({ type: 'SET_ACTIVITIES_LOADING', payload: false })
    }
}
