import instance from "../../config/axios"

const initialState = {
    User: {},
    Message: '',
    Token: '',
    Fulltimers: [],
    Freelancers: [],
    SearchJobseeker: [],
    Jobseeker: {},
    GetUser: {},
    GetCv: {},
    Confirm: {},
    Loading: false
}

export default function userReducer(state = initialState, action) {
    switch (action.type) {
        case 'SET_USER':
            return { ...state, User: action.payload }
        case 'SET_MESSAGE':
            return { ...state, Message: action.payload }
        case 'SET_TOKEN':
            return { ...state, Token: action.payload }
        case 'SET_FULLTIMERS':
            if (state.Fulltimers.length > 0) {
                const concatFulltimer = state.Fulltimers.concat(action.payload)
                return { ...state, Fulltimers: concatFulltimer }
            } else return { ...state, Fulltimers: action.payload }
        case 'SET_FREELANCERS':
            if (state.Freelancers.length > 0) {
                const concatFreelancer = state.Freelancers.concat(action.payload)
                return { ...state, Freelancers: concatFreelancer }
            } else return { ...state, Freelancers: action.payload }
        case 'SET_JOBSEEKER':
            return { ...state, Jobseeker: action.payload }
        case 'SET_SEARCH_JOBSEEKER':
            return { ...state, SearchJobseeker: action.payload }
        case 'GET_USER':
            return { ...state, GetUser: action.payload }
        case 'GET_CV':
            return { ...state, GetCv: action.payload }
        case 'SET_CONFIRM':
            return { ...state, Confirm: action.payload }
        case 'SET_LOADING':
            return { ...state, Loading: action.payload }
        default:
            return state
    }
}

export function updatePatchUser(id, payload) {
    return async (dispatch) => {
        try {
            const { data } = await instance.patch(`/user/${id}`, { interestCategory: payload })
            dispatch({ type: 'SET_MESSAGE', payload: 'success' })
        } catch (error) {
            dispatch({ type: 'SET_MESSAGE', payload: 'error' })
        }
    }
}

export function fetchUser(id, token) {
    return async (dispatch) => {
        try {
            const { data } = await instance.get(`/user/${id}`, {
                headers: {
                    access_token: token
                }
            })
            dispatch({ type: 'SET_USER', payload: data })
        } catch (error) {
            dispatch({ type: 'SET_USER', payload: {} })
        }
    }
}

export function fetchUserByEmail(email) {
    return async (dispatch) => {
        try {
            const { data } = await instance.get(`/user/email/${email}`)
            dispatch({ type: 'SET_USER', payload: data })
        } catch (error) {
            dispatch({ type: 'SET_MESSAGE', payload: error.message })
        }
    }
}

export function fetchUserByPhoneNumber(phoneNumber, token) {
    return async (dispatch) => {
        const { data } = await instance.get(`/user/phone-number/${phoneNumber}`, {
            headers: {
                access_token: token
            }
        })
        if (data) {
            dispatch({ type: 'GET_USER', payload: data })
        } else {
            dispatch({ type: 'GET_USER', payload: {} })
        }
    }
}

export function fetchFulltimers(token, skip) {
    return async (dispatch) => {
        dispatch({ type: 'SET_LOADING', payload: true })
        try {
            const { data } = await instance.get(`/user/type/fulltimer?skip=${skip}`, {
                headers: {
                    access_token: token
                }
            })
            dispatch({ type: 'SET_FULLTIMERS', payload: data })
            dispatch({ type: 'SET_LOADING', payload: false })
        } catch (error) {
            dispatch({ type: 'SET_MESSAGE', payload: error.message })
            dispatch({ type: 'SET_LOADING', payload: false })
        }
    }
}

export function fetchFreelancers(token, skip) {
    return async (dispatch) => {
        dispatch({ type: 'SET_LOADING', payload: true })
        try {
            const { data } = await instance.get(`/user/type/freelancer?skip=${skip}`, {
                headers: {
                    access_token: token
                }
            })
            dispatch({ type: 'SET_FREELANCERS', payload: data })
            dispatch({ type: 'SET_LOADING', payload: false })
        } catch (error) {
            dispatch({ type: 'SET_MESSAGE', payload: error.message })
            dispatch({ type: 'SET_LOADING', payload: false })
        }
    }
}

export function fetchJobseeker(id, token) {
    return async (dispatch) => {
        try {
            const { data } = await instance.get(`/user/${id}`, {
                headers: {
                    access_token: token
                }
            })
            if (data) {
                dispatch({ type: 'SET_JOBSEEKER', payload: data })
            } else {
                dispatch({ type: 'SET_JOBSEEKER', payload: {} })
            }
        } catch (error) {
            dispatch({ type: 'SET_MESSAGE', payload: error.message })
        }
    }
}

export function fetchGetUser(token, id) {
    return async (dispatch) => {
        const { data } = await instance.get(`/user/${id}`, {
            headers: {
                access_token: token
            }
        })
        dispatch({ type: 'GET_USER', payload: data })
    }
}

export function fetchGetCv(token, id) {
    return async (dispatch) => {
        try {
            const { data } = await instance.get(`/get-cv/${id}`, {
                headers: {
                    access_token: token
                }
            })
            dispatch({ type: 'GET_CV', payload: data })
        } catch (error) {
            dispatch({ type: 'GET_CV', payload: {} })
            dispatch({ type: 'SET_MESSAGE', payload: error.message })
        }
    }
}

export function searchJobseeker(name, token) {
    return async (dispatch) => {
        dispatch({ type: 'SET_LOADING', payload: true })
        try {
            const { data } = await instance.get(`/user/search?name=${name}`, {
                headers: {
                    access_token: token
                }
            })
            dispatch({ type: 'SET_SEARCH_JOBSEEKER', payload: data })
            dispatch({ type: 'SET_LOADING', payload: false })
        } catch (error) {
            dispatch({ type: 'SET_SEARCH_JOBSEEKER', payload: [] })
            dispatch({ type: 'SET_LOADING', payload: false })
        }
    }
}
