import workExperienceRedux from './workExperienceRedux';
import educationReducer from './educationReducer';
import certificationReducer from './certificationReducer';
import jobAppliedReducer from './jobAppliedReducer';
import jobBookmarkReducer from './jobBookmarkReducer';
import newsReducer from './newsReducer';
import loadingReducer from './loadingReducer';
import jobsReducer from './jobsReducer';
import skillsReducer from './skillsReducer';
import userReducer from './userReducer';
import projectTarget from './projectTargetReducer';
import jobPostingReducer from './jobPostingReducer';
import applicantReducer from './applicantReducer';
import jobseekerBookmarks from './jobseekerBookmarks';
import chattingReducer from './chattingReducer';
import messageReducer from './messagesReducer';
import vibePointReducer from './vibePointReducer'
import transactionReducer from './transactionReducer';
import bankAccountReducer from './bankAccountReducer';
import portofolioReducer from './portofolioReducer';
import projectReducer from './projectReducer';
import loanReducer from './loanReducer';
import epayslipReducer from './epayslipReducer';
import bpjsReducer from './bpjsReducer';
import attendanceReducer from './attendanceReducer';
import permissionReducer from './permissionReducer';
import videosReducer from './videosReducer';
import employeeReducer from './employeeReducer';
import overtimeReducers from './workOvertimeReducer';
import activityReducers from './activityReducer';

export {
    activityReducers,
    overtimeReducers,
    employeeReducer,
    videosReducer,
    permissionReducer,
    attendanceReducer,
    bpjsReducer,
    epayslipReducer,
    loanReducer,
    projectReducer,
    portofolioReducer,
    bankAccountReducer,
    transactionReducer,
    vibePointReducer,
    messageReducer,
    chattingReducer,
    jobseekerBookmarks,
    applicantReducer,
    projectTarget,
    jobPostingReducer,
    userReducer,
    skillsReducer,
    workExperienceRedux,
    educationReducer,
    certificationReducer,
    jobAppliedReducer,
    jobBookmarkReducer,
    newsReducer,
    loadingReducer,
    jobsReducer
}
