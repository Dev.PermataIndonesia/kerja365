import instance from "../../config/axios"

const initialState = {
    Employees: {
        All: [],
        Entered: [],
        Unentered: []
    },
    Employee: {},
    Permissions: {
        allDatas: [],
        inAction: [],
        alreadyAction: []
    },
    Absents: {
        allDatas: [],
        inAction: [],
        alreadyAction: []
    },
    Overtimes: {
        allDatas: [],
        inAction: [],
        alreadyAction: []
    },
    Activities: [],
    AttendaceRecaps: {},
    Loading: false
}

export default function employeeReducer(state = initialState, action) {
    switch (action.type) {
        case 'SET_EMPLOOYEES':
            return { ...state, Employees: action.payload }
        case 'SET_EMPLOOYEE':
            return { ...state, Employee: action.payload }
        case 'SET_EMPLOOYEE_PERMISSIONS':
            return { ...state, Permissions: action.payload }
        case 'SET_EMPLOOYEE_ABSENTS':
            return { ...state, Absents: action.payload }
        case 'SET_EMPLOOYEE_OVERTIMES':
            return { ...state, Overtimes: action.payload }
        case 'SET_EMPLOOYEE_ACTIVITIES':
            return { ...state, Activities: action.payload }
        case 'SET_EMPLOOYEE_ATTENDANCE_RECAPS':
            return { ...state, AttendaceRecaps: action.payload }
        case 'SET_EMPLOOYEES_LOADING':
            return { ...state, Loading: action.payload }
        default:
            return state
    }
}

export function fetchEmployees(token) {
    return async (dispatch) => {
        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: true })
        const { data } = await instance.get('/employee', {
            headers: {
                access_token: token
            }
        })
        if (data?.length > 0) {
            const entered = data?.filter(item => item.attendance)
            const untered = data?.filter(item => !item.attendance)
            dispatch({ type: 'SET_EMPLOOYEES', payload: { All: data, Entered: entered, Unentered: untered } })
        } else dispatch({ type: 'SET_EMPLOOYEES', payload: { All: [], Entered: [], Unentered: [] } })

        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: false })
    }
}

export function fetchEmployeePermissions(token) {
    return async (dispatch) => {
        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: true })
        const { data } = await instance.get('/employee/permission', {
            headers: {
                access_token: token
            }
        })

        if (data) {
            const allDatas = data
            const inAction = data.filter(item => item.status === 'Waiting')
            const alreadyAction = data.filter(item => item.status !== 'Waiting')
            dispatch({ type: 'SET_EMPLOOYEE_PERMISSIONS', payload: { allDatas, inAction, alreadyAction } })
        } else {
            dispatch({ type: 'SET_EMPLOOYEE_PERMISSIONS', payload: { allDatas: [], inAction: [], alreadyAction: [] } })
        }

        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: false })
    }
}

export function fetchEmployeeAbsents(token) {
    return async (dispatch) => {
        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: true })
        const { data } = await instance.get('/employee/off-work', {
            headers: {
                access_token: token
            }
        })
        if (data?.length > 0) {
            const allDatas = data
            const inAction = data.filter(item => item.status === 'Waiting')
            const alreadyAction = data.filter(item => item.status !== 'Waiting')

            dispatch({ type: 'SET_EMPLOOYEE_ABSENTS', payload: { allDatas, inAction, alreadyAction } })
        } else {
            dispatch({
                type: 'SET_EMPLOOYEE_ABSENTS', payload: {
                    allDatas: [],
                    inAction: [],
                    alreadyAction: []
                }
            })
        }

        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: false })
    }
}

export function fetchEmployeeOvertimes(token) {
    return async (dispatch) => {
        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: true })
        const { data } = await instance.get('/employee/work-overtime', {
            headers: {
                access_token: token
            }
        })

        if (data?.length > 0) {
            const allDatas = data
            const inAction = data.filter(item => item.status === 'Waiting')
            const alreadyAction = data.filter(item => item.status !== 'Waiting')
            dispatch({ type: 'SET_EMPLOOYEE_OVERTIMES', payload: { inAction, alreadyAction, allDatas } })
        } else dispatch({ type: 'SET_EMPLOOYEE_OVERTIMES', payload: [] })


        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: false })
    }
}

export function fetchEmployeeActivities(token) {
    return async (dispatch) => {
        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: true })
        const { data } = await instance.get('/employee/activity', {
            headers: {
                access_token: token
            }
        })
        if (data?.length > 0) dispatch({ type: 'SET_EMPLOOYEE_ACTIVITIES', payload: data })
        else dispatch({ type: 'SET_EMPLOOYEE_ACTIVITIES', payload: [] })

        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: false })
    }
}

export function fetchEmployeeActtendanceRecaps(token, startDate, endDate, name) {
    return async (dispatch) => {
        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: true })
        const { data } = await instance.get(`/employee/attendance/recap?startDate=${startDate}&endDate=${endDate}&name=${name}`, {
            headers: {
                access_token: token
            }
        })

        if (data) dispatch({ type: 'SET_EMPLOOYEE_ATTENDANCE_RECAPS', payload: data })
        else dispatch({ type: 'SET_EMPLOOYEE_ATTENDANCE_RECAPS', payload: {} })

        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: false })
    }
}

export function fetchEmployeeOvertimeSearch(token, startDate, endDate, name) {
    return async (dispatch) => {
        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: true })
        const { data } = await instance.get(`/employee/work-overtime/search?startDate=${startDate}&endDate=${endDate}&name=${name}`, {
            headers: {
                access_token: token
            }
        })


        if (data?.length > 0) {
            const inAction = data.filter(item => item.status === 'Waiting')
            const alreadyAction = data.filter(item => item.status !== 'Waiting')

            dispatch({ type: 'SET_EMPLOOYEE_OVERTIMES', payload: { inAction, alreadyAction } })
        }
        else dispatch({ type: 'SET_EMPLOOYEE_OVERTIMES', payload: [] })

        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: false })
    }
}

export function fetchEmployeeActivitiesSearch(token, startDate, endDate, name) {
    return async (dispatch) => {
        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: true })
        const { data } = await instance.get(`/employee/activity/search?startDate=${startDate}&endDate=${endDate}&name=${name}`, {
            headers: {
                access_token: token
            }
        })


        if (data?.length > 0) dispatch({ type: 'SET_EMPLOOYEE_ACTIVITIES', payload: data })
        else dispatch({ type: 'SET_EMPLOOYEE_ACTIVITIES', payload: [] })

        dispatch({ type: 'SET_EMPLOOYEES_LOADING', payload: false })
    }
}
