import database from '@react-native-firebase/database';

const initialState = {
    Chats: [],
    Loading: false
}


export default function chattingReducer(state = initialState, action) {
    switch (action.type) {
        case 'SET_CHATS':
            return { ...state, Chats: action.payload }
        case 'SET_CHAT_LOADING':
            return { ...state, Loading: action.payload }
        default:
            return state
    }
}

function compareOne(a, b) {
    const aDate = new Date(+String(a.id).substring(0, 4), +String(a.id).substring(5, 6) - 1, +String(a.id).substring(7, 9) + 1)
    const bDate = new Date(+String(b.id).substring(0, 4), +String(b.id).substring(5, 6) - 1, +String(b.id).substring(7, 9) + 1)
    return aDate - bDate
}

function compare(a, b) {
    return a.data.chatDate - b.data.chatDate
}

export function fetchChatting(userOne, userTwo) {
    return (dispatch) => {
        dispatch({ type: 'SET_CHAT_LOADING', payload: true })
        database().ref(`/chatting/${userOne}_${userTwo}/allChat/`).on('value', snapshot => {
            if (snapshot.val()) {
                const dataSnapshot = snapshot.val()
                const allDataChat = []
                Object.keys(dataSnapshot).map(key => {
                    const dataChat = dataSnapshot[key]
                    const newChat = []
                    Object.keys(dataChat).map(itemChat => {
                        newChat.push({
                            id: itemChat,
                            data: dataChat[itemChat]
                        })
                    })
                    allDataChat.push({
                        id: key,
                        data: newChat.sort(compare)
                    })
                })
                dispatch({ type: 'SET_CHATS', payload: allDataChat.sort(compareOne) })
            } else dispatch({ type: 'SET_CHATS', payload: null })

            dispatch({ type: 'SET_CHAT_LOADING', payload: false })
        })
    }
}
