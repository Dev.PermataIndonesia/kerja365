import instance from "../../config/axios"

const initialState = {
    Applications: [],
    Application: {},
    Loading: false
}

export default function jobAppliedReducer(state = initialState, action) {
    switch (action.type) {
        case 'SET_APPLICATIONS':
            return { ...state, Applications: action.payload }
        case 'SET_APPLICATION':
            return { ...state, Application: action.payload }
        case 'SET_APPLICATION_LOADING':
            return { ...state, Loading: action.payload }
        default:
            return state
    }
}

export function fetchApplications(token) {
    return async (dispatch) => {
        dispatch({ type: 'SET_APPLICATION_LOADING', payload: true })
        const { data } = await instance.get('/application', {
            headers: {
                access_token: token
            }
        })
        if (data) {
            dispatch({ type: 'SET_APPLICATIONS', payload: data })
        } else {
            dispatch({ type: 'SET_APPLICATIONS', payload: {} })
        }

        dispatch({ type: 'SET_APPLICATION_LOADING', payload: false })
    }
}

export function fetchApplication(token, jobId) {
    return async (dispatch) => {
        dispatch({ type: 'SET_APPLICATION_LOADING', payload: true })
        const { data } = await instance.get(`/application/applied/${jobId}`, {
            headers: {
                access_token: token
            }
        })
        if (data) dispatch({ type: 'SET_APPLICATION', payload: data })
        else dispatch({ type: 'SET_APPLICATION', payload: null })

        dispatch({ type: 'SET_APPLICATION_LOADING', payload: false })
    }
}
