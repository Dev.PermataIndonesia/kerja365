import instance from "../../config/axios"

const initialState = {
    Videos: [],
    Video: {},
    Loading: false,
}

export default function videosReducer(state = initialState, action) {
    switch (action.type) {
        case 'SET_VIDEOS':
            return { ...state, Videos: action.payload }
        case 'SET_VIDEO':
            return { ...state, Video: action.payload }
        case 'SET_VIDEO_LOADING':
            return { ...state, Loading: action.payload }
        default:
            return state
    }
}

export function fetchVideos(skip, limit) {
    return async (dispatch) => {
        dispatch({ type: "SET_VIDEO_LOADING", payload: true })
        const { data } = await instance.get(`/video?limit=${limit}&skip=${skip}`)
        if (data?.length > 0) dispatch({ type: "SET_VIDEOS", payload: data })
        else dispatch({ type: "SET_VIDEOS", payload: [] })
        dispatch({ type: "SET_VIDEO_LOADING", payload: false })
    }
}