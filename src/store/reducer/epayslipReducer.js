import instance from "../../config/axios"

const initialState = {
    Epayslip: [],
    Loading: false
}

export default function epayslipReducer(state = initialState, action) {
    switch (action.type) {
        case 'SET_EPAYSLIP':
            return { ...state, Epayslip: action.payload }
        case 'SET_EPAYSLIP_LOADING':
            return { ...state, Loading: action.payload }
        default:
            return state
    }
}

export function fetchEpayslip(year, token) {
    return async (dispatch) => {
        dispatch({ type: 'SET_EPAYSLIP_LOADING', payload: true })
        const { data } = await instance.get(`/epayslip?year=${year}`, {
            headers: {
                access_token: token
            }
        })
        dispatch({ type: 'SET_EPAYSLIP', payload: data })
        dispatch({ type: 'SET_EPAYSLIP_LOADING', payload: false })
    }
}
