import instance from "../../config/axios"

const initialState = {
    Attendances: [],
    Attendance: {},
    GetAttendance: {},
    AttendanceEmployee: {},
    Loading: false
}

export default function attendanceReducer(state = initialState, action) {
    switch (action.type) {
        case "SET_ATTENDANCES":
            return { ...state, Attendances: action.payload }
        case "SET_ATTENDANCE":
            return { ...state, Attendance: action.payload }
        case "SET_GET_ATTENDANCE":
            return { ...state, GetAttendance: action.payload }
        case "SET_ATTENDANCE_EMPLOYEE":
            return { ...state, AttendanceEmployee: action.payload }
        case "SET_ATTENDANCES_LOADING":
            return { ...state, Loading: action.payload }
        default:
            return state
    }
}

export function fetchAttendances(token) {
    return async (dispatch) => {
        dispatch({ type: "SET_ATTENDANCES_LOADING", payload: true })

        const { data } = await instance.get('/attendance', {
            headers: {
                access_token: token
            }
        })
        dispatch({ type: "SET_ATTENDANCES", payload: data })
        dispatch({ type: "SET_ATTENDANCES_LOADING", payload: false })
    }
}

export function fetchAttendancesByRange(token, start, end) {
    return async (dispatch) => {
        dispatch({ type: "SET_ATTENDANCES_LOADING", payload: true })

        const { data } = await instance.get(`/attendance/range?startDate=${start}&endDate=${end}`, {
            headers: {
                access_token: token
            }
        })
        dispatch({ type: "SET_ATTENDANCES", payload: data })
        dispatch({ type: "SET_ATTENDANCES_LOADING", payload: false })
    }
}

export function fetchAttendance(token) {
    return async (dispatch) => {
        const { data } = await instance.get('/attendance/today', {
            headers: {
                access_token: token
            }
        })
        if (!data) dispatch({ type: "SET_ATTENDANCE", payload: {} })
        else dispatch({ type: "SET_ATTENDANCE", payload: data })
    }
}

export function fetchGetAttendance(token, id) {
    return async (dispatch) => {
        dispatch({ type: "SET_ATTENDANCES_LOADING", payload: true })

        const { data } = await instance.get(`/attendance/${id}`, {
            headers: {
                access_token: token
            }
        })
        if (!data) dispatch({ type: "SET_GET_ATTENDANCE", payload: {} })
        else dispatch({ type: "SET_GET_ATTENDANCE", payload: data })

        dispatch({ type: "SET_ATTENDANCES_LOADING", payload: false })

    }
}

export function fetchAttendanceEmployee(token, id) {
    return async (dispatch) => {
        dispatch({ type: "SET_ATTENDANCES_LOADING", payload: true })

        const { data } = await instance.get(`/attendance/${id}`, {
            headers: {
                access_token: token
            }
        })
        if (!data) dispatch({ type: "SET_ATTENDANCE_EMPLOYEE", payload: {} })
        else dispatch({ type: "SET_ATTENDANCE_EMPLOYEE", payload: data })

        dispatch({ type: "SET_ATTENDANCES_LOADING", payload: false })

    }
}
