import { showMessage } from "react-native-flash-message"
import instance from "../../config/axios"

const initialState = {
    LoanHistory: [],
    ManFeePlafon: {},
    Loading: false
}

export default function loadingReducer(state = initialState, action) {
    switch (action.type) {
        case 'SET_LOAN_HISTORY':
            return { ...state, LoanHistory: action.payload }
        case 'SET_MAN_FEE_PLAFON':
            return { ...state, ManFeePlafon: action.payload }
        case 'SET_MAN_FEE_PLAFON_LOADING':
            return { ...state, Loading: action.payload }
        default:
            return state
    }
}

export function fetchManFeePlafon(date, tenor, schemaId, token) {
    return async (dispatch) => {
        dispatch({ type: "SET_MAN_FEE_PLAFON_LOADING", payload: true })
        const { data } = await instance.get(`/loan/manfeeplafon/${date}/${tenor}?schemaId=${schemaId}`, {
            headers: {
                access_token: token
            }
        })
        console.log(data, "<<< data");
        dispatch({ type: "SET_MAN_FEE_PLAFON", payload: data })
        dispatch({ type: "SET_MAN_FEE_PLAFON_LOADING", payload: false })
    }
}

export function fetchLoanHistory(token) {
    return async (dispatch) => {
        const { data } = await instance.get(`/loan/history`, {
            headers: {
                access_token: token
            }
        })
        dispatch({ type: "SET_LOAN_HISTORY", payload: data })
    }
}
