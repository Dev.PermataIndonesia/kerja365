import instance from "../../config/axios"

const initialState = {
    Bpjs: {},
    Loading: false
}

export default function bpjsReducer(state = initialState, action) {
    switch (action.type) {
        case "SET_BPJS":
            return { ...state, Bpjs: action.payload }
        case "SET_BPJS_LOADING":
            return { ...state, Loading: action.payload }
        default:
            return state
    }
}

export function fetchBpjs(token) {
    return async (dispatch) => {
        dispatch({ type: 'SET_BPJS_LOADING', payload: true })
        const { data } = await instance.get('/bpjs', {
            headers: {
                access_token: token
            }
        })
        dispatch({ type: 'SET_BPJS', payload: data })
        dispatch({ type: 'SET_BPJS_LOADING', payload: false })
    }
}
